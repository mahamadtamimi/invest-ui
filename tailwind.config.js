import {nextui} from '@nextui-org/react'

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './pages/**/*.{js,ts,jsx,tsx,mdx}',
        './components/**/*.{js,ts,jsx,tsx,mdx}',
        './app/**/*.{js,ts,jsx,tsx,mdx}',
        './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'
    ],
    theme: {
        extend: {},
    },
    darkMode: "class",
    plugins: [nextui({
        defaultTheme: "light",
        themes: {
            dark: {
                colors: {
                    primary: {
                        DEFAULT: "#D25487",
                        foreground: "#000000",
                    },
                    focus: "#D25487",
                },
            },
            light: {
                colors: {
                    primary: {
                        DEFAULT: "#D25487",
                        foreground: "#000000",
                    },
                    focus: "#D25487",
                },
            },
        },

    })],
}
