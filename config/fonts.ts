
import {DM_Sans, Roboto} from "next/font/google";
import localFont from 'next/font/local'

export const dmSans = DM_Sans({
    subsets: ['latin'],
    display: 'swap',
})

export const myFont = localFont({
    src: './GothamPro/gothampro.ttf',
    display: 'swap',
    variable: '--font-dm',
})


export const myFontBold = localFont({
    src: './GothamPro/gothampro_bold.ttf',
    display: 'swap',
    variable: '--font-dm-bold',
})


export const roboto = Roboto({
    subsets: ['latin'],
    display: 'swap',
    variable: '--font-roboto',
    weight: '900'
})

