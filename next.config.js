/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    API_PATH : 'http://127.0.0.1:8080'
  },
  reactStrictMode: true,
}

module.exports = nextConfig
