FROM node:20.12.2-alpine3.19

WORKDIR /user/src/app

COPY package*.json .

RUN npm install --legacy-peer-deps --loglevel verbose

COPY . .

EXPOSE 3000

RUN npm run build

CMD npm start