import {legacy_createStore as createStore, Store, applyMiddleware} from 'redux';
import {reducer} from './features/userReducer';
import {persistReducer, persistStore} from 'redux-persist';
import storage from 'redux-persist/lib/storage'


const persistConfig = {
    key: 'investStorage',
    storage: storage,
}

const persistedReducer = persistReducer(persistConfig, reducer);
const store: Store = createStore(
    persistedReducer,
);

const persistor = persistStore(store);
export {store, persistor};