import {initialState} from "./initialState"
// @ts-ignore
import {State} from "./initialState"
import {useSelector} from "react-redux";

export function reducer(state: State = initialState, action: any) {
    switch (action.type) {

        case "LOGIN":
            return {
                ...state,
                user: action.payload.user,

            }


        case "LOGOUT":
            return {
                ...state,
                user: null,

            }


        case "ACTIVE_ACCOUNT":


            return {
                ...state,
                user: action.payload,

            }

        case "UN_LOCK" :

            return {
                ...state,
                lock: action.payload,

            }

        default:
            return state;
    }
}