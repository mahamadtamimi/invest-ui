import React, {useEffect, useState} from "react";
import {
    Table,
    TableHeader,
    TableColumn,
    TableBody,
    TableRow,
    TableCell,
    User,
    Chip,
    Tooltip,
    ChipProps,
    getKeyValue, Input, Button, Pagination
} from "@nextui-org/react";
import {EditIcon} from "./EditIcon";
import {DeleteIcon} from "./DeleteIcon";
import {EyeIcon} from "./EyeIcon";
import {SearchIcon} from "@nextui-org/shared-icons";
import 'react-toastify/dist/ReactToastify.css';
import Link from "next/link";

import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import {toast} from "react-toastify";
import moment from "moment/moment";
import styles from "@/styles/dashboard.module.scss";
import UserTransaction from "@/components/Dashboard/Dashboard/UserTransaction";
import WithdrawalTransactionTable from "@/components/admin/WithdrawalArchive/WithdrawalTransactionTable";
// import {columns, users} from "./data";

const statusColorMap: Record<string, ChipProps["color"]> = {
    active: "success",
    paused: "danger",
    vacation: "warning",
};


export default function WithdrawalArchive() {

    // @ts-ignore
    const user = useSelector((state) => state.user);
    const router = useRouter()
    const [data, setData] = useState({})
    const [isLoading, setLoading] = useState(true)


    useEffect(() => {

        const url = `${process.env.API_PATH}/api/v1/withdrawal/list`
        fetch(url, {
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                Authorization: `bearer ${user.token}`
            },
        })
            .then((res) => {



                if (res.status === 401) {
                    router.push('/auth/login')
                    return null
                }

               return  res.json()

            })
            .then((data) => {

                setData(data)
                setLoading(false)
            })
            .catch((error) => {
                toast.error('server error')
            });


    }, [])
    // @ts-ignore



    if (isLoading) return <p>Loading...</p>
    if (!data) return <p>No profile data</p>
    return (
        <div className="flex flex-col gap-4">
            <div className="flex justify-end gap-3 items-end">


            </div>


            <div className={styles.content__transaction_table}>

                <WithdrawalTransactionTable data={data}/>

            </div>

        </div>
    )
        ;
}
