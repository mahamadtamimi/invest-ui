import React, {useEffect, useState} from "react";
import {
    Table,
    TableHeader,
    TableColumn,
    TableBody,
    TableRow,
    TableCell,
    Pagination,
    getKeyValue, Tooltip
} from "@nextui-org/react";
import moment from "moment";
import {EyeIcon} from "@/components/admin/UsersArchive/EyeIcon";
import {EditIcon} from "@/components/admin/UsersArchive/EditIcon";
import {DeleteIcon} from "@/components/admin/UsersArchive/DeleteIcon";
import {ConfirmIcon} from "@/components/admin/WithdrawalArchive/ConfirmIcon";
import {toast} from "react-toastify";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";


export default function WithdrawalTransactionTable(props: any) {

    const router = useRouter()


    const [tableData, setTableData] = useState(props.data)

    const [page, setPage] = React.useState(1);
    // @ts-ignore
    const user = useSelector((state) => state.user);

    function confirmWithdrawal(e: any) {


        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const url = `${process.env.API_PATH}/api/v1/withdrawal/set-complete`

        const userData = {
            'id': e,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                Authorization: `bearer ${user.token}`
            },
            // @ts-ignore
            body: formBody
        })
            .then((res) => {

                    if (res.status === 200) {
                        // router.reload()
                    }


                    if (res.status === 401) {
                        router.push('/auth/login')
                        return null
                    }
                    if (res.status === 404) {
                        toast.dismiss("registerToastId")
                        toast.error('bad request')
                        return null
                    }

                    return res.json()
                }
            ).then((data) => {
                console.log(data)
            // alert('hello')
            // console.log(data)
            // toast.dismiss("registerToastId")
            // toast.success('Status change was done successfully')
            // setTableData(data)

        })
            .catch((error) => {
                toast.dismiss("registerToastId")
                toast.error('server error')
            });

    }


    const rowsPerPage = 4;

    const pages = Math.ceil(tableData.length / rowsPerPage);

    const items = React.useMemo(() => {
        const start = (page - 1) * rowsPerPage;
        const end = start + rowsPerPage;

        return tableData.slice(start, end);
    }, [page, tableData, setTableData]);


    return <Table
        aria-label="Example table with client side pagination"
        bottomContent={
            <div className="flex w-full justify-center">
                <Pagination
                    isCompact
                    showControls
                    showShadow
                    color="primary"
                    page={page}
                    total={pages}
                    onChange={(page) => setPage(page)}
                />
            </div>
        }
        classNames={{
            wrapper: "min-h-[222px]",
        }}
    >
        <TableHeader>
            <TableColumn key="user">User</TableColumn>
            <TableColumn key="amount">Amount</TableColumn>
            <TableColumn key="role">Description</TableColumn>
            <TableColumn key="type">Type</TableColumn>

            <TableColumn key="date">Date</TableColumn>
            <TableColumn key="status">Status</TableColumn>
            <TableColumn key="action">Action</TableColumn>

        </TableHeader>
        <TableBody>
            {items.map((item: any) => (
                <TableRow key={item.id}>
                    <TableCell>{item.User.email}</TableCell>
                    <TableCell>{item.amount}$</TableCell>
                    <TableCell>{item.description}</TableCell>
                    <TableCell>{item.type}</TableCell>

                    <TableCell>{moment(item.createdAt).format('YYYY/MM/DD  HH:mm:ss')}</TableCell>
                    <TableCell>{item.status}</TableCell>
                    <TableCell>
                        <div className="relative flex items-center gap-2">
                            <Tooltip content="Details">
                                    <span
                                        className="text-lg text-default-400 cursor-pointer active:opacity-50">
                                        <EyeIcon/>
                                    </span>
                            </Tooltip>
                            <Tooltip color='success' content="confirm">
                                               <span data-id={item.id} onClick={() => confirmWithdrawal(item.id)}
                                                     className="text-lg text-default-400 cursor-pointer active:opacity-50">
                                                 <ConfirmIcon/>
                                               </span>
                            </Tooltip>


                        </div>
                    </TableCell>


                </TableRow>

            ))
            }
        </TableBody>
    </Table>;
}
