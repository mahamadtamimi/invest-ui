import {useEffect, useState} from "react";
import {Button, Input, Select, SelectItem, user} from "@nextui-org/react";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import {toast} from "react-toastify";
import Link from "next/link";
import UserPlan from "@/components/admin/UserPlans";

export default function UserInfo() {
    const router = useRouter();

    console.log(router.query.id);

    const [pending, setPending] = useState(true)
    const [walletAddress, setWalletAddress] = useState()
    const [responseUser, setResponseUser] = useState()
    const [responsePlans, setResponsePlans] = useState()

    const user = useSelector((state: any) => state.user)

    const [wallet, setWallet] = useState()

    useEffect(() => {
        if (router.query.id) {

            setPending(true)

            //

            const userData = {
                'id': router.query.id,
            }


            let formBody = [];
            for (var property in userData) {
                var encodedKey = encodeURIComponent(property);
                // @ts-ignore
                var encodedValue = encodeURIComponent(userData[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            // @ts-ignore
            formBody = formBody.join("&");
            // @ts-ignore


            fetch(`${process.env.API_PATH}/api/v1/get-user-info`, {
                method: 'POST',
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + user.token,
                },
                // @ts-ignore
                body: formBody,
            })
                .then(res => res.json())
                .then(data => {
                    if (data.success) {
                        console.log(data)
                        setPending(false)
                        setResponseUser(data.user)
                        setResponsePlans(data.plans)
                        setWallet(data.user.Wallet.credit)
                        setWalletAddress(data.user.walletAddress)
                    }
                })

        }

    }, [router])


    function updateWallet(e : any){
        const userData = {
            'id': router.query.id,
            wallet : wallet
        }

        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        fetch(`${process.env.API_PATH}/api/v1/update-user-wallet`, {
            method: 'POST',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + user.token,
            },
            // @ts-ignore
            body: formBody,
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data.success) {
                    console.log(data)
                    // setPending(false)
                    // setResponseUser(data.user)
                    // setResponsePlans(data.plans)
                    // setWallet(data.user.Wallet.credit)
                }
            })


    }


    function updateWalletAddress(e : any){
        const userData = {
            'id': router.query.id,
            walletAddress : walletAddress
        }

        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        fetch(`${process.env.API_PATH}/api/v1/update-user-wallet-address`, {
            method: 'POST',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + user.token,
            },
            // @ts-ignore
            body: formBody,
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data.success) {
                    console.log(data)
                    // setPending(false)
                    // setResponseUser(data.user)
                    // setResponsePlans(data.plans)
                    // setWallet(data.user.Wallet.credit)
                }
            })

    }

    // @ts-ignore

    return <div> {
        !pending && <>
            {/*<form action="" onSubmit={(e) => handleSubmit(e)}>*/}

            <div className={'flex flex-row gap-2 mb-4'}>


                <div className={'basis-1/3'}>
                    <Input label={'email'} required={true} name={'email'} value={
                        // @ts-ignore
                        responseUser.email} type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Input label={'first name'} required={true} name={'firstName'} value={
                        // @ts-ignore
                        responseUser.firstName}
                           type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Input label={'last name'} required={true} name={'lastName'} value={
                        // @ts-ignore
                        responseUser.lastName} type="text"/>
                </div>


            </div>

            <div className={'flex flex-row gap-2 mb-4'}>

                <div className={'basis-1/2'}>
                    <Select
                        // @ts-ignore
                        defaultSelectedKeys={[responseUser.Roles[0].role]}
                        name="role"
                        label="Select Role"
                        placeholder="Select Role"
                        className="max-w-xs"
                    >
                        <SelectItem key={'Admin'}>admin</SelectItem>
                        <SelectItem key={'User'}>user</SelectItem>
                    </Select>
                </div>

            </div>

            <div className={'flex gap-2 border-t pt-3'}>
                <div className={'basis-1/3'}>
                    <Input label={'wallet'} required={true} name={'wallet'} value={wallet}
                           onChange={(e) => setWallet(// @ts-ignore
                                e.target.value)} type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Button onClick={(e)=> updateWallet(e)} className={`btn-admin mt-1`}>
                        Update wallet
                    </Button>
                </div>
                <div className={'basis-1/3 align-bottom justify-end'}>
                    <Link href={'/admin/users/12/transaction-info'} className={'text-right block align-bottom mt-3'}>
                        Transaction Detail
                    </Link>
                </div>
            </div>
            <div className={'flex gap-2 border-t pt-3'}>
                <div className={'basis-1/3'}>
                    <Input label={'wallet address'} required={true} name={'walletAddress'} value={walletAddress}
                           onChange={(e) => setWalletAddress(// @ts-ignore
                                e.target.value)} type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Button onClick={(e)=> updateWalletAddress(e)} className={`btn-admin mt-1`}>
                        Update wallet address
                    </Button>
                </div>
                <div className={'basis-1/3 align-bottom justify-end'}>
                    <Link href={'/admin/users/12/transaction-info'} className={'text-right block align-bottom mt-3'}>
                        Transaction Detail
                    </Link>
                </div>
            </div>
            <div className={'flex gap-2 border-t pt-3 mt-3'}>
                <UserPlan plans={responsePlans} />
            </div>

            {/*<div>*/}
            {/*    <Button type={'submit'} size={"sm"} className={`btn-admin`}>*/}
            {/*        Submit*/}
            {/*    </Button>*/}
            {/*</div>*/}


            {/*</form>*/}
        </>
    }


    </div>
}