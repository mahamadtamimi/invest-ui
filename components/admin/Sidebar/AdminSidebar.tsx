import styles from '@/styles/dashboard.module.scss'
import logo from '../../../public/log.svg'
import Image from "next/image";

import dashboardIcon from '../../../public/Category.svg'
import transActions from '../../../public/transactions.svg'

import signOutIcon from '../../../public/Logout.svg'
import avatar from '../../../public/avatar.png'

import React from "react";
import {User, Link, Input} from "@nextui-org/react";
import {SearchIcon} from "@nextui-org/shared-icons";
import {HomeIcon} from "@/components/Dashboard/Sidebar/HomeIcon";
import {SignOutIcon} from "@/components/Dashboard/Sidebar/SignOutIcon";
import {SettingIcon} from "@/components/Dashboard/Sidebar/SettingIcon";
import {SupportIcon} from "@/components/Dashboard/Sidebar/SupportIcon";
import {useSelector} from "react-redux";

function Avatar(props: any) {
    return (
        <User
            classNames={{
                name: 'avatar-name'
            }}
            name={props.name}

        />
    );
}

export default function AdminSidebar() {

    // @ts-ignore
    const user = useSelector((state) => state.user);


    return <section className={styles.sidebar}>
        <span className={styles.admin_sidebar_bg}></span>
        <div className={styles.sidebar__title}>
            <Image src={logo} alt=''/>


        </div>
        <div className={styles.avatar_sec}>
            <Avatar name={`${user.firstName} ${user.lastName}`}/>
        </div>
        <div>
            <Input
                classNames={{
                    inputWrapper: 'sidebar-search-input-wrapper',
                    mainWrapper: 'sidebar-search-input-main-wrapper'
                }}


                placeholder={'search'}
                startContent={
                    <SearchIcon
                        className="text-black/50 mb-0.5 dark:text-white/90 text-slate-400 pointer-events-none flex-shrink-0"/>
                }
            />
        </div>
        <div className={styles.sidebar__content}>
            <ul className={styles.sidebar__content__links}>
                <li className={styles.links__item}>
                    <Link href="/admin" className={`${styles.link}`}>

                        <HomeIcon/>

                        Dashboard
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/admin/tickets/list" className={styles.link}>

                        <SupportIcon/>

                        Tickets
                    </Link>
                </li>


                <li className={styles.links__item}>
                    <Link href="/admin/plans/list" className={styles.link}>

                        <HomeIcon/>

                        Plans
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/admin/users/list" className={styles.link}>

                        <HomeIcon/>

                        Users
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/admin/withdrawal-request/list" className={styles.link}>

                        <HomeIcon/>

                        Withdrawal Request
                    </Link>
                </li>

            </ul>
        </div>

        <div className={styles.sidebar__footer}>
            <ul className={styles.sidebar__content__links}>
                <li className={styles.links__item}>

                    <Link href="#" className={styles.link}>
                        <SettingIcon/>
                        Setting
                    </Link>

                </li>

                <li className={styles.links__item}>

                    <Link href="#" className={styles.link}>
                        <SignOutIcon/>
                        Sign Out
                    </Link>

                </li>
            </ul>


        </div>
    </section>
}