import styles from '@/styles/summery.module.scss'
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from "@nextui-org/react";
import moment from "moment";

export default function Summery() {

    const [pending, setPending] = useState(true)
    const [balanceData, setBalanceData] = useState({})
    const [payoutPending, setPayoutPending] = useState({})

    const [activePlans, setActivePlans] = useState()
    const [activePlanPending, setActivePlanPending] = useState(true)
    const [networkError, setNetworkError] = useState(false)
    const [wallet, setWallet] = useState()
    const [pendingWallet, setPendingWallet] = useState(true)

    // @ts-ignore
    const user = useSelector((state) => state.user);
    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        const headers = {
            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },
        }
        fetch(`${process.env.API_PATH}/api/v1/balance`, headers)
            .then(res => res.json())
            .then(data => {
                    console.log(data)
                    setPending(false)
                    if (data.error) {

                        setNetworkError(true)
                    }
                    if (!data.error) {

                        setBalanceData(data.balance)
                        setPayoutPending(data.payoutPending.payouts)
                    }
                }
            )


        fetch(`${process.env.API_PATH}/api/v1/user-active-plans`, headers)
            .then(res => res.json())
            .then(data => {
                    setActivePlans(data)
                    setActivePlanPending(false)


                }
            )
        fetch(`${process.env.API_PATH}/api/v1/user-wallet`, headers)
            .then(res => res.json())
            .then(data => {

                    setPendingWallet(false)
                    setWallet(data.total)
                }
            )

    }, []);


    const WalletCart = !pending && networkError && Object.keys(balanceData).map((k, idx) => {

        return <div className={styles.wallets} key={`${k}-${idx}`}>
            {k} :
            <span className={styles.wallets__cradit}>
                  {
                      // @ts-ignore
                      balanceData[k]['amount']}
            </span>

        </div>

    });


    const PayoutRequest = !pending && networkError && Object.keys(payoutPending).map((k, idx) => {

        return <div key={`pending-${idx}`} className={styles.pennding__seccc}>
            <div className={styles.pennding__seccc__head}>
                            <span key={`${k}-${idx}`}>
                                <span className={styles.label}>
                                     id :
                                </span>
                            <span className={styles.bod}>
                                     {// @ts-ignore
                                         payoutPending[k]['id']}
                            </span>
                            </span>

                <span key={`${k}-${idx}`}>
                                             <span className={styles.label}>
                                 created at :
                                             </span>
                            <span className={styles.bod}>
                                  {// @ts-ignore
                                      payoutPending[k]['created_at']}
                            </span>
                            </span>
            </div>
            <div className={styles.pennding__seccc__body}>
                              <span key={`${k}-${idx}`}>
                                  <span className={styles.label}>
                                         address :
                                  </span>
                                      <span className={styles.bod}>
                                  {// @ts-ignore
                                      payoutPending[k]['address']}
                                      </span>
                    </span>
                <span key={`${k}-${idx}`}>
                         <span className={styles.label}>
                         amount :
                         </span>
                            <span className={styles.bod}>
                        {
                            // @ts-ignore
                            payoutPending[k]['amount']}
                            </span>
                    </span>

                <span key={`${k}-${idx}`}>
                          <span className={styles.label}>
                             currency :
                          </span>
                         <span className={styles.bod}>
                            {
                                // @ts-ignore
                                payoutPending[k]['currency']}
                        </span>
                    </span>
            </div>

        </div>


    });

    function NetworkError() {
        return <p>Network Error</p>;
    }


    return <>
        <div className={styles.summery__sec}>

            <div className={styles.summery_sec_wallet}>
                <p className={styles.summery_sec_wallet_label}>
                    wallets :
                </p>

                {networkError ? <NetworkError/> : WalletCart}
            </div>
            <div className={styles.payout_sec_wallet}>
                <p className={styles.summery_sec_wallet_label}>
                    pending request :
                </p>
                {networkError ? <NetworkError/> : PayoutRequest}
            </div>

        </div>
        <div>
            <p>
                total user wallet :

                {wallet} $
            </p>
        </div>
        <div>
            <p>
                active plans :
            </p>
            <Table aria-label="Example static collection table">
                <TableHeader>
                    <TableColumn>Amount</TableColumn>
                    <TableColumn>StartAt</TableColumn>
                    <TableColumn>ExpireAt</TableColumn>
                    <TableColumn>Plan Name</TableColumn>
                    <TableColumn>User email</TableColumn>
                </TableHeader>
                <TableBody>
                    {// @ts-ignore
                        !activePlanPending && activePlans.map((item: any) => {
                            console.log(item.Plan.name)
                            return <TableRow key={item.id}>
                                <TableCell>{item.initAmount} $</TableCell>
                                <TableCell>{moment(item.startAt).format('YYYY/MM/DD HH:mm:ss')}</TableCell>
                                <TableCell>{moment(item.expireAt).format('YYYY/MM/DD HH:mm:ss')}</TableCell>

                                <TableCell>{item.Plan.name}</TableCell>
                                <TableCell>{item.User.email}</TableCell>


                            </TableRow>

                        })}

                </TableBody>
            </Table>

        </div>


    </>

}