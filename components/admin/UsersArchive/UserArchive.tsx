import React, {useCallback, useEffect, useMemo, useState} from "react";
import {
    Table,
    TableHeader,
    TableColumn,
    TableBody,
    TableRow,
    TableCell,
    User,
    Chip,
    Tooltip,
    ChipProps,
    getKeyValue, Input, Button, Pagination, user
} from "@nextui-org/react";
import {EditIcon} from "./EditIcon";
import {DeleteIcon} from "./DeleteIcon";
import {EyeIcon} from "./EyeIcon";
import {SearchIcon} from "@nextui-org/shared-icons";

import Link from "next/link";
import {toast} from "react-toastify";
import loading = toast.loading;
import {he} from "@faker-js/faker";
import {useSelector} from "react-redux";

// import {columns, users} from "./data";

const statusColorMap: Record<string, ChipProps["color"]> = {
    active: "success",
    paused: "danger",
    vacation: "warning",
};


export default function UserArchive() {

    const [data, setData] = useState({})
    const [isLoading, setLoading] = useState(true)
    const [filterValue, setFilterValue] = React.useState("");
    const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));
    const [page, setPage] = React.useState(1);

    const columns = [
        {name: "email", uid: "email"},
        // {name: "minimum deposit", uid: "minimumDeposit"},
        {name: "role", uid: "role"},

        {name: "actions", uid: "actions"},

    ];
    const statusOptions = [
        {name: "Active", uid: "active"},
        {name: "Paused", uid: "paused"},
        {name: "Vacation", uid: "vacation"},
    ];
    const [sortDescriptor, setSortDescriptor] = useState({
        column: "age",
        direction: "ascending",
    });
    const [statusFilter, setStatusFilter] = useState("all");
    const [rowsPerPage, setRowsPerPage] = useState(5);


    // @ts-ignore
    const user = useSelector((state) => state.user);


    useEffect(() => {
        console.log(localStorage.getItem('user'))
        const url = `${process.env.API_PATH}/api/v1/users/list`
        fetch(url , {headers : {"authorization": "Bearer " + user.token}})
            .then((res) => res.json())
            .then((data) => {
                setData(data)
                setLoading(false)
            })
    }, [])

    //

    const hasSearchFilter = Boolean(filterValue);


    const filteredItems = useMemo(() => {
        if (!isLoading) {

            // @ts-ignore
            let filteredUsers = [...data];

            if (hasSearchFilter) {
                filteredUsers = filteredUsers.filter((user) =>
                    user.email.toLowerCase().includes(filterValue.toLowerCase()),
                );
            }
            // if (statusFilter !== "all" && Array.from(statusFilter).length !== statusOptions.length) {
            //     filteredUsers = filteredUsers.filter((user) =>
            //         Array.from(statusFilter).includes(user.status),
            //     );
            // }

            return filteredUsers;


        }

    }, [data, filterValue, statusFilter]);
    // @ts-ignore
    const pages = !isLoading && Math.ceil(filteredItems.length / rowsPerPage);
    // @ts-ignore
    type User = typeof data[0];

    const items = useMemo(() => {
        if (!isLoading) {
            const start = (page - 1) * rowsPerPage;
            const end = start + rowsPerPage;
            // @ts-ignore
            return filteredItems.slice(start, end);
        }

    }, [page, filteredItems, rowsPerPage]);

    const sortedItems = useMemo(() => {
        if (!isLoading) {
            // @ts-ignore
            return [...items].sort((a, b) => {
                const first = a[sortDescriptor.column];
                const second = b[sortDescriptor.column];
                const cmp = first < second ? -1 : first > second ? 1 : 0;

                return sortDescriptor.direction === "descending" ? -cmp : cmp;
            });

        }

    }, [sortDescriptor, items]);

    const renderCell = useCallback((user: User, columnKey: React.Key) => {


        const cellValue = user[columnKey as keyof User];

        switch (columnKey) {
            case "username":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">  {user.username}</p>

                    </div>

                );

            case "role":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">  {user.Roles?.map((item: any) => (
                            item.role
                        ))}</p>
                    </div>

                )
                    ;


            case "actions":
                return (
                    <div className="relative flex items-center gap-2">
                        <Tooltip content="Details">
                            <Link href={`/admin/users/${user.id}/user-info`} className="text-lg text-default-400 cursor-pointer active:opacity-50">
                                <EyeIcon/>

                                </Link>
                        </Tooltip>
                    </div>
            );
            default:
            return cellValue;
            }
            }, []);

    // @ts-ignore
            const onSearchChange = React.useCallback((value) => {
                if (value) {
                    setFilterValue(value);
                    setPage(1);
                } else {
                    setFilterValue("");
                }
            }, []);

            const onClear = React.useCallback(() => {
                setFilterValue("")
                setPage(1)
            }, [])

            const onNextPage = React.useCallback(() => {
                // @ts-ignore
                if (page < pages) {
                    setPage(page + 1);
                }
            }, [page, pages]);

            const onPreviousPage = React.useCallback(() => {
                if (page > 1) {
                    setPage(page - 1);
                }
            }, [page]);


            const topContent = useMemo(() => {
                return (
                    <div className="flex flex-col gap-4">
                        <div className="flex justify-between gap-3 items-end">
                            <Input
                                isClearable
                                className="w-full sm:max-w-[44%]"
                                placeholder="Search by name..."
                                startContent={<SearchIcon/>}
                                value={filterValue}
                                onClear={() => onClear()}
                                onValueChange={onSearchChange}
                            />
                            <div className="flex gap-3">

                                <Button as={Link} href={'/admin/users/create-new-user'} size={"sm"} className={`btn-admin`}>
                                    Add New
                                </Button>
                            </div>
                        </div>
                    </div>
                );
            }, [
            filterValue,
            onSearchChange,
            hasSearchFilter,
            ]);


            // @ts-ignore
    const bottomContent = useMemo(() => {
                if (!isLoading) {

                    return (
                        <div className="py-2 px-2 flex justify-between items-center">
        <span className="w-[30%] text-small text-default-400">
          {    // @ts-ignore
              selectedKeys === "all"
              ? "All items selected"
              : `${selectedKeys.size} of ${
                      // @ts-ignore
                  filteredItems.length} selected`}
        </span>
                            <Pagination
                                isCompact
                                showControls
                                showShadow
                                color="primary"
                                page={page}
                                // @ts-ignore
                                total={pages}
                                onChange={setPage}
                            />
                            <div className="hidden sm:flex w-[30%] justify-end gap-2">
                                <Button isDisabled={pages === 1} size="sm" variant="flat" onPress={onPreviousPage}>
                                    Previous
                                </Button>
                                <Button isDisabled={pages === 1} size="sm" variant="flat" onPress={onNextPage}>
                                    Next
                                </Button>
                            </div>
                        </div>
                    );

                }

            },
        // @ts-ignore
        [selectedKeys, data.length, page, pages, hasSearchFilter]);

            if (isLoading) return
                <p>Loading...</p>
            if (!data) return
                <p>No profile data</p>
            return (
                <div className="flex flex-col gap-4">


                    <Table
                        topContent={topContent}
                        bottomContent={bottomContent}
                        aria-label="Example table with custom cells">
                        <TableHeader columns={columns}>
                            {(column) => (
                                <TableColumn key={column.uid} align={column.uid === "actions" ? "center" : "start"}>
                                    {column.name}
                                </TableColumn>
                            )}
                        </TableHeader>
                        <TableBody emptyContent={"No users found"} items={sortedItems}>
                            {(item) => (
                                <TableRow key={item.id}>
                                    {(columnKey) => <TableCell>{renderCell(item, columnKey)}</TableCell>}
                                </TableRow>
                            )}
                        </TableBody>

                    </Table>
                </div>
            )

            }

