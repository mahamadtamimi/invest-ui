export default async function getPlan() {
    const url = `${process.env.API_PATH}/api/v1/plans/list`

    return fetch(url).then((response) => {
        return response.json().then((data) => {

            return data;
        }).catch((err) => {
            console.log(err);
        })
    });

}