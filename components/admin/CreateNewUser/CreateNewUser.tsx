import {Button, Input, Select, SelectItem} from "@nextui-org/react";
import {useSelector} from "react-redux";
import {useState} from "react";
import {toast} from "react-toastify";
import {useRouter} from "next/router";


export default function CreateNewUser() {
    const router = useRouter();

    const user = useSelector((state: any) => state.user);
    const [inLoad, setInLoad] = useState(false)
    const [formDaat, setFormDaat] = useState({
        name: {
            value: ''
        },
        firstName: {
            value: ''
        },
        lastName: {
            value: ''
        },
        role: {
            value: ''
        },
        password: {
            value: ''
        }
    })

    function handleSubmit(e :any) {

        e.preventDefault()
        setInLoad(true)

        //
        const formDataCollection = new FormData(e.currentTarget)
        // @ts-ignore
        if (formDataCollection.get('email').length === 0 ||
            // @ts-ignore
            formDataCollection.get('firstName').length === 0 ||
            // @ts-ignore
            formDataCollection.get('lastName').length === 0 ||
            // @ts-ignore
            formDataCollection.get('password').length === 0 ||
            // @ts-ignore
            formDataCollection.get('role').length === 0
        ) return toast.error('Fill All field !')


        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const userData = {
            'email': formDataCollection.get('email'),
            'firstName': formDataCollection.get('firstName'),
            'lastName': formDataCollection.get('lastName'),
            'password': formDataCollection.get('password'),
            'role': formDataCollection.get('role'),


        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        const config = {
            method: 'POST',
            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },
            body: formBody,
        };


        // @ts-ignore
        fetch(`${process.env.API_PATH}/api/v1/create-new-user`, config)
            .then(res => res.json())
            .then((data) => {
                toast.dismiss('registerToastId')
                if (data.success) {
                    router.push('/admin/users/list')
                } else {
                    toast.error(data.error)
                }
                console.log(data)
            })
    }

    return <div>
        <form action="" onSubmit={(e) => handleSubmit(e)}>

            <div className={'flex flex-row gap-2 mb-4'}>


                <div className={'basis-1/3'}>
                    <Input label={'email'} required={true} name={'email'} type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Input label={'first name'} required={true} name={'firstName'} type="text"/>
                </div>
                <div className={'basis-1/3'}>
                    <Input label={'last name'} required={true} name={'lastName'} type="text"/>
                </div>


            </div>

            <div className={'flex flex-row gap-2 mb-4'}>
                <div className={'basis-1/2'}>
                    <Input label={'set password'} required={true} name={'password'} type="text"/>
                </div>
                <div className={'basis-1/2'}>
                    <Select
                        defaultSelectedKeys={["admin"]}
                        name="role"
                        // items={animals}
                        label="Select Role"
                        placeholder="Select Role"
                        className="max-w-xs"
                    >
                        <SelectItem key={'admin'}>admin</SelectItem>
                        <SelectItem key={'user'}>user</SelectItem>
                    </Select>
                </div>

            </div>
            <div>
                <Button type={'submit'} size={"sm"} className={`btn-admin`}>
                    Submit
                </Button>
            </div>


        </form>

    </div>

}