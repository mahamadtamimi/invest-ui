import React, {useEffect, useState} from "react";
import {
    Table,
    TableHeader,
    TableColumn,
    TableBody,
    TableRow,
    TableCell,
    User,
    Chip,
    Tooltip,
    ChipProps,
    getKeyValue, Input, Button
} from "@nextui-org/react";
import {EditIcon} from "./EditIcon";
import {DeleteIcon} from "./DeleteIcon";
import {EyeIcon} from "./EyeIcon";
import {SearchIcon} from "@nextui-org/shared-icons";

import Link from "next/link";
import {toast} from "react-toastify";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
// import {columns, users} from "./data";

const statusColorMap: Record<string, ChipProps["color"]> = {
    active: "success",
    paused: "danger",
    vacation: "warning",
};


export default function PlanArchive() {

    const [data, setData] = useState(null)
    const [isLoading, setLoading] = useState(true)
    // @ts-ignore
    const user = useSelector((state) => state.user);

    const router = useRouter()


    useEffect(() => {
        const config = {

            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },

        };
        const url = `${process.env.API_PATH}/api/v1/plans/list-all`
        fetch(url, config)
            .then((res) => res.json())
            .then((data) => {
                setData(data)
                setLoading(false)
            })
    }, [])
    // @ts-ignore
    type Plan = typeof data[0];

    console.log(data)

    const columns = [
        {name: "name", uid: "name"},
        {name: "minimum deposit", uid: "minimumDeposit"},
        {name: "interest rates", uid: "interestRates"},
        {name: "active", uid: "active"},

        {name: "actions", uid: "actions"},
    ];

    function destroyPlan(id: any) {


        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const userData = {
            'id': id,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        const config = {
            method: 'POST',
            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },
            body: formBody,
        };

        // @ts-ignore
        fetch(`${process.env.API_PATH}/api/v1/plan/destroy`, config)
            .then((res) => res.json())
            .then((data) => {

                if (data.success) {
                    toast.dismiss("registerToastId")
                    router.reload()
                }
            })


    }


    function changeActivate(id: any) {
        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const userData = {
            'id': id,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        const config = {
            method: 'POST',
            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },
            body: formBody,
        };

        // @ts-ignore
        fetch(`${process.env.API_PATH}/api/v1/plan/change-activate`, config)
            .then((res) => res.json())
            .then((data) => {

                if (data.success) {
                    toast.dismiss("registerToastId")
                    router.reload()
                }
            })


    }


    const renderCell = React.useCallback((plan: Plan, columnKey: React.Key) => {


        const cellValue = plan[columnKey as keyof Plan];

        switch (columnKey) {
            case "name":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">  {plan.name}</p>

                    </div>

                )
                    ;
            case "minimumDeposit":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">
                            {plan.minimumDeposit} $</p>

                    </div>
                );
            case "interestRates":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">
                            {plan.interestRates} %</p>

                    </div>
                );
            case "active":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-sm capitalize">
                            <Tooltip content={plan.active ? 'Deactive plan' : 'active plan'}>
                                <span onClick={() => changeActivate(plan.id)}
                                      className="text-lg text-default-400 cursor-pointer active:opacity-50">
                                    {plan.active ? 'active' : 'deactive'}
                                </span>
                            </Tooltip>

                        </p>

                    </div>
                );

            case "actions":
                return (
                    <div className="relative flex items-center gap-2">

                        <Tooltip content="Edit plan">
                            <Link href={`/admin/plans/${plan.id}/edit-plan`}
                                  className="text-lg text-default-400 cursor-pointer active:opacity-50">
                                <EditIcon/>
                            </Link>
                        </Tooltip>
                        {/*  <Tooltip color="danger" content="Delete plan">*/}
                        {/* <span onClick={()=>destroyPlan(plan.id)}*/}
                        {/*       className="text-lg text-danger cursor-pointer active:opacity-50">*/}
                        {/*   <DeleteIcon/>*/}
                        {/*</span>*/}
                        {/*  </Tooltip>*/}
                    </div>
                );
            default:
                return cellValue;
        }
    }, []);
    if (isLoading) return <p>Loading...</p>
    if (!data) return <p>No profile data</p>
    return (
        <div className="flex flex-col gap-4">
            <div className="flex justify-end gap-3 items-end">

                <div className="flex gap-3">

                    <Button as={Link} href={'/admin/plans/add-plan'} size={"sm"} className={`btn-one`}>
                        Add New
                    </Button>
                </div>
            </div>


            <Table aria-label="Example table with custom cells">
                <TableHeader columns={columns}>
                    {(column) => (
                        <TableColumn key={column.uid} align={column.uid === "actions" ? "center" : "start"}>
                            {column.name}
                        </TableColumn>
                    )}
                </TableHeader>
                <TableBody items={data}>
                    {(item) => (
                        <TableRow key={
                            // @ts-ignore
                            item.id}>
                            {(columnKey) => <TableCell>{renderCell(item, columnKey)}</TableCell>}
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </div>
    )
        ;
}
