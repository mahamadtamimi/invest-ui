import {Button, Input, user} from "@nextui-org/react";
import {toast, ToastContainer} from "react-toastify";
import styles from "@/styles/auth.module.scss";

import React, {useEffect, useState} from "react";
import 'react-toastify/dist/ReactToastify.css';
import adminStyles from "@/styles/admin.module.scss"

import {fromDate} from "@internationalized/date";
import {useSelector} from "react-redux";

import {useRouter} from "next/router";
import {Textarea} from "@nextui-org/input";


export default function AddPlan() {
    let initialsData: any = {
        name: {
            validate: false,
            label: 'Plan name',
            value: '',
            type: 'text',
            error: ''
        },
        minimumDeposit: {
            validate: false,
            label: 'Minimum deposit',
            value: '',
            type: 'number',
            error: ''
        },
        dailyCredit: {
            validate: false,
            label: 'Daily credit',
            value: '',
            type: 'number',
            error: ''
        },
        withdrawalPeriod: {
            validate: false,
            label: 'Withdrawal period',
            value: '',
            type: 'number',
            error: ''
        },
        interestRates: {
            validate: false,
            label: 'Interest rates',
            value: '',
            type: 'number',
            error: ''
        },
        description: {
            validate: true,
            label: 'description',
            value: '',
            type: 'textarea',
            error: ''
        }

    }

    // @ts-ignore
    const user = useSelector((state) => state.user);
    const router = useRouter()

    const [formData, setFormData] = useState(initialsData)

    const [inLoad, setInLoad] = useState(false)

    async function handleSubmit(event: any) {

        event.preventDefault()
        setInLoad(true)
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        //
        const formDataCollection = new FormData(event.currentTarget)
        const userData = {
            'name': formDataCollection.get('name'),
            'minimumDeposit': formDataCollection.get('minimumDeposit'),
            'dailyCredit': formDataCollection.get('dailyCredit'),
            'withdrawalPeriod': formDataCollection.get('withdrawalPeriod'),
            'interestRates': formDataCollection.get('interestRates'),
            'description' : formDataCollection.get('description'),

        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");
        // @ts-ignore

        const config = {
            method: 'POST',
            headers: {
                Authorization: `bearer ${user.token}`,
                'content-type': 'application/x-www-form-urlencoded',
            },
            body: formBody,
        };

        // @ts-ignore
        fetch(`${process.env.API_PATH}/api/v1/plan/store`, config)
            .then((res) => res.json())
            .then((data) => {
                if (data.success) {
                    router.push('/admin/plans/list')
                }
            })


        setInLoad(false)
    }

    const inputs = Object.keys(formData).map(key => {
            return <>
                {key !== 'description' &&
                    <div key={key} className={adminStyles.half_input}>
                        <Input
                            name={key}
                            value={formData[key]['value']}
                            variant="bordered"

                            color={!formData[key]['validate'] ? "danger" : "success"}
                            errorMessage={!formData[key]['validate'] && formData[key]['error']}
                            onChange={(e) => checkForm(e)}
                            type={formData[key]['type']}
                            data-name={key}
                            label={formData[key]['label']}

                            labelPlacement='inside'
                        />
                    </div>


                }


            </>


        }
    )


    function checkForm(e: React.ChangeEvent<HTMLInputElement>) {
        const newFormData = () => {
            switch (e.target.getAttribute('data-name')) {

                case 'name' :
                    const name = e.target.value
                    const nameValidate = name.length > 2;

                    return {
                        ...formData,
                        name: {
                            ...formData.name,
                            value: e.target.value,
                            validate: nameValidate,
                            error: `${!nameValidate ? 'The name of the plan should be at least 2 letters' : ''}`
                        }
                    }

                case 'minimumDeposit' :
                    const minimumDeposit = e.target.value

                    const minimumDepositValidate = minimumDeposit.length > 0;


                    return {
                        ...formData,
                        minimumDeposit: {
                            ...formData.minimumDeposit,
                            value: e.target.value,
                            validate: minimumDepositValidate,
                            error: `${!minimumDepositValidate ? 'Minimum deposit can not empty !' : ''}`
                        }
                    }
                case 'maximumDeposit' :
                    const maximumDeposit = e.target.value

                    const maximumDepositValidate = maximumDeposit.length > 0;

                    return {
                        ...formData,
                        maximumDeposit: {
                            ...formData.maximumDeposit,
                            value: e.target.value,
                            validate: maximumDeposit,
                            error: `${!maximumDeposit ? 'Maximum deposit can not empty !' : ''}`
                        }
                    }
                case 'dailyCredit' :
                    const dailyCredit = e.target.value

                    const dailyCreditValidate = dailyCredit.length > 0;

                    return {
                        ...formData,
                        dailyCredit: {
                            ...formData.dailyCredit,
                            value: e.target.value,
                            validate: dailyCreditValidate,
                            error: `${!dailyCreditValidate ? 'Daily credit can not empty !' : ''}`

                        }
                    }
                case 'withdrawalPeriod' :
                    const withdrawalPeriod = e.target.value
                    const withdrawalPeriodValidate = withdrawalPeriod.length > 0;

                    return {
                        ...formData,
                        withdrawalPeriod: {
                            ...formData.withdrawalPeriod,
                            value: e.target.value,
                            validate: withdrawalPeriodValidate,
                            error: `${!withdrawalPeriodValidate ? 'Withdrawal period can not empty !' : ''}`
                        }
                    }
                case 'interestRates' :
                    const interestRates = e.target.value
                    const interestRatesValidate = interestRates.length > 0;
                    return {
                        ...formData,
                        interestRates: {
                            ...formData.interestRates,
                            value: e.target.value,
                            validate: interestRatesValidate,
                            error: `${!interestRatesValidate ? 'Interest rates can not empty !' : ''}`
                        }
                    }

                case 'description' :


                    return {
                        ...formData,
                        description: {
                            ...formData.description,
                            value: e.target.value,
                            validate: true,

                        }
                    }
            }
        }

        setFormData(newFormData)
    }


    return <>

        <div>
            <ToastContainer
                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>
        <form onSubmit={handleSubmit} className={adminStyles.form_group}>
            <div className={adminStyles.form_group_section}>
                {inputs}
            </div>


            <Textarea
                className={adminStyles.textAreaInput}
                name={'description'}
                label="Description"
                variant="bordered"
                labelPlacement="outside"
                value={formData.description.value}
                data-name={'description'}
                onChange={(e) => checkForm(e)}
            />

            <div className={styles.form_input_section}>
                <Button
                    isDisabled={!(formData.name.validate && formData.minimumDeposit.validate &&
                        formData.dailyCredit.validate &&
                        formData.withdrawalPeriod.validate && formData.interestRates.validate)}

                    isLoading={inLoad}
                    type={'submit'}
                    className={`btn-one`}
                >
                    add
                </Button>
            </div>
            {/*<SubmitButton />*/}
        </form>

    </>
}