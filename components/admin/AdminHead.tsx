import styles from "@/styles/dashboard.module.scss";
import {useSelector} from "react-redux";

export default function AdminHead(props: any) {
    const user = useSelector( (state: any) => state.user )
    const token = useSelector( (state: any) => state.token )


    return <div className={styles.content__header}>
        <div className={styles.content__header__title}>
            <h1>{props.title}</h1>
            <span> WELCOME BACK, {user.firstName} {user.lastName} </span>
        </div>

        <div className={styles.content__header__icons}>
            <div className={styles.content__header__icons__date}>
                <span> SUNDAY, NOVEMBER 26 </span>
            </div>
            <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                <span className={styles.icon}>

                </span>
            </div>
        </div>
    </div>

}