import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from "@nextui-org/react";
import moment from "moment/moment";
import {useState} from "react";

export default function UserPlan(props: any) {


    return <div className={'w-full'}>
        <p className={'pl-4 mb-2 text-gray-700' +
            ''}>
            active plans :
        </p>
        <Table aria-label="Example static collection table">
            <TableHeader>
                <TableColumn>Initial Amount</TableColumn>
                <TableColumn>Profit</TableColumn>
                <TableColumn>StartAt</TableColumn>
                <TableColumn>ExpireAt</TableColumn>
                <TableColumn>Plan Name</TableColumn>
                <TableColumn>User email</TableColumn>
            </TableHeader>
            <TableBody>
                {// @ts-ignore
                    props.plans.map((item: any) => {
                        // console.log(item.Plan.name)
                        return <TableRow key={item.id}>
                            <TableCell>{item.initAmount} $</TableCell>
                            <TableCell>{item.profit} $</TableCell>
                            <TableCell>{moment(item.startAt).format('YYYY/MM/DD HH:mm:ss')}</TableCell>
                            <TableCell>{moment(item.expireAt).format('YYYY/MM/DD HH:mm:ss')}</TableCell>

                            <TableCell>{item.Plan.name}</TableCell>
                            <TableCell>{item.User.email}</TableCell>


                        </TableRow>

                    })}

            </TableBody>
        </Table>

    </div>
}