import styles from "@/styles/aboutUs.module.scss";
import {BreadcrumbItem, Breadcrumbs} from "@nextui-org/react";
import Image from "next/image";
import slide1 from "@/public/slide-1.png";
import Footer from "@/components/Footer/Footer";
import FaqAll from "@/components/Home/Faq/FaqAll";

export default function FAQComponent() {
    return <>
        <div className={styles.bread_crumbs_sec}>
            <Breadcrumbs>
                <BreadcrumbItem href={'/'}>Home</BreadcrumbItem>
                <BreadcrumbItem>Faq</BreadcrumbItem>
            </Breadcrumbs>

            <FaqAll/>

        </div>
        <Footer/>
    </>
}