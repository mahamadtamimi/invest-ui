import Footer from "@/components/Footer/Footer";
import aboutUsBanner1 from '@/public/aboutUsBanner1.png'
import Image from "next/image";
import styles from '@/styles/aboutUs.module.scss'
import slide1 from '@/public/slide-1.png'
import slide2 from '@/public/slide-2.png'
import slide3 from '@/public/slide-3.png'
import slide4 from '@/public/slide-4.png'
import {BreadcrumbItem, Breadcrumbs} from "@nextui-org/react";

export default function AboutUsComponent() {

    return <>
        <div className={styles.bread_crumbs_sec}>
            <Breadcrumbs>
                <BreadcrumbItem href={'/'}>Home</BreadcrumbItem>
                <BreadcrumbItem>About Us</BreadcrumbItem>
            </Breadcrumbs>

        </div>
        <section className={styles.about}>

            <div className={styles.about_us_image_sec}>
                <Image src={slide1} alt={''}/>


            </div>
            <div className={styles.about_us_descriptions}>


                <h1 className={styles.about__title}>OUR STORY</h1>

                <div className="about__body">
                    <p className="about__body__text">
                        We are a group of experienced professionals in the construction field
                        who began our professional activities in this area in 2017. After 7
                        successful years of operation, in 2024 we decided to expand the
                        scope of our activities by designing an online platform to provide
                        more investment opportunities for the general public.
                        Our goal is to create an online platform to attract investors and allow
                        them to share in the profits from residential and commercial building
                        construction and renovation projects. Through this platform, users
                        can invest their capital with confidence in our experience and
                        expertise in this field, and fairly benefit from the resulting profits.
                        Relying on values such as honesty, transparency, quality, and
                        innovation, we are committed to providing professional services and
                        fairly distributing profits among investors. Our main activities include
                        repairing and renovating dilapidated doors and windows of buildings
                        and companies, interior design, repairing and renovating walls of
                        buildings and companies, and building construction.
                        What sets us apart is the combination of years of experience,
                        technical expertise, innovation, along with complete transparency in
                        the investment process and profit distribution. We execute projects to
                        the highest standards by employing modern methods and using high-
                        quality materials.
                        During this time, we have succeeded in gaining the trust of many
                        customers and investors. We hope that by launching this platform, we
                        can provide attractive and profitable investment opportunities for the
                        general public and distribute the resulting profits in a completely fair
                        and transparent manner among investors.
                        On our platform, users can choose from 5 different investment plans,
                        each with different durations and profit rates based on the type of
                        building construction or renovation project that the investment is
                        used for.
                    </p>
                </div>

            </div>


        </section>


        <Footer/>

    </>


}