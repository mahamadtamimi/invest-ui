import styles from '@/styles/auth.module.scss'
export const BackIcon = () => (
    <svg fill="#D25487" className={styles.back_svg} height="16" width="16" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"

         viewBox="0 0 490 490" >
<polygon points="241.367,64.458 0,245.001 241.367,425.542 176.545,267.963 490,267.963 490,222.026 176.55,222.026 "/>
</svg>


)