function ChatApi(e : any , data : any) : void {


    const userData = {
        // @ts-ignore
        ticketId: data[itemSelected].id,
        // @ts-ignore
        text: chatController.value,
        side: 0
    }


    let formBody = [];
    for (var property in userData) {
        var encodedKey = encodeURIComponent(property);
        // @ts-ignore
        var encodedValue = encodeURIComponent(userData[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    // @ts-ignore
    formBody = formBody.join("&");



    fetch(`${process.env.API_PATH}/api/v1/admin/tickets/answer`, {
        method: "POST",
        headers: {
            // @ts-ignore
            Authorization: `bearer ${user.token}`,
            "Content-Type": 'application/x-www-form-urlencoded',
        },
        // @ts-ignore
        body: formBody
    }).then(res => res.json()).then((data) => {
        // @ts-ignore
        setData(data)
        // @ts-ignore
        setChatController({
            // @ts-ignore
            ...chatController,
            value: '',
            validate: false
        })
    })
}