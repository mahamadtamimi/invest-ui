import style from "@/styles/chat.module.scss";
import React from "react";
import moment from "moment/moment";


export default function AdminChatBody(props:any) {

    return <div className={style.chat_main__all_section} id='massageEmail'>

        {
            // @ts-ignore
            props.chats.TicketBodies.length &&
            // @ts-ignore
            props.chats.TicketBodies.map((ticket) => (

                <span key={ticket.id}>
                    {ticket.side == 0 ? <div
                            key={ticket.id}
                            className={style.chat_main_user_sec}>
                                    <span className={style.chat_main_user}>
                                        <span className={style.chat_label}>
                                            {props.user}
                                        </span>
                                        {ticket.text}


                                        <div className={style.chat_main_user_data}>
                                              <span> {moment(ticket.createdAt).format('YYYY/MM/DD')} </span>
                                            <span> {moment(ticket.createdAt).format('HH:mm:ss')} </span>
                                        </div>

                                    </span>


                        </div> :
                        <div className={style.chat_main_admin_sec}>
                                      <span className={style.chat_main_admin}>
                                              <span className={style.chat_label}>
                                            admin:
                                        </span>
                                          {ticket.text}

                                          <div className={style.chat_main_user_data}>
                                                   <span> {moment(ticket.createdAt).format('YYYY/MM/DD')} </span>
                                            <span> {moment(ticket.createdAt).format('HH:mm:ss')} </span>
                                            </div>

                                        </span>

                        </div>}
                </span>
            ))
        }

    </div>

}
