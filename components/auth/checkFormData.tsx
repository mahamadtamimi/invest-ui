// @ts-ignore
import validator from 'validator';
export default function checkRegisterFormData(e : any, formData : any) {

    switch (e.target.getAttribute('data-name')) {

        case 'firstName' :
            const firstName = e.target.value
            const firstNameValid = firstName.length > 2;

            return {
                ...formData,
                firstName: {
                    ...formData.firstName,
                    value: e.target.value,
                    validate: firstNameValid,
                    error: 'Please enter more than three characters !'
                }
            }

        case 'lastName' :
            const lastName = e.target.value
            const lastNameValid = lastName.length > 2;

            return {
                ...formData,
                lastName: {
                    ...formData.lastName,
                    value: e.target.value,
                    validate: lastNameValid,
                    error: 'Please enter more than three characters !'
                }
            }
        case 'username' :
            const username = e.target.value
            const nameValid = username.length > 2;

            return {
                ...formData,
                username: {
                    ...formData.username,
                    value: e.target.value,
                    validate: nameValid,
                    error: 'Please enter more than three characters !'
                }
            }
        case 'email' :
            const email = e.target.value

            const emailValid =  validator.isEmail(email)

            return {
                ...formData,
                email: {
                    ...formData.email,
                    value: e.target.value,
                    validate: emailValid,
                    error: 'Please enter valid Email !'
                }
            }
        case 'password' :
            const password = e.target.value
            const passwordValid = password.length > 2;

            return {
                ...formData,
                password: {
                    ...formData.password,
                    value: e.target.value,
                }
            }
        case 'rePassword' :
            const rePassword = e.target.value
            const rePasswordValid = rePassword === formData.password.value ;

            return {
                ...formData,
                rePassword: {
                    ...formData.rePassword,
                    value: e.target.value,
                    validate :  rePasswordValid ,

                }
            }
    }
}

