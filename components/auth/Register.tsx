import styles from '@/styles/auth.module.scss'

import Image from "next/image";
import {Button, Input, Link} from "@nextui-org/react";
import {useRouter} from "next/router";
import React, {useState} from "react";
import {initialData} from "@/components/auth/initialData";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {registerSubmit} from "@/servicess/api/registerSubmit";
import checkRegisterFormData from "@/components/auth/checkFormData";
import PasswordStrengthBar from 'react-password-strength-bar';
import {EyeFilledIcon, EyeSlashFilledIcon, MailIcon} from "@nextui-org/shared-icons";
import emailIcon from '@/public/Message.svg'
import profile from '@/public/username.svg'
import {useDispatch, useSelector} from "react-redux";


export default function Register() {
    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);


    const [isVisible, setIsVisible] = useState(false);
    const [isVisibleRePassword, setIsVisibleRePassword] = useState(false);

    const toggleVisibility = () => setIsVisible(!isVisible);
    const toggleVisibilityREenter = () => setIsVisibleRePassword(!isVisibleRePassword);

    // console.log(userData)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    const [inLoad, setInLoad] = useState(false)
    const init: any = initialData




    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(init)
    const [responseData, setResponseData] = useState({});


    async function handleSubmit(event: any) {

        event.preventDefault()
        setInLoad(true)
        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const formDataCollection = new FormData(event.currentTarget)
        const userData = {
            'firstName': formDataCollection.get('firstName'),
            'lastName': formDataCollection.get('lastName'),
            'username': formDataCollection.get('email'),
            'email': formDataCollection.get('email'),
            'password': formDataCollection.get('password'),
            'from': router.query.from,

        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");

        const responseData = await fetch(`${process.env.API_PATH}/api/v1/auth/register`,
            {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded'

                },
                redirect: "follow",
                referrerPolicy: "no-referrer",
                // @ts-ignore
                body: formBody, // body data type must match "Content-Type" header
            });


        if (responseData.status == 200) {

            const data = await responseData.json()



            toast.dismiss('registerToastId')
            toast.success('Register was success!')


            dispatch({type: 'LOGIN', payload: {user: data}});


            router.push('/auth/otp')


        } else if (responseData.status == 409) {
            toast.dismiss('registerToastId')
            toast.error('Email early exited !')
            const updateDate: any = {
                ...formData,
                email: {
                    ...formData.email,
                    validate: false,
                    error: 'Email early exited !'
                }
            }
            setFormData(updateDate)

        } else if (responseData.status == 500) {
            toast.dismiss('registerToastId')
            toast.error('Server Error ! please try again!')
        }
        setInLoad(false)
    }


// eslint-disable-next-line react-hooks/rules-of-hooks
    function checkForm(e: any) {
        const data = checkRegisterFormData(e, formData)


        setFormData(data)
    }


    return <>

        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>
        {user &&
            <p className={styles.you_have_early_acount}>are you {user.firstName} {user.lastName} ? you have early
                account ! <Link href={'/auth/login'}>Login</Link></p>}
        <form onSubmit={handleSubmit} className={styles.form_group}>
            <div className={styles.form_input_section}>
                <Input
                    name={'email'}
                    value={formData.email.value}
                    variant="bordered"
                    color={!formData.email.validate ? "danger" : "success"}
                    errorMessage={!formData.email.validate && formData.email.error}
                    onChange={(e) => checkForm(e)}
                    type="email"
                    data-name={'email'}
                    label="Email"
                    endContent={
                        <Image src={emailIcon} alt={''}/>
                    }
                    labelPlacement='inside'
                />
            </div>
            <div className={styles.form_input_section}>
                <Input
                    name={'firstName'}
                    value={formData.firstName.value}
                    variant="bordered"
                    // isInvalid={formData.firstName.validate}
                    color={!formData.firstName.validate ? "danger" : "success"}
                    errorMessage={!formData.firstName.validate && formData.firstName.error}
                    onChange={(e) => checkForm(e)}
                    type="text"
                    data-name={'firstName'}
                    label="First Name"
                    endContent={
                        <Image src={profile} alt={''}/>
                    }
                    labelPlacement='inside'
                />
            </div>
            <div className={styles.form_input_section}>
                <Input
                    name={'lastName'}
                    value={formData.lastName.value}
                    variant="bordered"
                    // isInvalid={formData.firstName.validate}
                    color={!formData.lastName.validate ? "danger" : "success"}
                    errorMessage={!formData.lastName.validate && formData.lastName.error}
                    onChange={(e) => checkForm(e)}
                    type="text"
                    data-name={'lastName'}
                    label="Last Name"
                    endContent={
                        <Image src={profile} alt={''}/>
                    }
                    labelPlacement='inside'
                />
            </div>


            <div className={styles.form_input_section_password}>
                <Input
                    name={'password'}
                    value={formData.password.value}
                    variant="bordered"
                    // isInvalid={formData.firstName.validate}
                    color={!formData.password.validate ? "danger" : "success"}

                    onChange={(e) => checkForm(e)}

                    data-name={'password'}
                    label="Password"
                    errorMessage={formData.password.error}
                    labelPlacement='inside'


                    endContent={
                        <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                            {isVisible ? (
                                <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                            ) : (
                                <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                            )}
                        </button>
                    }
                    type={isVisible ? "text" : "password"}

                />
                <div className={styles.form_input_section_validator}>
                    <PasswordStrengthBar
                        barColors={['#ddd', '#ef4836', '#f6b44d', '#2bef79', '#25c281']}
                        password={formData.password.value} onChangeScore={(score, feedback) => {
                        if (score < 3) {
                            const data = {
                                ...formData,
                                password: {
                                    ...formData.password,
                                    value: formData.password.value,
                                    validate: false,
                                    error: feedback.warning
                                }

                            }

                            setFormData(data)
                        }
                        if (score >= 3) {
                            const data = {
                                ...formData,
                                password: {
                                    ...formData.password,
                                    value: formData.password.value,
                                    validate: true,
                                    error: ''
                                }

                            }

                            setFormData(data)
                        }


                    }}/>
                </div>

            </div>
            <div className={styles.form_input_section}>
                <Input
                    name={'rePassword'}
                    value={formData.rePassword.value}
                    variant="bordered"
                    // isInvalid={formData.firstName.validate}
                    color={!formData.rePassword.validate ? "danger" : "success"}

                    onChange={(e) => checkForm(e)}

                    data-name={'rePassword'}
                    label="Re Enter Password"
                    errorMessage={!formData.rePassword.validate && formData.rePassword.error}
                    labelPlacement='inside'


                    endContent={
                        <button className="focus:outline-none" type="button" onClick={toggleVisibilityREenter}>
                            {isVisibleRePassword ? (
                                <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                            ) : (
                                <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                            )}
                        </button>
                    }
                    type={isVisibleRePassword ? "text" : "password"}

                />


            </div>
            <div className={styles.form_input_section}>
                <Button
                    isLoading={inLoad}
                    type={'submit'}
                    isDisabled={!(formData.firstName.validate && formData.lastName.validate && formData.email.validate && formData.password.validate && formData.rePassword.validate)}

                    className={`btn-one   
                    ${!(formData.firstName.validate && formData.lastName.validate && formData.email.validate && formData.password.validate && formData.rePassword.validate) ? 'disabled-btn' : ''}
                    
                    `}>
                    REGISTRATION
                </Button>
            </div>
            {/*<SubmitButton />*/}
        </form>

    </>


}