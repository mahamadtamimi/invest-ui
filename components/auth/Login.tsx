import styles from '@/styles/auth.module.scss'
import logo from '@/public/log.svg'
import Image from "next/image";
import {Button, Input, Link} from "@nextui-org/react";
import banner from '@/public/login-banner.png'
import {toast, ToastContainer} from "react-toastify";
import React, {useEffect, useState} from "react";
import {EyeFilledIcon, EyeSlashFilledIcon} from "@nextui-org/shared-icons";
import 'react-toastify/dist/ReactToastify.css';
import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import validator from "validator";
import {BackIcon} from "@/components/BackIcon";

export default function Login() {
    const init = {
        email: {
            value: '',
            validate: false,
            error: ''
        },
        password: {
            value: ''
        }
    }
    const router = useRouter()

    const [formData, setFormData] = useState(init)
    const [inLoad, setInLoad] = useState(false)
    const [isVisiblePassword, setIsVisiblePassword] = useState(false)


    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);


    useEffect(() => {
        if (user) {
            setFormData({
                ...formData,
                email: {
                    ...formData.email,
                    value: user.email,
                    validate: true,
                }
            })
        }
    }, []);

    async function handleSubmit(event: any) {
        event.preventDefault()
        setInLoad(true)
        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const formDataCollection = new FormData(event.currentTarget)
        const userData = {
            'username': formDataCollection.get('email'),
            'password': formDataCollection.get('password'),
        }

        console.log('========================')
        console.log(userData)

        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        const responseData = await fetch(`${process.env.API_PATH}/api/v1/auth/login`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded'
                },
                // @ts-ignore
                body: formBody, // body data type must match "Content-Type" header
            });

        console.log(responseData)

        if (responseData.status == 200) {

            const data = await responseData.json()


            toast.dismiss('registerToastId')
            toast.success('Register was success!')


            dispatch({type: 'LOGIN', payload: {user: data}});


            switch (data.redirect) {
                case 'Admin':
                    router.push('/admin')
                    break
                case 'User':
                    router.push('/auth/dashboard')
                    break
            }


        } else if (responseData.status == 401) {
            toast.dismiss('registerToastId')
            toast.error('Invalid email or password !')
            const updateDate: any = {
                ...formData,
                email: {
                    ...formData.email,
                }
            }
            // setFormData(updateDate)

        }
        setInLoad(false)

    }


    function updateFormData(e: any) {
        const target = e.target.getAttribute('data-role')

        switch (target) {
            case 'userName' :
                const email = e.target.value
                const emailValid = validator.isEmail(email)

                setFormData({
                        ...formData,
                        email: {
                            ...formData.email,
                            value: email,
                            validate: emailValid,
                            error: 'Please enter valid Email !'

                        }
                    }
                )
                break

        }
    }


    function toggleVisibilityREenter() {
        setIsVisiblePassword(!isVisiblePassword)
    }

    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>


        <div className={styles.signin}>
            <div className={styles.signin__content}>
                <span className={styles.back_link}><Link href={'/'}> <BackIcon/>  <span>back</span> </Link></span>

                <div className={styles.signin__content__left}>
                    <div className={styles.signin__content__left__title}>
                    <span>

                        <Image src={logo} alt={''}/>
                    </span>

                    </div>
                    <div className={styles.signin__content__left__content}>
                        <div className={styles.signin__content__left__content__title}>
                            <h1>Sign in</h1>

                        </div>
                        <div className={styles.signin__content__left__form}>
                            <form onSubmit={handleSubmit} className={styles.form_group}>
                                <div className={styles.input_group}>
                                    <Input
                                        name={'email'}
                                        value={formData.email.value}
                                        variant="bordered"
                                        onChange={(e) => updateFormData(e)}
                                        isInvalid={!formData.email.validate}
                                        color={!formData.email.validate ? "danger" : "success"}
                                        errorMessage={!formData.email.validate && formData.email.error}
                                        type="text"
                                        data-role={'userName'}
                                        label="Email"

                                        labelPlacement='inside'
                                    />
                                </div>
                                <div className={styles.input_grou}>
                                    <Input
                                        name={'password'}

                                        variant="bordered"
                                        data-name={'password'}
                                        label="password"

                                        labelPlacement='inside'
                                        endContent={
                                            <button className="focus:outline-none" type="button"
                                                    onClick={toggleVisibilityREenter}>
                                                {isVisiblePassword ? (
                                                    <EyeSlashFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                ) : (
                                                    <EyeFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                )}
                                            </button>
                                        }
                                        type={isVisiblePassword ? "text" : "password"}
                                    />
                                </div>

                                <div className={styles.form_group__forget__seave}>
                                    <div className={styles.form_group__seave}>

                                    </div>
                                    <div className={styles.form_group__forge}>
                                        <Link href={'/auth/forgot-password'}
                                              className={`${styles.forget__pass_link} tx-red`}>FORGET
                                            PASSWORD
                                            ?</Link>
                                    </div>
                                </div>

                                <div className={styles.input_grou}>
                                    <Button type={'submit'} className={'btn-one'}>
                                        Login
                                    </Button>
                                </div>


                            </form>
                            <div className={styles.input_grou}>
                                <Button variant={'bordered'} as={Link} href={'/auth/register'} className={'btn-two mt-4 text-center'}>
                                    Register
                                </Button>
                            </div>

                        </div>

                    </div>
                </div>

                <div className={styles.signin__content__rigth}>
                    <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
                </div>
            </div>
        </div>
    </>


}