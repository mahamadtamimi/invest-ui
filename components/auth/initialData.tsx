export const initialData = {
    firstName: {
        value: '',
        validate: false,
        error: 'Please enter more than three characters !'
    },
    lastName: {
        value: '',
        validate: false,
        error: 'Please enter more than three characters !'
    },
    username: {
        value: '',
        validate: false,
        error: 'Please enter more than three characters !'
    },
    email: {
        value: '',
        validate: false,
        error: 'Please enter valid Email !'
    },
    password: {
        value: '',
        validate: false,
        error: 'Please enter more than three characters !'
    },
    rePassword: {
        value: '',
        validate: false,
        error: 'Mismatch of passwords !'
    },
}