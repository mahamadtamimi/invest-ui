import {BreadcrumbItem, Breadcrumbs} from "@nextui-org/react";

export default function BreadcrumbsComponent(){



    return  <Breadcrumbs>
        <BreadcrumbItem>Home</BreadcrumbItem>
        <BreadcrumbItem>Music</BreadcrumbItem>
        <BreadcrumbItem>Artist</BreadcrumbItem>
        <BreadcrumbItem>Album</BreadcrumbItem>
        <BreadcrumbItem>Song</BreadcrumbItem>
    </Breadcrumbs>
}