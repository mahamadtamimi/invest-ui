import {Link} from "@nextui-org/link";
import Image from 'next/image'
import logo from '@/public/log.svg'


import {
    Navbar,
    NavbarBrand,
    NavbarContent,
    NavbarItem,
    NavbarMenuToggle,
    NavbarMenu,
    NavbarMenuItem, Button, Badge, User
} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";
import {NotificationIcon} from "@/components/MainMenu/NotificationIcon";
import React, {useEffect, useState} from "react";
import styles from "@/styles/MainMenu.module.scss";
import {TetherIcon} from "@/components/Dashboard/Dashboard/TetherIcon";
import {toast} from "react-toastify";
import {HomeIcon} from "@/components/Dashboard/Sidebar/HomeIcon";
import {PurchaseIcon} from "@/components/Dashboard/Sidebar/PurchaseIcon";
import {TransactionIcon} from "@/components/Dashboard/Sidebar/TransactionIcon";
import {SettingIcon} from "@/components/Dashboard/Sidebar/SettingIcon";
import {SupportIcon} from "@/components/Dashboard/Sidebar/SupportIcon";
import style from "@/styles/chat.module.scss";
import {SignOutIcon} from "@/components/Dashboard/Sidebar/SignOutIcon";
import {useRouter} from "next/router";
import {TwoFactorIcon} from "@/components/Dashboard/Sidebar/TwoFactorIcon";
import {TwoFactorIconGreen} from "@/components/Dashboard/Sidebar/TwoFactorIconGreen";
import {ChangePasswordIcon} from "@/components/Dashboard/Sidebar/ChangePasswordIcon";


export default function DashboradMenu() {
    const router = useRouter()

    // @ts-ignore
    const user = useSelector((state) => state.user);
    const [userWallet, setUserWallet] = useState()

    // useEffect(() => {
    //     router.push('/auth/login')
    //
    //     return <>404</>
    // }, []);




    const dispatch = useDispatch()

    useEffect(() => {
        {
            user &&
            fetch(`${process.env.API_PATH}/api/v1//auth/me`, {
                headers: {
                    Authorization: `bearer ${user?.token}`
                }
            })
                .then((res) => {
                    if (res.status === 401 || res.status === 404) {

                        dispatch({type: 'LOGOUT'});
                        return
                    }
                    if (res.status == 500) {

                        toast.error('Server Error !')
                        return
                    }


                    return res.json()
                })
                .then((data) => {

                    dispatch({type: 'LOGIN', payload: {user: data}});
                })
        }

    }, [])

    const [ticket, setTicket] = useState()
    const [pending, setPending] = useState(true)
    const [newMassage, setNewMassage] = useState(false)
    useEffect(() => {


        fetch(`${process.env.API_PATH}/api/v1/user/tickets`, {
            headers: {
                Authorization: `bearer ${user?.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setTicket(data)
                setPending(false)
            })


    }, [])


    useEffect(() => {
        if (!pending) {
            // @ts-ignore
            if (ticket.length > 0) {
                {
                    // @ts-ignore
                    ticket.map((item, index) => {

                        const newMassage = item.TicketBodies.filter((item: any) => {
                            return (item.side == 1 && item.seen == false)
                        }).length


                        if (newMassage) setNewMassage(true)
                    })
                }


            }
        }

    }, [ticket, pending]);


    return <>
        <Navbar maxWidth={'full'}
                height={'4rem'}
                isBordered={true}
                classNames={{
                    base: 'base',
                    brand: 'dashbord_menu_logo',
                }}
        >
            <NavbarContent className="sm:hidden" justify="start">
                <NavbarMenuToggle/>
            </NavbarContent>


            <NavbarBrand className={'text__class'}>
                <Image src={logo} alt=''/>

            </NavbarBrand>
            <NavbarContent className="hidden sm:flex gap-4" justify="start">
                <NavbarItem>
                    <Link color={'foreground'} href="/">
                        Home
                    </Link>
                </NavbarItem>
                <NavbarItem>
                    <Link color={'foreground'} href="/about-us">
                        About Us
                    </Link>
                </NavbarItem>
                <NavbarItem>
                    <Link color={'foreground'} href="/faq">
                        Faq
                    </Link>
                </NavbarItem>
                <NavbarItem>
                    <Link color={'foreground'} href="/plans">
                        Plans
                    </Link>
                </NavbarItem>
            </NavbarContent>
            <NavbarContent justify="end">
                {user ?
                    <>

                        <NavbarItem className="hidden lg:flex">
                            <Link href="/auth/dashboard">
                                <User
                                    name={`${user.firstName} ${user.lastName}`}
                                    description={(
                                        <Link href="/auth/dashboard" size="sm" isExternal>
                                            @ Go Dashboard
                                        </Link>
                                    )}
                                    avatarProps={{
                                        // src: "https://avatars.githubusercontent.com/u/30373425?v=4"
                                    }}
                                />
                            </Link>
                        </NavbarItem>
                        <NavbarItem className="">

                            <Link href={'/auth/dashboard/withdrawal'} className={styles.main_menu_wallet}>

                                <TetherIcon/>
                                <span className={styles.wallet__in__price}>
                                            {user.wallet}$



                                </span>


                            </Link>

                        </NavbarItem>
                        <NavbarItem className="hidden lg:flex">
                            <Button color={'primary'} as={Link} className={'text-white'}
                                    href="/auth/dashboard/purchase-plans">Purchase Plan</Button>
                        </NavbarItem>


                    </>

                    :
                    <>
                        <NavbarItem className="">
                            <Link className={'login-link-head'} href="/auth/login">Login</Link>
                        </NavbarItem>
                        <NavbarItem className={'hidden lg:flex'}>
                            <Button as={Link} className={'text-white'} color={'primary'} href={'/auth/register'}>
                                Sign Up
                            </Button>
                        </NavbarItem>
                    </>

                }


            </NavbarContent>


            <NavbarMenu>
                <div className={styles.mobile_user_item}>
                    {user ?
                        <>

                            <div className={styles.mobile_user_cart}>

                                <Link href="/auth/dashboard">
                                    <User
                                        name={`${user.firstName} ${user.lastName}`}
                                        description={(
                                            <Link href="/auth/dashboard" size="sm" isExternal>
                                                @ Go Dashboard
                                            </Link>
                                        )}
                                        avatarProps={{
                                            // src: "https://avatars.githubusercontent.com/u/30373425?v=4"
                                        }}
                                    />
                                </Link>
                            </div>
                            {/*<div className={styles.wallet_cart}>*/}

                            {/*    <Link href={'/auth/dashboard/withdrawal'}*/}
                            {/*          className={styles.main_menu_wallet}>*/}

                            {/*        <TetherIcon/>*/}
                            {/*        <span className={styles.wallet__in__price}>*/}
                            {/*                {*/}

                            {/*                    new Intl.NumberFormat('en-IN', {maximumSignificantDigits: 3}).format(*/}
                            {/*                        // @ts-ignore*/}
                            {/*                        user.wallet,*/}
                            {/*                    )}$*/}


                            {/*                    </span>*/}


                            {/*    </Link>*/}

                            {/*</div>*/}
                            <div className={styles.mob_btn}>
                                <Button color={'primary'} as={Link} className={'text-white w-full'}
                                        href="/auth/dashboard/purchase-plans">Purchase Plan</Button>
                            </div>


                        </>

                        :
                        <>
                            <NavbarItem className="hidden lg:flex">
                                <Link className={'login-link-head'} href="/auth/login">Login</Link>
                            </NavbarItem>
                            <NavbarItem>
                                <Button as={Link} className={'btn btn-dark'} href={'/auth/register'}
                                        variant="flat">
                                    Sign Up
                                </Button>
                            </NavbarItem>

                        </>

                    }


                </div>
                <NavbarMenuItem>
                    <div className={styles.sidebar__content}>
                        <ul className={styles.sidebar__content__links}>
                            <li className={styles.links__item}>
                                <Link href="/auth/dashboard" className={`${styles.link}`}>


                                    Dashboard
                                </Link>
                            </li>


                            <li className={styles.links__item}>
                                <Link href="/auth/dashboard/withdrawal" className={styles.link}>


                                    Withdrawal
                                </Link>
                            </li>
                            <li className={styles.links__item}>
                                <Link href="/auth/dashboard/general-info" className={styles.link}>


                                    General Info
                                </Link>
                            </li>

                            <li className={styles.links__item}>
                                <Link href="/auth/dashboard/ticket" className={styles.link}>


                                    Tickets
                                    {newMassage ?
                                        <span className={style.have_new_massage__menu}>{newMassage}</span> : ''}
                                </Link>
                            </li>

                            <li className={styles.links__item}>
                                <Link
                                    href="/"
                                    className={styles.link}>
                                    Home
                                </Link>
                            </li>
                            <li className={styles.links__item}>
                                <Link


                                    href="/about-us"
                                    className={styles.link}>
                                    About Us
                                </Link>
                            </li>
                            <li className={styles.links__item}>
                                <Link


                                    href="/faq"
                                    className={styles.link}>
                                    FAQ
                                </Link>
                            </li>
                            <li className={styles.links__item}>
                                <Link


                                    href="/plans"
                                    className={styles.link}>
                                    Plans
                                </Link>
                            </li>


                            {
                                user.lock ?
                                    <li className={styles.links__item}>
                                        <Link href="/auth/dashboard/disble-two-verification"
                                              className={`${styles.link}  ${styles.links__item_text_red}`}>


                                            Disable 2FA
                                        </Link>
                                    </li>
                                    :
                                    <li className={`${styles.links__item}`}>
                                        <Link href="/auth/dashboard/two-verification"
                                              className={`${styles.link}  ${styles.links__item_text_danger}`}>


                                            Enable 2FA
                                        </Link>
                                    </li>
                            }

                            <li className={styles.links__item}>
                                <Link href="/auth/dashboard/change-password" className={styles.link}>


                                    Change Password
                                </Link>
                            </li>


                        </ul>
                    </div>

                    <div className={styles.sidebar__footer}>
                        <ul className={styles.sidebar__content__links}>


                            <li className={styles.links__item}>

                                <Link
                                    href={'/auth/dashboard/sign-out'}
                                    // onClick={signUpHandle}
                                    className={styles.link}>

                                    Sign Out
                                </Link>

                            </li>
                        </ul>


                    </div>
                </NavbarMenuItem>

            </NavbarMenu>
        </Navbar>

    </>

        ;


}