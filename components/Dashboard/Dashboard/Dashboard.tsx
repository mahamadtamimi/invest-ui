import styles from '@/styles/dashboard.module.scss'
// import './style.scss'

import testMony from '@/public/testMony.svg'
import Image from "next/image";
import {Button, Link} from "@nextui-org/react";
import DashboardLayout from "@/layouts/dashboard";


import {useSelector} from "react-redux";
import moment from "moment";

import UserTransaction from "@/components/Dashboard/Dashboard/UserTransaction";

import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";


export default function Dashboard() {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector((state: any) => state.user);
// eslint-disable-next-line react-hooks/rules-of-hooks
    const lock = useSelector((state: any) => state.lock);

    // @ts-ignore
    const router = useRouter();

    // if (!user.token) { return router.push('/auth/login')}


    if (user.lock && lock) {
        router.push("/auth/google-auth-token");
    }

    // return null
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [userActivePlan, setUserActivePlan] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [userTransactions, setUserTransactions] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [userWallet, setUserWallet] = useState()

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/user/plan`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {

                setUserActivePlan(data)

            })


        fetch(`${process.env.API_PATH}/api/v1/user/transactions`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {

                setUserTransactions(data)

            })

    }, [])


    // @ts-ignore
    return <>
        <DashboardHead title={'Dashboard'}/>


        <div className={styles.content__wallets}>
            <div className={styles.content__wallets__title}>
                <span> Plan</span>
            </div>


            <div className={styles.content__wallets__cards}>

                {userActivePlan &&
                    // @ts-ignore
                    userActivePlan.map((item) => {

                        const str = item.startAt;
                        const date = moment(str);
                        const dateComponent = date.utc().format('YYYY/MM/DD hh:mm:ss')


                        const dailyRate = item.Plan.interestRates / item.Plan.dailyCredit


                        return <div key={item.id} className={`${styles.wallets__card} bg-red no-border`}>
                            <div>
                                <Image src={testMony} alt=''/>
                            </div>
                            <div className={`${styles.wallets__card__content} txWhite`}>
                                <span className={`${styles.wallets__card__title} text-white`}> {item.Plan.name}
                                </span>
                                <span
                                    className={`${styles.wallets__card__title} text-white`}> $ {(item.initAmount + item.profit).toFixed(2)} </span>
                                <div className={styles.wallets__card__namad}>
                                        <span
                                            className={`${styles.namad} tx-black`}> initials : ${item.initAmount} </span>
                                    <span
                                        className={`${styles.namad} tx-black`}> start : {dateComponent} </span>

                                    <div className={`${styles.namad__text} text-white`}>
                                        + {((item.profit / item.initAmount) * 100).toFixed(2)} %
                                    </div>
                                </div>
                            </div>
                        </div>
                    })}


                <Button
                    className={`${styles.wallets__card__add} border-roted  ${styles.wallets__card__content} tx-gray`}
                    as={Link} href={'/auth/dashboard/purchase-plans'}>+ Purchase plans</Button>


            </div>


        </div>

        <div className={styles.content__chrat}>
            <div className={styles.content__chart__title}>
                <h1>
                    INCOME AND EXPENSE REPORTz
                </h1>
            </div>
        </div>
        {userTransactions &&

            <div className={styles.content__transaction_table}>

                <UserTransaction data={userTransactions}/>

            </div>

        }

    </>
}