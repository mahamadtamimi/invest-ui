export const PlusIcon = () => (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0_257_825)">
            <path d="M9.02734 6.34692V11.7083" stroke="white" strokeWidth="1.5" strokeLinecap="round"
                  strokeLinejoin="round"/>
            <path d="M11.7106 9.02759H6.34375" stroke="white" strokeWidth="1.5" strokeLinecap="round"
                  strokeLinejoin="round"/>
            <path fillRule="evenodd" clipRule="evenodd"
                  d="M1.72461 9.02773C1.72461 3.55115 3.55066 1.7251 9.02725 1.7251C14.5038 1.7251 16.3298 3.55115 16.3298 9.02773C16.3298 14.5043 14.5038 16.3303 9.02725 16.3303C3.55066 16.3303 1.72461 14.5043 1.72461 9.02773Z"
                  stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
        </g>
        <defs>
            <clipPath id="clip0_257_825">
                <rect width="18" height="18" fill="white"/>
            </clipPath>
        </defs>
    </svg>


)