import React, {useEffect} from "react";
import {
    Table,
    TableHeader,
    TableColumn,
    TableBody,
    TableRow,
    TableCell,
    Pagination,
    getKeyValue, useSwitch, Button, Link
} from "@nextui-org/react";
import moment from "moment";
import {ArrowTopIcon} from "@/components/Dashboard/Dashboard/ArrowTopIcon";
import {PlusIcon} from "@/components/Dashboard/TicketList/PlusIcon";


export default function TicketList(props:any) {
    const [page, setPage] = React.useState(1);


    const rowsPerPage = 4;

    const pages = Math.ceil(props.data.length / rowsPerPage);

    const items = React.useMemo(() => {
        const start = (page - 1) * rowsPerPage;
        const end = start + rowsPerPage;

        return props.data.slice(start, end);
    }, [page, props.data]);



    return (<>
            <div>
                <Button color={'primary'}
                        endContent={<PlusIcon/>}
                        className={'text-white mt-4 mb-2'} as={Link} href={'/auth/dashboard/new-ticket'}>
                    New Ticket
                </Button>
            </div>
            <p>LIST OF TICKETS</p>
            <Table
                aria-label="Example table with client side pagination"
                bottomContent={
                    <div className="flex w-full justify-center">
                        <Pagination
                            isCompact
                            showControls
                            showShadow
                            color="primary"
                            page={page}
                            total={pages}
                            onChange={(page) => setPage(page)}
                        />
                    </div>
                }
                classNames={{
                    wrapper: "min-h-[222px]",
                }}
            >
                <TableHeader>
                    <TableColumn key="amount">Amount</TableColumn>
                    <TableColumn key="role">Description</TableColumn>
                    <TableColumn key="type">Type</TableColumn>

                    <TableColumn key="date">Date</TableColumn>
                    <TableColumn key="status">Status</TableColumn>

                </TableHeader>
                <TableBody>
                    {items.map((item:any) => (
                        <TableRow key={item.id}>
                            <TableCell>{item.amount}$</TableCell>
                            <TableCell>{item.description}</TableCell>
                            <TableCell>{item.type}</TableCell>

                            <TableCell>{moment(item.createdAt).format('YYYY/MM/DD  HH:mm:ss')}</TableCell>
                            <TableCell>{item.status}</TableCell>


                        </TableRow>

                    ))
                    }
                </TableBody>
            </Table>
    </>

    );
}