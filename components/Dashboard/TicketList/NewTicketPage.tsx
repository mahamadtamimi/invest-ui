import TicketList from "@/components/Dashboard/TicketList/TicketList";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import styles from '@/styles/dashboard.module.scss'
import {Button, Input} from "@nextui-org/react";
import {Textarea} from "@nextui-org/input";
import {useState} from "react";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";

export default function NewTicketPage() {
    // @ts-ignore
    const user = useSelector((state) => state.user)

    const router = useRouter()

    const [formData, setFormData] = useState({
        subject: {
            value: '',
            validate: false
        },
        description: {
            value: '',
            validate: false
        }

    });

    function handleSubmit(e: any) {
        e.preventDefault();
        const data = new FormData(e.target);

        const userData = {
            'subject': data.get('subject'),
            'description': data.get('description'),
            'user': data.get('email'),
            'anonymous': false,

        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");

        fetch(`${process.env.API_PATH}/api/v1/new/ticket`, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded'
            },
            // @ts-ignore
            body: formBody, // body data type must match "Content-Type" header
        }).then(res => res.json()).then((data) => {
            if (data.success) {
                router.push('/auth/dashboard/ticket')
            }
        })

    }

    function updateFormData(e: any) {

        switch (e.target.getAttribute("data-role")) {
            case 'subject' :
                const subjectValidate = e.target.value.length > 4;

                setFormData({
                    ...formData,
                    subject: {
                        ...formData.subject,
                        value: e.target.value,
                        validate: subjectValidate
                    }
                })

                break;

                case 'description' :

                    const descriptionValidate = e.target.value.length > 0;

                    setFormData({
                        ...formData ,
                        description: {
                            ...formData.description,
                            value: e.target.value,
                            validate: descriptionValidate
                        }
                    })

                break

        }

    }

    return <>

        <DashboardHead title={'Ticket'}/>
        <div className={styles.ticket_sec}>
            <form action="" onSubmit={(e) => handleSubmit(e)}>
                <input type={'hidden'} name={'email'} value={user.email}/>
                <Input
                    value={formData.subject.value}
                    isInvalid={!formData.subject.validate}
                    data-role={'subject'}
                    onChange={(e) => updateFormData(e)}
                    errorMessage={!formData.subject.validate && 'Please enter more than 5 characters'}
                    name={'subject'}
                    label={"Subject"}
                    className={'mb-4'}
                    labelPlacement="outside"
                    placeholder={'Enter subject'}/>

                <Textarea
                    onChange={(e) => updateFormData(e)}
                    isInvalid={!formData.description.validate}
                    errorMessage={!formData.description.validate && ''}
                    value={formData.description.value}
                    data-role={'description'}
                    label="Description"
                    labelPlacement="outside"
                    name={'description'}
                    placeholder="Enter your description"
                    className={'mb-4'}
                />

                <Button type={"submit"} isDisabled={!(formData.subject.validate && formData.description.validate)}
                        color={'primary'}
                        className={`${!(formData.subject.validate && formData.description.validate) && styles.btn__disable} text-white w-full`}>
                    send
                </Button>

            </form>

        </div>
    </>

}