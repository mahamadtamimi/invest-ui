import dashboardStyles from "@/styles/dashboard.module.scss";
import Image from "next/image";
import testMony from "@/public/testMony.svg";
import {Button, Checkbox, Input, Link, Slider, Spinner} from "@nextui-org/react";

import styles from '@/styles/home.module.scss'
import React, {useEffect, useLayoutEffect, useState} from "react";

import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";


export default function TankYou() {
    const router = useRouter();

    const [transactionResult, setTransactionResult] = useState({})
    const [isLoad, setIsLoad] = useState(true)

    const [barClass, setBarClass] = useState('')
    // @ts-ignore
    const user = useSelector((state) => state.user);

    const [useTimer, setUseTimer] = useState({
        use: false,
        timer: 30
    })


    console.log(user)
    console.log('===========')

    // console.log(user.token)


    useEffect(() => {
        // if (!user.token) return
        if (!router.query.NP_id) return
        const form = {
            'authority': router.query.NP_id,
        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        // @ts-ignore

        fetch(`${process.env.API_PATH}/api/v1/verify`, {
            method: "POST",
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                Authorization: `bearer ${user.token}`
            },
            // @ts-ignore
            body: formBody,
        }).then((res) => res.json())
            .then((data) => {
                console.log(data)
                setTransactionResult(data)
                setIsLoad(false)
                {
                    if (data.success == true) {
                        setBarClass(dashboardStyles.green)
                        router.push('/auth/dashboard');


                    } else if (data.status === 'confirmed') {
                        setUseTimer({
                            ...useTimer,
                            use: true,
                        })
                    } else {
                        setBarClass(dashboardStyles.red)
                    }

                }

            })


    }, [router, user])
    useEffect(() => {
        if (useTimer.use == true && useTimer.timer > 0) {
            const interval = setInterval(() => {
                setUseTimer({
                    ...useTimer,

                    timer: useTimer.timer - 1
                })
            }, 1000);
            return () => clearInterval(interval);
        }

        if (useTimer.use === true && useTimer.timer == 0) {

            console.log('test')

            // if (!user.token) return
            if (!router.query.NP_id) return
            const form = {
                'authority': router.query.NP_id,
            }


            let formBody = [];
            for (var property in form) {
                var encodedKey = encodeURIComponent(property);
                // @ts-ignore
                var encodedValue = encodeURIComponent(form[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            // @ts-ignore
            formBody = formBody.join("&");
            console.log('new log')

            // @ts-ignore

            fetch(`${process.env.API_PATH}/api/v1/verify`, {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            }).then((res) => res.json())
                .then((data) => {
                    console.log(data)
                    setTransactionResult(data)
                    setIsLoad(false)
                    {
                        if (data.success == true) {
                            setUseTimer({
                                ...useTimer,
                                use: false,
                                timer: 30
                            })
                            setBarClass(dashboardStyles.green)
                            router.push('/auth/dashboard');


                        } else if (data.status === 'confirmed') {
                            setUseTimer({
                                ...useTimer,
                                use: true,
                                timer: 30
                            })
                        } else {
                            setBarClass(dashboardStyles.red)
                            setUseTimer({
                                ...useTimer,
                                use: false,
                                timer: 30
                            })
                        }

                    }

                })
        }

    }, [router, user, useTimer])

    console.log(useTimer)

    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>
        <DashboardHead title={'Dashboard'}/>
        <div className={styles.plans_sec}>
            <div
                className={`${dashboardStyles.procress_bar}  ${barClass}`}>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 01</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT PLANS</h1>
                    </div>
                    <span className={dashboardStyles.active}></span>
                </div>

                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 02</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT AMOUNT</h1>
                    </div>
                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 03</span>
                        <h1 className={dashboardStyles.plans__title__text}>START PAYMENT</h1>
                    </div>

                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 04</span>
                        <h1 className={dashboardStyles.plans__title__text}>DONE</h1>
                    </div>
                    <span></span>
                </div>


            </div>

            {isLoad &&
                <section className={dashboardStyles.get__payment__section}><Spinner color="primary"/></section>}


            { // @ts-ignore
                transactionResult.success && !isLoad &&
                <section className={dashboardStyles.success__section}>
                    <div className={dashboardStyles.get__success}>
                        <h2 className={dashboardStyles.title}> The plan has been successfully activated for
                            you </h2>
                        <Spinner color="success"/>
                    </div>
                </section>
            }
            {
                // @ts-ignore
                transactionResult.error === 404 && !transactionResult.success && !isLoad &&
                <section className={dashboardStyles.not_found__section}>
                    <div className={dashboardStyles.get__not_found}>
                        <h2 className={dashboardStyles.title}>404</h2>
                        <p className={dashboardStyles.pragh}>NOT FOUND!</p>
                        <div className={dashboardStyles.btn__wrapper}>
                            <Button>Back</Button>
                            <Button>Support</Button>

                        </div>

                    </div>
                </section>

            }
            {
                // @ts-ignore
                transactionResult.error === 1 && !transactionResult.success && !isLoad &&
                <section className={dashboardStyles.refresh__section}>
                    <div className={dashboardStyles.get__refresh}>
                        <h2 className={dashboardStyles.title}>IP ERROR</h2>
                        <p className={dashboardStyles.pragh}>Change your IP and refresh the page</p>
                        <div className={dashboardStyles.btn__wrapper}>
                            <Button className={dashboardStyles.btn} onClick={() => router.reload()}>refresh</Button>


                        </div>

                    </div>
                </section>

            }


            {
                useTimer.use && <section className={dashboardStyles.waith__section}>
                    <div className={dashboardStyles.get__refresh}>

                        <p className={dashboardStyles.pragh}> The amount has been deposited from your account and we are
                            waiting to receive it. Please wait and keep this window open</p>
                        <div className={dashboardStyles.btn__wrapper}>


                            <Spinner color="primary"/>
                        </div>

                    </div>
                </section>


            }


        </div>
    </>
}