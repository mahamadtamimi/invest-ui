import dashboardStyles from "@/styles/dashboard.module.scss";
import Image from "next/image";
import testMony from "@/public/testMony.svg";
import {Button, Checkbox, Input, Link, Slider, Spinner} from "@nextui-org/react";

import styles from '@/styles/home.module.scss'
import React, {useEffect, useLayoutEffect, useState} from "react";

import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import {useRouter} from "next/router";
import {useSelector} from "react-redux";

export default function TankYouSuccess() {
    const router = useRouter();

    const [transactionResult, setTransactionResult] = useState({})
    const [isLoad, setIsLoad] = useState(true)

    const [barClass, setBarClass] = useState(dashboardStyles.green)
    // @ts-ignore
    const user = useSelector((state) => state.user);

    console.log(user)
    setTimeout(() => {
        router.push('/auth/dashboard');
    }, 1000);

    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>
        <div className={dashboardStyles.content__header}>
            <div className={dashboardStyles.content__header__title}>
                <span> WELCOME BACK, MARYAM JAFARPOUR </span>
                {/*<h1>PURCHASE PLAN</h1>*/}

            </div>

            <div className={dashboardStyles.content__header__icons}>
                <div className={dashboardStyles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={dashboardStyles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={dashboardStyles.icon}>

                </span>
                </div>
            </div>
        </div>
        <div className={styles.plans_sec}>
            <div
                className={`${dashboardStyles.procress_bar}  ${barClass}`}>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 01</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT PLANS</h1>
                    </div>
                    <span className={dashboardStyles.active}></span>
                </div>

                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 02</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT AMOUNT</h1>
                    </div>
                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 03</span>
                        <h1 className={dashboardStyles.plans__title__text}>START PAYMENT</h1>
                    </div>

                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 04</span>
                        <h1 className={dashboardStyles.plans__title__text}>DONE</h1>
                    </div>
                    <span></span>
                </div>


            </div>


            <section className={dashboardStyles.success__section}>
                <div className={dashboardStyles.get__success}>
                    <h2 className={dashboardStyles.title}> The plan has been successfully activated for
                        you </h2>
                    <Spinner color="success"/>
                </div>
            </section>


        </div>
    </>
}