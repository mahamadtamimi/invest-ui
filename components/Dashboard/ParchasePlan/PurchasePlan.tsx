import dashboardStyles from "@/styles/dashboard.module.scss";
import Image from "next/image";
import testMony from "@/public/testMony.svg";
import {Button, Checkbox, Input, Link, Slider, Spinner} from "@nextui-org/react";
import certificate from "@/public/plans-gerb.png";
import styles from '@/styles/home.module.scss'
import React, {useEffect, useState} from "react";
import {RadioGroup, Radio, cn} from "@nextui-org/react";
import {VisaIcon} from "@/components/Dashboard/ParchasePlan/VisaIcon";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {TikIcon} from "@/components/Dashboard/ParchasePlan/TikIcon";
import {redirect} from "next/navigation";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import nowPaymentGateWay from "@/public/nowPaymentGateway.png"
import walletIcon from '@/public/icons8-wallet-80.png'


export default function PurchasePlan() {
    const [data, setData] = useState(null)
    const [isLoading, setLoading] = useState(true)
    const [isSelected, setIsSelected] = React.useState(false);


    const [selected, setSelected] = React.useState("sandbox");

    const [amount, setAmount] = useState()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    const router = useRouter()
    const formInit = {
        tab: 1,
        plan: null,
        amount: amount
    }


    const [paymentValidator, setPaymentValidator] = useState({
            value: '',
            validate: false,
        }
    )

    const [formData, setFormData] = useState(formInit)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/plans/list`)
            .then((res) => res.json())
            .then((data) => {
                setData(data)
                setLoading(false)
            })
    }, [])


    if (isLoading) return <p>Loading...</p>
    if (!data) return <p>No profile data</p>

    // @ts-ignore
    function choosePlan(e) {
        const index = e.target.getAttribute('data-id')

        setFormData({
            ...formData,
            tab: 2,
            // @ts-ignore
            plan: data[index]
        })
    }

    // @ts-ignore
    const CustomRadio = (props) => {
        const {children, ...otherProps} = props;

        return (
            <Radio
                {...otherProps}

                classNames={{
                    base: ' bg-default-100 data-[hover=true]:bg-default-200 payment-radio-button-base',
                    wrapper: 'payment-radio-button-wrapper',
                    labelWrapper: 'payment-radio-button-labelWrapper',
                    label: 'payment-radio-button-label',
                    control: 'payment-radio-button-controller',
                    description: 'payment-radio-button-description',
                }}
            >
                {children}
            </Radio>
        );
    };


    const TermCheckBox = () => (
        <div className="flex flex-col gap-2">
            <Checkbox isSelected={isSelected}

                      classNames={{
                          label: !isSelected ? 'term-conditions-check-box-label' : 'term-conditions-check-box-label-selected'
                      }}
                      color="success"
                      onValueChange={setIsSelected}>
                I accept all the rules and regulations
            </Checkbox>

        </div>
    );


    function nextToGetPaymentLink() {
        if (!paymentValidator) {
            toast.error('Enter valid amount !')

        }


        setFormData({
            ...formData,
            tab: 3
        })




        const form = {
            // @ts-ignore
            'planId': formData.plan.id,
            'amount': formData.amount,
            'gateway': selected
        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/get-payments-link`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => res.json())
            .then((data) => {
                // console.log(data)
                if (data.success) {
                    router.replace(data.url)
                } else {
                    toast.error('The IP of your country is blocked')
                }

            })


    }

    // @ts-ignore
    function checkValidateAmount(e) {
        // @ts-ignore
        const minAmount = formData.plan.minimumDeposit
        // @ts-ignore


        const validate = (e.target.value >= minAmount)
        setPaymentValidator({
            ...paymentValidator,
            value: e.target.value,
            validate: validate
        })

        setFormData({
            ...formData,
            amount: e.target.value
        })

    }


    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>
        <DashboardHead title={'Purchase Plan'}/>
        <div className={styles.plans_sec}>
            <div className={dashboardStyles.procress_bar}>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 1 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 01</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT PLANS</h1>
                    </div>
                    <span className={dashboardStyles.active}></span>
                </div>

                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 2 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}>02</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT AMOUNT</h1>
                    </div>
                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 3 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 03</span>
                        <h1 className={dashboardStyles.plans__title__text}>START PAYMENT</h1>
                    </div>

                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 4 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 04</span>
                        <h1 className={dashboardStyles.plans__title__text}>DONE</h1>
                    </div>

                    <span></span>
                </div>


            </div>
            {formData.tab == 1 &&
                <section className={styles.plans__section}>
                    <div className={styles.plans__card__rigth}>


                        {
                            // @ts-ignore
                            data.map((item, index) => (
                                <div key={item.id} className={styles.plans__crads}>
                                    <div className={`${styles.plans__cards_top}`}>
                                        <div className={styles.plans__card}>
                                            <div className={`${styles.plans__card__title} ${dashboardStyles.bg_pink}`}>
                                                <h1 className={styles.plans__card__title__text}>{item.name}</h1>

                                                <p className={styles.plans__card_sub_title}>
                                                    For small teams that have less that 10 members.
                                                </p>

                                            </div>
                                            <div className={styles.plans__card__body}>
                                                <div className={styles.plans__card__persenting}>

                                                    <span className={styles.persenting}>+{item.interestRates}%</span>
                                                    <span className={styles.sub_persenting}>/ In contract period</span>
                                                </div>
                                                <div className={styles.plans__crad__body__top}>


                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                      minimum {item.minimumDeposit} $
                                                </span>


                                                    </p>

                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                       Profit withdrawal
                                                every {item.withdrawalPeriod} days

                                                </span>
                                                    </p>
                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                         The contract period
                                                is {item.dailyCredit} days
                                                </span>
                                                    </p>
                                                </div>

                                                <Button data-id={index} onClick={choosePlan}
                                                        className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink}`}>INVEST
                                                    NOW
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}


                    </div>

                </section>
            }
            {formData.tab == 2 &&
                <section className={dashboardStyles.amount__section}>

                    <div className={dashboardStyles.amount__section__body}>
                        <div className={dashboardStyles.amount__section__form}>
                            <div className={dashboardStyles.input_amount_section}>
                                <Input
                                    classNames={{
                                        label: 'radio-group-label'
                                    }}
                                    isRequired
                                    onChange={(e) => checkValidateAmount(e)}
                                    value={paymentValidator.value}
                                    type="number"
                                    variant="bordered"
                                    color={!paymentValidator.validate ? "danger" : "success"}
                                    isInvalid={!paymentValidator.validate}
                                    errorMessage={!paymentValidator.validate && `You must enter from ${new Intl.NumberFormat("en-IN", {maximumSignificantDigits: 3}).format(
                                        // @ts-ignore
                                        formData.plan.minimumDeposit,
                                    )} $ .`}
                                    label="Choose Invest Amount"
                                    labelPlacement={'outside'}
                                    placeholder={'2000'}

                                />


                            </div>


                            <div>

                                <div className={dashboardStyles.payment_method_choose}>

                                    <RadioGroup
                                        value={selected}
                                        onValueChange={setSelected}
                                        classNames={{
                                            'base': 'radio-group-base',
                                            'wrapper': 'radio-group-wrapper',
                                            'label': 'font-bold radio-group-label',

                                        }}

                                        label="Payment Method">

                                        <CustomRadio value="sandbox">

                                            <span className={'radio-custom-image'}>
                                                <Image src={nowPaymentGateWay} width={100} alt={''}/>
                                            </span>

                                            <span>
                                               NowPayment gateway


                                            </span>


                                        </CustomRadio>

                                        <CustomRadio value="wallet">

                                            <span className={'radio-custom-image'}>
                                                <Image src={walletIcon} width={100} alt={''}/>
                                            </span>

                                            <span>
                                             From Wallet

                                            </span>


                                        </CustomRadio>
                                    </RadioGroup>


                                </div>

                            </div>


                        </div>
                        <div className={dashboardStyles.term_and_conditions__section}>
                            <h3 className={dashboardStyles.term_and_conditions__header}>Terms & Conditions</h3>
                            <div className={dashboardStyles.term_and_conditions__body}>
                                Acceptable stablecoins on this site are:
                                USDT TRC20, USDT Bep20
                                Users must use secure and trusted wallets for storing and
                                transferring stablecoins.
                                When depositing, please double-check the provided wallet address
                                and amount. The site is not responsible for any loss of your capital.
                                Users must comply with the laws and regulations regarding the use of
                                digital currencies in their country of residence.Users should use the latest version of
                                their wallet and keep it updated
                                for better security and performance.
                                Users commit not to use digital currencies or any other methods on
                                this site for money laundering, terrorist financing, or any other illegal
                                activities.
                                The site reserves the right to temporarily or permanently block a
                                users account and report information to relevant legal authorities if
                                any suspicious or illegal activity is observed.
                                Upon request, users must provide necessary information and
                                documents to verify the legal source of their funds.
                                Failure to cooperate in providing requested information and
                                documents may result in account suspension and cancellation of all
                                executed transactions.


                            </div>
                            <div className={dashboardStyles.term_and_conditions__footer}>

                                <TermCheckBox/>

                            </div>
                        </div>


                    </div>


                    <div className={dashboardStyles.input_amount_btns}>
                        <Button
                            onClick={() => {
                                setFormData({...formData, tab: 1})
                            }}
                            className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink}`}>
                            back
                        </Button>
                        <Button
                            isDisabled={!(paymentValidator.validate && isSelected)}
                            onClick={nextToGetPaymentLink}
                            className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink} 
                            
                            ${!(paymentValidator.validate && isSelected) && 'disabled-btn'}
                            `}>
                            next
                        </Button>

                    </div>

                </section>
            }
            {formData.tab == 3 &&
                <section className={dashboardStyles.get__payment__section}>
                    <Spinner color="primary"/>
                </section>
            }

        </div>
    </>
}