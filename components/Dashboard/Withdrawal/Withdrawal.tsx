import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import dashboardStyles from "@/styles/dashboard.module.scss";


import React, {useEffect, useState} from "react";

import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import {Button, Input, Link, Snippet, useDisclosure} from "@nextui-org/react";
import {useSelector} from "react-redux";
import styles from "@/styles/dashboard.module.scss";

import {useRouter} from "next/router";
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader} from "@nextui-org/modal";

export default function Withdrawal() {

    const [formData, setFormData] = useState({
        amount: {
            value: '',
            validate: false,
        },
        googleCode: {
            value: '',
            validate: false
        }

    })

    const [inLoad, setInLoad] = useState(false)

    const [userWallet, setUserWallet] = useState()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    const [lock, setLock] = useState()

    const router = useRouter()

    function handelForm(e: any) {
        e.preventDefault();
        setInLoad(true)
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const form = {
            // @ts-ignore
            'amount': formData.amount.value,
            'googleCode': formData.googleCode.value

        }

        // console.log(form)


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/wallet/withdrawal`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => {

                    setInLoad(false)
                    if (res.status === 401) {
                        router.push('/auth/login')
                    }
                    if (res.status === 404 || res.status === 500) {
                        toast.dismiss('registerToastId')
                        toast.error('invalid request')
                        return null
                    }
                    if (res.status === 402) {
                        toast.dismiss('registerToastId')
                        toast.error('Invalid Auth Code !')
                        return null
                    }
                    if (res.status === 200) {
                        // router.push('/auth/dashboard')
                    }


                    return res.json()
                }
            ).then((data) => {
            console.log(data)
            toast.dismiss('registerToastId')
            toast.success('Your request has been registered with care')
            router.push('/auth/dashboard')
        })


    }



    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/wallet/me`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                console.log('===================')
                console.log(data)
                if (data.success) {
                    setUserWallet(data.wallet.credit)
                    setLock(data.lock)
                    // setLock(false)

                    const validate = data.credit > 0

                    setFormData({
                        ...formData,
                        amount: {
                            value: data.wallet.credit,
                            validate: validate
                        }

                    })
                    if (!data.lock){
                        setFormData({
                            ...formData,
                            googleCode: {
                                value: '',
                                validate: true
                            }

                        })
                    }
                } else {

                }

            })
    }, []);


    function updateFormData(e: any) {

        // @ts-ignore
        switch (e.target.getAttribute("data-role")) {
            case 'amount' :
                // @ts-ignore
                const validate = e.target.value > 0 && e.target.value <= userWallet

                setFormData({
                    ...formData,
                    amount: {
                        value: e.target.value,
                        validate: validate,
                    }

                })
                break;
            case 'code' :
                if (e.target.value.length > 6) return
                const codeValidate = e.target.value.length === 6

                setFormData({
                    ...formData,
                    googleCode: {
                        value: e.target.value,
                        validate: codeValidate,
                    }

                })
        }

    }




    const {isOpen, onOpen, onClose} = useDisclosure();


    // @ts-ignore
    return <>

        <DashboardHead title={'Withdrawal'}/>
        <div className={dashboardStyles.widrawal_main_sec}>


            <div className={dashboardStyles.widrawal_main_sec_grid}>
                <div className={dashboardStyles.widrawal_form}>
                    {!user.active ?

                        <div className={styles.content__header_alarm_box}>For withdrawal, please activate your account
                            ! <Link className={styles.text_decoration} color={'primary'}
                                    href={'/auth/dashboard/general-info'}>Generale
                                Info</Link></div>

                        :
                        <form action="" onSubmit={(e) => handelForm(e)}>
                        <span className={dashboardStyles.credit_box}>
                            <p>Credit :</p>
                            <Snippet>{userWallet}</Snippet>
                        </span>
                            <div className={'h-14'}>

                                <Input
                                    data-role={'amount'}
                                    classNames={{
                                        label: 'radio-group-label'
                                    }}
                                    variant="bordered"
                                    label="Price"
                                    labelPlacement={'outside'}
                                    placeholder={'200'}
                                    className={dashboardStyles.widrawal_input}
                                    type="number"
                                    color={formData.amount.validate ? 'success' : 'danger'}
                                    isInvalid={!formData.amount.validate}
                                    // @ts-ignore
                                    value={
                                        // @ts-ignore
                                        parseFloat(formData.amount.value)}
                                    errorMessage={!formData.amount.validate && 'Please enter a valid number'}
                                    onChange={(e) => updateFormData(e)}
                                />
                            </div>
                            {user.lock &&
                                <div className={'h-14'}>
                                    <Input type={'number'}
                                           classNames={{
                                               label: 'radio-group-label'
                                           }}

                                           data-role={'code'}
                                           variant="bordered"
                                           label="Google Authenticator Code"
                                           labelPlacement={'outside'}
                                           placeholder={'******'}
                                           className={dashboardStyles.widrawal_input}
                                           color={formData.googleCode.validate ? 'success' : 'danger'}
                                           isInvalid={!formData.googleCode.validate}
                                        // @ts-ignore
                                           value={formData.googleCode.value}
                                           errorMessage={!formData.googleCode.validate && 'Please enter a six digit code'}
                                           onChange={(e) => updateFormData(e)}/>

                                </div>

                            }
                            <div className={'mt-3'}>
                                <Button
                                    isLoading={inLoad}
                                    type={'submit'}
                                    isDisabled={!(formData.amount.validate &&
                                        formData.googleCode.validate)}

                                    className={`btn-one  flex
                                    ${!(formData.amount.validate && formData.googleCode.validate) ? 'disabled-btn' : ''}`}>
                                    Submit
                                </Button>
                            </div>


                        </form>

                    }


                </div>

                <div className={dashboardStyles.widrawal_description}>
                    <h2 className={dashboardStyles.widrawal_description_head}> Terms & Conditions </h2>
                    <div className={dashboardStyles.widrawal_description_text}>
                        Users can request withdrawal of their profits or capital after the
                        completion of the plan period or maturity date through the respective
                        section on the site.
                        Amounts will only be deposited to the wallet address registered by
                        the user on the site, and the wallet address cannot be changed.Therefore, users must be careful
                        when registering their wallet
                        address.
                        After submitting a withdrawal request, the requested amounts will be
                        deposited to the users wallet within 24 to 48 business hours.
                        Users must enter a valid and active digital wallet address to receive
                        withdrawn funds on the site.
                        The site is not responsible for any loss or theft of withdrawn funds
                        after being deposited into the users wallet address.
                        Users must use a secure and trusted wallet to receive withdrawn
                        funds.
                        The site reserves the right to cancel or postpone withdrawal requests
                        if any suspicious or illegal activity is observed.
                        By submitting a withdrawal request, the user agrees to comply with
                        all of the above terms and conditions.
                    </div>
                </div>

            </div>


        </div>


    </>


}
