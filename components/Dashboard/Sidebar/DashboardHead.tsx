import styles from '@/styles/dashboard.module.scss'
import {TetherIcon} from "@/components/Dashboard/Dashboard/TetherIcon";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";

import {Button, Link} from '@nextui-org/react';
import moment from "moment";
import {ClockIcon} from "@/components/Dashboard/Sidebar/ClockIcon";
import {useRouter} from "next/router";


export default function DashboardHead(props: any) {


    const router = useRouter();
    const user = useSelector((state:any) => state.user);

    if (!user) router.push("/auth/login");
    const [time, setTime] = useState(moment().utc().format('YYYY/MM/DD HH:mm:ss'))
    useEffect(() => {
        const interval = setInterval(() => {
            setTime(moment().utc().format('YYYY/MM/DD HH:mm:ss'))
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return <>
        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>{props.title}</h1>
                <span>Welcome <Link
                    href={'/auth/dashboard/general-info'}>{user.firstName} {user.lastName} </Link></span>
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>


                    <span>
                        <ClockIcon/>
                    </span>

                    <span>{time}  UTC</span>

                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>


        </div>
        {!user.active &&
            <div className={styles.content__header_alarm_box}>For activity, please activate your account ! <Link
                className={styles.text_decoration} color={'primary'} href={'/auth/dashboard/general-info'}>Generale
                Info</Link></div>
        }
    </>
}