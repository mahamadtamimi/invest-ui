export const TransactionIcon = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.75 13.974C11.75 13.974 16.25 13.974 19.9997 13.974C19.9997 16.738 13.9497 20 13.9497 20"
              stroke="#fff" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M20 13.974L5 13.974" stroke="#fff" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M5.00002 9.974L20 9.974" stroke="#fff" strokeWidth="1.5" strokeLinecap="round"
              strokeLinejoin="round"/>
        <path d="M13.25 9.97404C13.25 9.97404 8.75 9.97404 5.00029 9.97404C5.00029 7.21 11.0503 3.948 11.0503 3.948"
              stroke="#fff" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>


)