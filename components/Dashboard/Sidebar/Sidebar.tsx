import styles from '@/styles/dashboard.module.scss'
import logo from '@/public/log.svg'
import Image from "next/image";


import React, {useEffect, useState} from "react";
import {User, Link, Input} from "@nextui-org/react";
import {SearchIcon} from "@nextui-org/shared-icons";
import {HomeIcon} from "@/components/Dashboard/Sidebar/HomeIcon";
import {SignOutIcon} from "@/components/Dashboard/Sidebar/SignOutIcon";
import {SettingIcon} from "@/components/Dashboard/Sidebar/SettingIcon";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {PurchaseIcon} from "@/components/Dashboard/Sidebar/PurchaseIcon";
import {TransactionIcon} from "@/components/Dashboard/Sidebar/TransactionIcon";

import {SupportIcon} from "@/components/Dashboard/Sidebar/SupportIcon";
import style from "@/styles/chat.module.scss";
import {MassgeIcon} from "@/components/Dashboard/TicketList/MassgeIcon";
import {ChangePasswordIcon} from "@/components/Dashboard/Sidebar/ChangePasswordIcon";
import {TwoFactorIcon} from "@/components/Dashboard/Sidebar/TwoFactorIcon";
import {TwoFactorIconGreen} from "@/components/Dashboard/Sidebar/TwoFactorIconGreen";

function Avatar(props: any) {
    return (
        <User
            classNames={{
                name: 'avatar-name'
            }}
            name={props.name}

        />
    );
}


export default function Sidebar() {
    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    const router = useRouter();

    const [ticket, setTicket] = useState()
    const [pending, setPending] = useState(true)

    const [newMassage, setNewMassage] = useState(false)


    function signUpHandle() {

        dispatch({type: 'LOGIN', payload: {user: null}});
        router.push('/auth/login')
        // return null

    }

    useEffect(() => {

        fetch(`${process.env.API_PATH}/api/v1/user/tickets`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setTicket(data)
                setPending(false)
            })

    }, [])


    useEffect(() => {
        if (!pending) {
            // @ts-ignore
            if (ticket.length > 0) {
                {
                    // @ts-ignore
                    ticket.map((item, index) => {

                        const newMassage = item.TicketBodies.filter((item: any) => {
                            return (item.side == 1 && item.seen == false)
                        }).length


                        if (newMassage) setNewMassage(true)
                    })
                }


            }
        }

    }, [ticket, pending]);


    return <section className={styles.sidebar}>
        <span className={styles.sidebar_bg}></span>
        <div className={styles.sidebar__title}>
            <Image src={logo} alt=''/>


        </div>
        <div className={styles.avatar_sec}>
            <Avatar name={`${user.firstName} ${user.lastName}`}/>
        </div>
        <div>
            <Input
                classNames={{
                    inputWrapper: 'sidebar-search-input-wrapper',
                    mainWrapper: 'sidebar-search-input-main-wrapper'
                }}


                placeholder={'search'}
                startContent={
                    <SearchIcon
                        className="text-black/50 mb-0.5 dark:text-white/90 text-slate-400 pointer-events-none flex-shrink-0"/>
                }
            />
        </div>
        <div className={styles.sidebar__content}>
            <ul className={styles.sidebar__content__links}>
                <li className={styles.links__item}>
                    <Link href="/auth/dashboard" className={`${styles.link}`}>

                        <HomeIcon/>

                        Dashboard
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/auth/dashboard/purchase-plans" className={styles.link}>

                        <PurchaseIcon/>

                        Purchase Plan
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/auth/dashboard/withdrawal" className={styles.link}>

                        <TransactionIcon/>

                        Withdrawal
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/auth/dashboard/general-info" className={styles.link}>

                        <SettingIcon/>

                        General Info
                    </Link>
                </li>
                <li className={styles.links__item}>
                    <Link href="/auth/dashboard/change-password" className={styles.link}>

                        <ChangePasswordIcon/>

                        Change Password
                    </Link>
                </li>

                <li className={styles.links__item}>
                    <Link href="/auth/dashboard/ticket" className={styles.link}>

                        <SupportIcon/>

                        Tickets
                        {newMassage ? <span className={style.have_new_massage__menu}>{newMassage}</span> : ''}
                    </Link>
                </li>
                {
                    user.lock ?
                        <li className={styles.links__item}>
                            <Link href="/auth/dashboard/disble-two-verification"
                                  className={`${styles.link}  ${styles.links__item_text_red}`}>


                                <TwoFactorIcon/>

                                Disable 2FA
                            </Link>
                        </li>
                        :
                        <li className={`${styles.links__item}`}>
                            <Link href="/auth/dashboard/two-verification"
                                  className={`${styles.link}  ${styles.links__item_text_danger}`}>

                                <TwoFactorIconGreen/>

                                Enable 2FA
                            </Link>
                        </li>
                }


            </ul>
        </div>

        <div className={styles.sidebar__footer}>
            <ul className={styles.sidebar__content__links}>


                <li className={styles.links__item}>

                    <Link
                        href={'/auth/dashboard/sign-out'}
                        // onClick={signUpHandle}
                        className={styles.link}>
                        <SignOutIcon/>
                        Sign Out
                    </Link>

                </li>
            </ul>


        </div>
    </section>
}