// import './style.scss'
export default function Setting() {
    return <section className="content">
        <div className="container">

            <div className="row">
                <div className="col-12">
                    <div className="content__header">
                        <div className="content__header__title">
                            <h1>GENERAI DETAILS</h1>
                            <span> UPDATE YOUR PHOTO AND PERSONAL DETAILS HERE </span>
                        </div>
                        <div className="content__header__icons">
                            <div className="content__header__icons__date">
                                <span> SUNDAY, NOVEMBER 26 </span>
                            </div>
                            <div className="content__header__icon">
                    <span className="icon">

                    </span>

                                <span className="icon">

                    </span>

                                <span className="icon">

                    </span>
                            </div>
                        </div>
                    </div>
                    <div className="row align-center">
                        <div className="col-6">
                            <div className="form_settings">
                                <div className="form_setting_header">
                                    <h1>PERSONAL INFORMATION</h1>
                                    <div className="cc">

                                    </div>
                                </div>
                                <form action="#" className="forms">
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input
                                            type="text"
                                            className="input-group-form"
                                            placeholder="ENTER YOUR USERNAME"
                                        />
                                        <span>

                        </span>
                                    </div>
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input
                                            type="text"
                                            className="input-group-form"
                                            placeholder="ENTER YOUR USERNAME"
                                        />
                                        <span>

                                            </span>
                                    </div>
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input
                                            type="text"
                                            className="input-group-form"
                                            placeholder="ENTER YOUR USERNAME"
                                        />
                                        <span>
                                            </span>
                                    </div>
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input type="text"
                                               className="input-group-form"
                                               placeholder="ENTER YOUR USERNAME"/>
                                        <span>
                                            </span>
                                    </div>
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input
                                            type="text"
                                            className="input-group-form"
                                            placeholder="ENTER YOUR USERNAME"
                                        />
                                        <span> </span>
                                    </div>
                                    <div className="input-group">
                                        <label htmlFor="" className="label-group"> USERNAME </label>
                                        <input
                                            type="text"
                                            className="input-group-form"
                                            placeholder="ENTER YOUR USERNAME"/>
                                        <span></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="btns__submit">
                                <button className="btn btn__submit ">CANCEL</button>
                                <button className="btn btn__submit bg-red tx-bg-white border-none">SAVE</button>
                            </div>
                            <div className="setting__profile__img">
                                <div className="setting__profile__img__title">
                                    <h1>YOUR PHOTO</h1>
                                </div>
                                <div className="setting__profile__user__crad">
                                    <img
                                        src="../assets/images/Photo1.png"
                                        alt="imga-profile"
                                    />
                                    <div className="card__user__body">
                                        <h1>EDIT YOUR PHOTO</h1>
                                        <div className="btns__card">
                                            <button className="btn card__user__btn__del">
                                                UPDATE
                                            </button>
                                            <button className="btn card__user__btn__upd">
                                                DELETE
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div className="upload__box">
                                          <span>

                                          </span>
                                    <h1 className="tx-gray">
                                         <span className="tx-red">
                                          CLICK TO UPLOAD
                                         </span>
                                        OR DRAG AND DROP
                                    </h1>
                                </div>
                            </div>
                            <div className="setting__profile__google">
                                <div className="setting__profile__google__top">
                                    <img src="../assets/images/Google_2015_logo 1.png" alt=""/>
                                    <button className=" btn  setting__profile__google__top__btn">CONNECTED</button>
                                </div>
                                <div className="setting__profile__google__bottom">
                                    <h1>GOOGLE</h1>
                                    <span>USE GOOGLE TO SIGN IN TO YOUR ACCOUNT</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
}