import {Link} from "@nextui-org/link";
import Image from 'next/image'
import logo from '@/public/log.svg'


import {
    Navbar,
    NavbarBrand,
    NavbarContent,
    NavbarItem,
    NavbarMenuToggle,
    NavbarMenu,
    NavbarMenuItem, Button, Badge, User
} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";
import {NotificationIcon} from "@/components/MainMenu/NotificationIcon";
import React, {useEffect, useState} from "react";
import styles from "@/styles/MainMenu.module.scss";
import {TetherIcon} from "@/components/Dashboard/Dashboard/TetherIcon";
import {toast} from "react-toastify";
import {router} from "next/client";
import {useRouter} from "next/router";

export default function MainMenu() {

    const menuItems = [
        "Profile",
        "Dashboard",
        "Activity",
        "Analytics",
        "System",
        "Deployments",
        "My Settings",
        "Team Settings",
        "Help & Feedback",
        "Log Out",
    ];


    // @ts-ignore
    const user = useSelector((state) => state.user);
    const [userWallet, setUserWallet] = useState()

    const router = useRouter();


    const dispatch = useDispatch()

    useEffect(() => {
        {
            user &&
            fetch(`${process.env.API_PATH}/api/v1/auth/me`, {
                headers: {
                    Authorization: `bearer ${user.token}`
                }
            })
                .then((res) => {
                    if (res.status === 401 || res.status === 404) {

                        dispatch({type: 'LOGOUT'});
                        return
                    }
                    if (res.status == 500) {

                        toast.error('Server Error !')
                        return
                    }


                    return res.json()
                })
                .then((data) => {
                    console.log(data)
                    dispatch({type: 'LOGIN', payload: {user: data}});
                })
        }

    }, [])

    return <>
        <Navbar maxWidth={'full'}
                height={'4rem'}
                isBordered={true}
                classNames={{
                    menu: 'test-menu'
                }}
        >
            <NavbarContent className="sm:hidden" justify="start">
                <NavbarMenuToggle/>
            </NavbarContent>


            <NavbarBrand className={'text__class'}>
                <Image className={styles.menu_logo} src={logo} alt=''/>

            </NavbarBrand>
            <NavbarContent className="hidden sm:flex gap-4" justify="start">
                <NavbarItem isActive={router.route === '/'}>
                    <Link color={router.route === '/' ? 'primary' : 'foreground'} href="/">
                        Home
                    </Link>
                </NavbarItem>
                <NavbarItem isActive={router.route === '/about-us'}>
                    <Link color={router.route === '/about-us' ? 'primary' : 'foreground'} href="/about-us">
                        About Us
                    </Link>
                </NavbarItem>
                <NavbarItem isActive={router.route === '/faq'}>
                    <Link color={router.route === '/faq' ? 'primary' : 'foreground'} href="/faq">
                        Faq
                    </Link>
                </NavbarItem>
                <NavbarItem isActive={router.route === '/plans'}>
                    <Link color={router.route === '/plans' ? 'primary' : 'foreground'} href="/plans">
                        Plans
                    </Link>
                </NavbarItem>
            </NavbarContent>
            <NavbarContent justify="end">
                {user ?
                    <>

                        <NavbarItem className="hidden lg:flex">
                            <Link href="/auth/dashboard">

                                <User
                                    name={`${user.firstName} ${user.lastName}`}
                                    description={(
                                        <Link href="/auth/dashboard" size="sm" isExternal>
                                            @ Go Dashboard
                                        </Link>
                                    )}
                                    avatarProps={{
                                        // src: "https://avatars.githubusercontent.com/u/30373425?v=4"
                                    }}
                                />

                            </Link>

                        </NavbarItem>
                        <NavbarItem className="">

                            <Link href={'/auth/dashboard/withdrawal'} className={styles.main_menu_wallet}>

                                <TetherIcon/>
                                <span className={styles.wallet__in__price}>
                                            {
                                                user.wallet}



                                </span>


                            </Link>

                        </NavbarItem>
                        <NavbarItem className="hidden lg:flex">
                            <Button color={'primary'} as={Link} className={'text-white'}
                                    href="/auth/dashboard/purchase-plans">Purchase Plan</Button>
                        </NavbarItem>


                    </>

                    :
                    <>
                        <NavbarItem className="">
                            <Link className={'login-link-head'} href="/auth/login">Login</Link>
                        </NavbarItem>
                        <NavbarItem className={'hidden lg:flex'}>
                            <Button as={Link} className={'text-white'} color={'primary'} href={'/auth/register'}>
                                Sign Up
                            </Button>
                        </NavbarItem>
                    </>

                }


            </NavbarContent>


            <NavbarMenu>
                <div className={styles.mobile_user_item}>
                    {user ?
                        <>

                            <div className={styles.mobile_user_cart}>
                                <Link href="/auth/dashboard">
                                    <User
                                        name={`${user.firstName} ${user.lastName}`}
                                        description={(
                                            <Link href="/auth/dashboard" size="sm" isExternal>
                                                @ Go Dashboard
                                            </Link>
                                        )}
                                        avatarProps={{
                                            // src: "https://avatars.githubusercontent.com/u/30373425?v=4"
                                        }}
                                    />
                                </Link>

                            </div>
                            {/*<div className={styles.wallet_cart}>*/}

                            {/*    <Link href={'/auth/dashboard/withdrawal'}*/}
                            {/*          className={styles.main_menu_wallet}>*/}

                            {/*        <TetherIcon/>*/}
                            {/*        <span className={styles.wallet__in__price}>*/}
                            {/*                {*/}

                            {/*                    new Intl.NumberFormat('en-IN', {maximumSignificantDigits: 3}).format(*/}
                            {/*                        // @ts-ignore*/}
                            {/*                        user.wallet,*/}
                            {/*                    )}$*/}


                            {/*                    </span>*/}


                            {/*    </Link>*/}

                            {/*</div>*/}
                            <div className={styles.mob_btn}>
                                <Button color={'primary'} as={Link} className={'text-white w-full'}
                                        href="/auth/dashboard/purchase-plans">Purchase Plan</Button>
                            </div>


                        </>

                        :
                        <>
                            <NavbarItem className="hidden lg:flex">
                                <Link className={'login-link-head'} href="/auth/login">Login</Link>
                            </NavbarItem>
                            <NavbarItem className={''}>
                                <Button as={Link} className={'btn btn-dark'} href={'/auth/register'}
                                        variant="flat">
                                    Sign Up
                                </Button>
                            </NavbarItem>

                        </>

                    }


                </div>
                <NavbarMenuItem>
                    <Link
                        className="w-full mb-2"

                        href="/"
                        size="lg"
                    >
                        Home
                    </Link>
                    <Link
                        className="w-full mb-2"

                        href="/about-us"
                        size="lg"
                    >
                        About Us
                    </Link>
                    <Link
                        className="w-full mb-2"

                        href="/faq"
                        size="lg"
                    >
                        FAQ
                    </Link>
                    <Link
                        className="w-full mb-2"

                        href="/plans"
                        size="lg"
                    >
                        Plans
                    </Link>
                </NavbarMenuItem>

            </NavbarMenu>
        </Navbar>

    </>

        ;


}