import PurchasePlan from "@/components/Dashboard/ParchasePlan/PurchasePlan";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {Button, Checkbox, Input, Radio, RadioGroup, Spinner} from "@nextui-org/react";
import {toast, ToastContainer} from "react-toastify";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import styles from "@/styles/home.module.scss";
import dashboardStyles from "@/styles/dashboard.module.scss";
import {TikIcon} from "@/components/Dashboard/ParchasePlan/TikIcon";
import {VisaIcon} from "@/components/Dashboard/ParchasePlan/VisaIcon";
import Image from "next/image";
import certificate from "@/public/plans-gerb.png";
import Link from "next/link";

export default function HomeParchasePlan() {
    const [data, setData] = useState(null)
    const [isLoading, setLoading] = useState(true)
    const [isSelected, setIsSelected] = React.useState(false);


    const [selected, setSelected] = React.useState("sandbox");

    const [amount, setAmount] = useState()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    const router = useRouter()
    const formInit = {
        tab: 1,
        plan: null,
        amount: amount
    }


    const [paymentValidator, setPaymentValidator] = useState({
            value: '',
            validate: false,
        }
    )

    const [formData, setFormData] = useState(formInit)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/plans/list`)
            .then((res) => res.json())
            .then((data) => {
                setData(data)
                setLoading(false)
            })
    }, [])


    const color = [
        'green',
        'yellow',
        'blue',
        'red',
    ]

    if (isLoading) return <p>Loading...</p>
    if (!data) return <p>No profile data</p>





    // @ts-ignore
    function choosePlan(e) {
        if (user !== null){
            router.push('/auth/dashboard/purchase-plans')

        }else {
            router.push('/auth/login')
        }

        // {! user && router.push('/auth/login')}


        // const index = e.target.getAttribute('data-id')
        //
        // setFormData({
        //     ...formData,
        //     tab: 2,
        //     @ts-ignore
            // plan: data[index]
        // })
    }

    // @ts-ignore
    const CustomRadio = (props) => {
        const {children, ...otherProps} = props;

        return (
            <Radio
                {...otherProps}

                classNames={{
                    base: ' bg-default-100 data-[hover=true]:bg-default-200 payment-radio-button-base',
                    wrapper: 'payment-radio-button-wrapper',
                    labelWrapper: 'payment-radio-button-labelWrapper',
                    label: 'payment-radio-button-label',
                    control: 'payment-radio-button-controller',
                    description: 'payment-radio-button-description',
                }}
            >
                {children}
            </Radio>
        );
    };


    const TermCheckBox = () => (
        <div className="flex flex-col gap-2">
            <Checkbox isSelected={isSelected}

                      classNames={{
                          label: !isSelected ? 'term-conditions-check-box-label' : 'term-conditions-check-box-label-selected'
                      }}
                      color="success"
                      onValueChange={setIsSelected}>
                I accept all the rules and regulations
            </Checkbox>

        </div>
    );


    function nextToGetPaymentLink() {
        if (!paymentValidator) {
            toast.error('Enter valid amount !')

        }


        setFormData({
            ...formData,
            tab: 3
        })


        const form = {
            // @ts-ignore
            'planId': formData.plan.id,
            'amount': formData.amount,
            'gateway': selected
        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/get-payments-link`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => res.json())
            .then((data) => {

                router.replace(`https://sandbox.zarinpal.com/pg/StartPay/${data.Authority}`)
            })


    }

    // @ts-ignore
    function checkValidateAmount(e) {
        // @ts-ignore
        const minAmount = formData.plan.minimumDeposit
        // @ts-ignore


        const validate = (e.target.value >= minAmount)
        setPaymentValidator({
            ...paymentValidator,
            value: e.target.value,
            validate: validate
        })

        setFormData({
            ...formData,
            amount: e.target.value
        })

    }


    return <div className={styles.plan_main_sec}>

        <div className={styles.plans_sec}>
            <div className={dashboardStyles.procress_bar}>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 1 && dashboardStyles.procress_bar_part__active}  ${formData.tab == 1 && dashboardStyles.mobile_show}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 01</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT PLANS</h1>
                    </div>
                    <span className={dashboardStyles.active}></span>
                </div>

                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 2 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 02</span>
                        <h1 className={dashboardStyles.plans__title__text}>INVESTMENT AMOUNT</h1>
                    </div>
                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 3 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 03</span>
                        <h1 className={dashboardStyles.plans__title__text}>START PAYMENT</h1>
                    </div>

                    <span></span>
                </div>
                <div
                    className={`${dashboardStyles.procress_bar_part}  ${formData.tab >= 4 && dashboardStyles.procress_bar_part__active}`}>
                    <div className={dashboardStyles.plans__title}>
                        <span className={dashboardStyles.plans__title_sub}> 04</span>
                        <h1 className={dashboardStyles.plans__title__text}>DONE</h1>
                    </div>

                    <span></span>
                </div>


            </div>
            {formData.tab == 1 &&
                <section className={styles.plans__section}>
                    <div className={styles.plans__card__rigth}>


                        {
                            // @ts-ignore
                            data.map((item, index) => (
                                <div key={item.id} className={styles.plans__crads}>
                                    <div className={`${styles.plans__cards_top}`}>
                                        <div className={styles.plans__card}>
                                            <div className={`${styles.plans__card__title} ${dashboardStyles.bg_pink}`}>
                                                <h1 className={styles.plans__card__title__text}>{item.name}</h1>

                                                {/*<p className={styles.plans__card_sub_title}>*/}
                                                {/*    {item.description}*/}
                                                {/*</p>*/}

                                            </div>
                                            <div className={styles.plans__card__body}>
                                                <div className={styles.plans__card__persenting}>

                                                    <span className={styles.persenting}>+{item.interestRates}%</span>
                                                    <span className={styles.sub_persenting}>/ In contract period</span>
                                                </div>
                                                <div className={styles.plans__crad__body__top}>


                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                      minimum {item.minimumDeposit} $
                                                </span>


                                                    </p>

                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                       Profit withdrawal
                                                every {item.withdrawalPeriod} days

                                                </span>
                                                    </p>
                                                    <p className={styles.plans__crad__body__item}>
                                                        <TikIcon/>
                                                        <span className={styles.plans__crad__body__item__text}>
                                                         The contract period
                                                is {item.dailyCredit} days
                                                </span>
                                                    </p>
                                                </div>

                                                <Button data-id={index} onClick={choosePlan}
                                                        className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink}`}>INVEST
                                                    NOW
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}


                    </div>

                </section>
            }
            {formData.tab == 2 &&
                <section className={dashboardStyles.amount__section}>

                    <div className={dashboardStyles.amount__section__body}>
                        <div className={dashboardStyles.amount__section__form}>
                            <div className={dashboardStyles.input_amount_section}>
                                <Input
                                    classNames={{
                                        label: 'radio-group-label'
                                    }}
                                    isRequired
                                    onChange={(e) => checkValidateAmount(e)}
                                    value={paymentValidator.value}
                                    type="number"
                                    variant="bordered"
                                    color={!paymentValidator.validate ? "danger" : "success"}
                                    isInvalid={!paymentValidator.validate}
                                    errorMessage={!paymentValidator.validate && `You must enter from ${new Intl.NumberFormat("en-IN", {maximumSignificantDigits: 3}).format(
                                        // @ts-ignore
                                        formData.plan.minimumDeposit,
                                    )} $ .`}
                                    label="Choose Invest Amount"
                                    labelPlacement={'outside'}
                                    placeholder={'2000'}

                                />


                            </div>


                            <div>

                                <div className={dashboardStyles.payment_method_choose}>

                                    <RadioGroup
                                        value={selected}
                                        onValueChange={setSelected}
                                        classNames={{
                                            'base': 'radio-group-base',
                                            'wrapper': 'radio-group-wrapper',
                                            'label': 'font-bold radio-group-label',

                                        }}
                                        orientation="horizontal"
                                        label="Payment Method">

                                        <CustomRadio value="sandbox">

                                            <span className={'radio-custom-image'}>
                                                <VisaIcon/>
                                            </span>

                                            <span>
                                               Sand Box

                                                <span className={'radio-custom-descrption'}>description</span>
                                            </span>


                                        </CustomRadio>

                                        <CustomRadio description="Up to 20 items" value="free">
                                            Free
                                        </CustomRadio>
                                        <CustomRadio description="Unlimited items. $10 per month." value="pro">
                                            Pro
                                        </CustomRadio>

                                    </RadioGroup>


                                </div>

                            </div>


                        </div>
                        <div className={dashboardStyles.term_and_conditions__section}>
                            <h3 className={dashboardStyles.term_and_conditions__header}>Terms & Conditions</h3>
                            <div className={dashboardStyles.term_and_conditions__body}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut
                                labore et dolore magna aliqua. Nulla facilisi morbi tempus iaculis urna id volutpat
                                lacus.
                                Integer malesuada nunc vel risus commodo. A cras semper auctor neque vitae tempus. Mi
                                ipsum
                                faucibus vitae aliquet nec. Arcu cursus euismod quis viverra nibh cras. Penatibus et
                                magnis
                                dis parturient montes nascetur ridiculus mus mauris. Quisque non tellus orci ac.
                                Maecenas
                                accumsan lacus vel facilisis volutpat. Sollicitudin aliquam ultrices sagittis orci.
                                Morbi
                                tristique senectus et netus et malesuada fames ac. Elementum sagittis vitae et leo duis
                                ut.
                                Sed adipiscing diam donec adipiscing tristique risus nec.

                                A pellentesque sit amet porttitor eget dolor. Eget nulla facilisi etiam dignissim diam
                                quis
                                enim lobortis. Quis blandit turpis cursus in hac habitasse platea dictumst. Vulputate
                                dignissim suspendisse in est ante in nibh mauris cursus. Sit amet massa vitae tortor
                                condimentum lacinia quis. Ullamcorper malesuada proin libero nunc. Orci phasellus
                                egestas
                                tellus rutrum. Augue mauris augue neque gravida in fermentum. Sed vulputate odio ut enim
                                blandit volutpat maecenas. Turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet.
                                Habitant morbi tristique senectus et netus et malesuada fames. Quis vel eros donec ac
                                odio
                                tempor orci dapibus. Faucibus in ornare quam viverra orci sagittis eu. Felis imperdiet
                                proin
                                fermentum leo vel orci porta non pulvinar. Lacus laoreet non curabitur gravida arcu ac.
                                Cursus turpis massa tincidunt dui ut. Sed nisi lacus sed viverra tellus in hac
                                habitasse.
                                Sollicitudin aliquam ultrices sagittis orci. Ligula ullamcorper malesuada proin libero
                                nunc
                                consequat interdum varius sit. Netus et malesuada fames ac turpis.


                            </div>
                            <div className={dashboardStyles.term_and_conditions__footer}>

                                <TermCheckBox/>

                            </div>
                        </div>


                    </div>


                    <div className={dashboardStyles.input_amount_btns}>
                        <Button
                            onClick={() => {
                                setFormData({...formData, tab: 1})
                            }}
                            className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink}`}>
                            back
                        </Button>
                        <Button
                            isDisabled={!(paymentValidator.validate && isSelected)}
                            onClick={nextToGetPaymentLink}
                            className={`${styles.btn}  ${styles.plans__card__btn} ${dashboardStyles.bg_pink} 
                            
                            ${!(paymentValidator.validate && isSelected) && 'disabled-btn'}
                            `}>
                            next
                        </Button>

                    </div>

                </section>
            }
            {formData.tab == 3 &&
                <section className={dashboardStyles.get__payment__section}>
                    <Spinner color="primary"/>
                </section>
            }

        </div>
    </div>
}