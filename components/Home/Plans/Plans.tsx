import styles from '@/styles/home.module.scss'
import Image from "next/image";
import certificate from '@/public/plans-gerb.png'
import Link from "next/link";
import PlanArchive from "@/components/admin/PlansArchive/PlanArchive";
export default function Plans() {
    return <div className={styles.plans_sec}>
        <div className={styles.plans__title}>
            <span className={styles.plans__title_sub}> 01</span>
            <h1 className={styles.plans__title__text }>INVESTMENT PLANS</h1>
        </div>
        <section className={styles.plans__section}>
            <div className={styles.plans__card__rigth}>
                <div className={styles.certificate}>
                    <div>
                        <Image src={certificate} alt='' className={styles.certificate_img}/>


                        <div>
                            <p className={styles.certifacate_title}>
                                COMPANY NUMBER:
                            </p>
                            <p className={styles.certifacate_prag}>
                                15091482
                            </p>
                            <p className={styles.certifacate_title}>
                                REGISTERED OFFICE ADDRESS:
                            </p>
                            <p className={styles.certifacate_prag}>
                                16a Curzon Street, London, England, W1J 5HP
                            </p>

                        </div>
                        <Link href='' className={styles.ceck_certi}>CHECK CERTIFICATE</Link>
                    </div>

                </div>
                <div className={styles.plans__crads}>
                    <div className={`${styles.plans__cards_top} ${styles.green}`}>
                        <div className={styles.plans__card}>
                            <div className={styles.plans__card__title}>
                                <h1 className={styles.plans__card__title__text}>BASIC PLAN</h1>
                                <span className={styles.persenting}>+0.1%</span>
                            </div>
                            <div className={styles.plans__card__body}>
                                <div className={styles.plans__crad__body__top}>
                                    <p className={styles.plans__crad__body__top_one}>MIN$1500.00</p>
                                    <p className={styles.plans__crad__body__top_two}>MAX$3000.00</p>
                                    <p className={styles.plans__crad__body__top_tree}>DAILY FOR LIFE (working days only)</p>
                                </div>
                                <button className={`${styles.btn} ${styles.btn_green} ${styles.plans__card__btn}`}>INVEST NOW</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.plans__crads}>
                    <div className={`${styles.plans__cards_top} ${styles.yellow}`}>
                        <div className={styles.plans__card}>
                            <div className={styles.plans__card__title}>
                                <h1 className={styles.plans__card__title__text}>STANDARD PLAN</h1>
                                <span className={styles.persenting}>+0.1%</span>
                            </div>
                            <div className={styles.plans__card__body}>
                                <div className={styles.plans__crad__body__top}>
                                    <p className={styles.plans__crad__body__top_one}>MIN$1500.00</p>
                                    <p className={styles.plans__crad__body__top_two}>MAX$3000.00</p>
                                    <p className={styles.plans__crad__body__top_tree}>PER MONTH FOR LIFE</p>
                                </div>
                                <button className={`${styles.btn} ${styles.btn_green} ${styles.plans__card__btn}`} >INVEST NOW</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.plans__crads}>
                    <div className={`${styles.plans__cards_top} ${styles.blue}`}>
                        <div className={styles.plans__card}>
                            <div className={styles.plans__card__title}>
                                <h1 className={styles.plans__card__title__text}>STANDARD PLAN</h1>
                                <span className={styles.persenting}>+0.1%</span>
                            </div>
                            <div className={styles.plans__card__body}>
                                <div className={styles.plans__crad__body__top}>
                                    <p className={styles.plans__crad__body__top_one}>MIN$1500.00</p>
                                    <p className={styles.plans__crad__body__top_two}>MAX$3000.00</p>
                                    <p className={styles.plans__crad__body__top_tree}>PER MONTH FOR LIFE</p>
                                </div>
                                <button
                                    className={`${styles.btn} ${styles.btn_green} ${styles.plans__card__btn}`}>INVEST
                                    NOW
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.plans__crads}>
                    <div className={`${styles.plans__cards_top} ${styles.red}`}>
                        <div className={styles.plans__card}>
                            <div className={styles.plans__card__title}>
                                <h1 className={styles.plans__card__title__text}>STANDARD PLAN</h1>
                                <span className={styles.persenting}>+0.1%</span>
                            </div>
                            <div className={styles.plans__card__body}>
                                <div className={styles.plans__crad__body__top}>
                                    <p className={styles.plans__crad__body__top_one}>MIN$1500.00</p>
                                    <p className={styles.plans__crad__body__top_two}>MAX$3000.00</p>
                                    <p className={styles.plans__crad__body__top_tree}>PER MONTH FOR LIFE</p>
                                </div>
                                <button
                                    className={`${styles.btn} ${styles.btn_green} ${styles.plans__card__btn}`}>INVEST
                                    NOW
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>


}