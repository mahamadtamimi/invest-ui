import styles from '@/styles/home.module.scss'
import Image from "next/image";
import background from "@/public/[fpdl 5.png";
import {Button, Link} from '@nextui-org/react';
import aboutUseBack from '@/public/home-section-wrong-choice.png'
export default function AboutUs() {
    return <section className={`${styles.about} ${styles.p_num} mt-10`}>

        <div className={styles.plans__title}>
            <span className={styles.plans__title_sub}> 03</span>
            <h1 className={styles.plans__title__text}>About Us</h1>
        </div>
        <div className={styles.about__body__home}>
            <div className={styles.about__body__home__text}>

                <p className={styles.about__body__home__title_description}>
                    Our team consists of experienced professionals in the construction
                    industry who have been operating professionally in this field since
                    2017. After 7 successful years, we have now decided to expand the
                    scope of our activities by launching an innovative online platform.
                    Our goal is to create a secure and transparent platform to attract
                    investors and allow them to share in the profits from residential and
                    commercial building construction and renovation projects. Through
                    this platform, users can confidently invest their capital in reliable and
                    profitable projects, relying on our extensive experience and expertise.
                    Leveraging our specialized knowledge and skills in construction, we
                    introduce and execute diverse projects in the residential and
                    commercial sectors. Investors can participate in these projects
                    through our online platform and fairly benefit from the generated
                    profits.
                    Our professional and experienced team, focusing on transparency,
                    security, and high efficiency, is ready to provide the best services and
                    support to investors. We take pride in offering new and profitable
                    opportunities to the public through this innovative platform.

                </p>

            </div>
            <div className={styles.about__body__home__btn}>
               <Image src={aboutUseBack} alt={''} className={styles.about__body__home__image} width={300} height={400} />

                <Button as={Link} href={'/about-us'} color={'primary'} radius={'none'} className={`${styles.btn} ${styles.btn_red} ${styles.tx_white} mt-10 w-full text-white`}>

                    MORE ABOUT US
                </Button>
            </div>
        </div>

    </section>
}