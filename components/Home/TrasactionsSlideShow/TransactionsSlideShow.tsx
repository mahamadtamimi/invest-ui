import styles from "@/styles/home.module.scss";
import {TetherIcon} from "@/components/Dashboard/Dashboard/TetherIcon";
import { faker } from '@faker-js/faker';
import React, {useEffect, useState} from "react";
import {Swiper, SwiperSlide} from 'swiper/react';
import {Autoplay, Mousewheel, Navigation, Pagination} from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';

export default function TransactionsSlideShow() {

    const [transaction, setTransaction] = useState({})
    const [pending, setPending] = useState(true)
    useEffect(() => {

        fetch(`${process.env.API_PATH}/api/v1/all-transaction`).then(res => res.json()).then(data => {
            console.log(data)
            setTransaction(data)
            setPending(false)
        })
    }, []);


    // @ts-ignore
    return <div className={styles.banner__content__rartines}>
        <div className={styles.banner__content__rartines_trasaction_main_sec}>

            <h1 className={styles.banner__content__rartines__text}>Transaction Summery:</h1>

            <div className={styles.withdrawal_summery_main_sec}>
                <>
                    <Swiper
                        slidesPerView={5}
                        spaceBetween={8}
                        pagination={{
                            type: 'progressbar',
                        }}
                        autoplay={{
                            delay: 5000,
                            disableOnInteraction: false,
                        }}
                        direction={'vertical'}
                        navigation={true}
                        modules={[Autoplay, Mousewheel, Pagination, Navigation]}
                        mousewheel={true}
                        className="mySwiper"
                    >
                        {    // @ts-ignore
                            !pending && transaction.map((item) => {
                            const email = item.User.email
                            const startEmail = email.substring(0, 2)
                            const endEmail = email.substring(email.length - 2, email.length)


                            const fakeEmail = faker.internet.email()
                            const startEmail1 = fakeEmail.substring(0, 2)
                            const endEmail1 = fakeEmail.substring(email.length - 2, email.length)
                            const fakeEmail2 = faker.internet.email()
                            const startEmail2 = fakeEmail2.substring(0, 2)
                            const endEmail2 = fakeEmail2.substring(email.length - 2, email.length)


                            var star = ''
                            var star1 = ''
                            var star2 = ''
                            for (let i = 0; i < email.length - 4; i++) {
                                star += '*'
                            }
                            for (let i = 0; i < fakeEmail.length - 4; i++) {
                                star1 += '*'
                            }
                            for (let i = 0; i < fakeEmail2.length - 4; i++) {
                                star2 += '*'
                            }


                            const type = [
                                'Invest with us' , 'Withdrawal' , 'Interest deposit'
                            ]

                            return <>
                            <SwiperSlide key={item.id}>
                                <div className={styles.withdrawal_summery}>
                                    <div className={styles.withdrawal_summery_header}>
                                        <p className={styles.withdrawal_summery_title}>
                                            {item.type === 'increment from gateway' && 'Invest with us'}
                                            {item.type === 'decrement' && 'Withdrawal'}
                                            {item.type === 'System internal transaction' && 'Interest deposit'}
                                            {item.type === 'increment from wallet' && 'Invest with us'}
                                            <span
                                                className={styles.withdrawal_summery_title_price}>
                                            {item.amount}
                                                <TetherIcon/></span>
                                        </p>
                                        <p className={styles.withdrawal_summery_sub_title}>
                                            {startEmail}{star}{endEmail}

                                        </p>
                                    </div>
                                </div>

                            </SwiperSlide>

                            <SwiperSlide key={item.id}>
                                <div className={styles.withdrawal_summery}>
                                    <div className={styles.withdrawal_summery_header}>
                                        <p className={styles.withdrawal_summery_title}>
                                            {type[Math.floor(Math.random() * 3) ]}

                                            <span
                                                className={styles.withdrawal_summery_title_price}>
                                            {((Math.random() * 5000) + 50).toFixed(2)}
                                                <TetherIcon/></span>
                                        </p>
                                        <p className={styles.withdrawal_summery_sub_title}>
                                            {startEmail1}{star1}{endEmail1}

                                        </p>
                                    </div>
                                </div>

                            </SwiperSlide>


                            <SwiperSlide key={item.id}>
                                <div className={styles.withdrawal_summery}>
                                    <div className={styles.withdrawal_summery_header}>
                                        <p className={styles.withdrawal_summery_title}>
                                            {type[Math.floor(Math.random() * 3) ]}
                                            <span
                                                className={styles.withdrawal_summery_title_price}>
                                              {Math.floor(Math.random() * 5000) + 50}
                                                <TetherIcon/></span>
                                        </p>
                                        <p className={styles.withdrawal_summery_sub_title}>
                                            {startEmail2}{star2}{endEmail2}

                                        </p>
                                    </div>
                                </div>

                            </SwiperSlide>
                        </>


                        })
                        }


                    </Swiper>
                </>


            </div>


        </div>


    </div>
}
