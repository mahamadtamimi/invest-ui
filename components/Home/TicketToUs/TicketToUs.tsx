import styles from '@/styles/home.module.scss'
import {FaqIcon} from "@/components/Home/Faq/FaqIcon";
import {Button, Input} from '@nextui-org/react';
import {useState} from "react";
import {Textarea} from "@nextui-org/input";

export default function TicketToUs() {


    return <div className={styles.ticket_to_us_main_sec}>


        <div className={styles.ticket_form_sec}>
            <form action="">
                <div className={'mb-2'}>
                    <Input label={'email'} type={"email"}/>
                </div>
                <div className={'mb-2'}>

                    <Input label={'subject'} type={"text"}/>
                </div>
                <div className={'mb-2'}>
                    <Textarea label={'massage'} type={"text"}/>
                </div>

                <Button color={'primary'} radius={'none'} className={'text-white w-full'}>Submit</Button>

            </form>


        </div>
        <div className={styles.faq_main_title_sec}>
            <span className={styles.faq_main_title_number}>06</span>
            <span className={styles.faq_main_title_title}>
            We are with you
            </span>

        </div>

    </div>
}