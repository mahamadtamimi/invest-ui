import styles from '@/styles/home.module.scss'
import Image from "next/image";
import background from '@/public/mainBanner.png'
import icMarketLogo from '@/public/Juniper-Logo 1.svg'
import dellIcon from '@/public/dell.09332c44.png'
import registerIcon from '@/public/btnEdit.svg'
import microsoft from '@/public/microsoft.4a9a93f0.png'
import nasa from '@/public/nasa.3bf5af29.png'
import loginIcon from '@/public/btnProfile.svg'
import {Link} from "@nextui-org/link";
import {Button} from "@nextui-org/react";
import {TypeAnimation} from "react-type-animation";
import Marquee from "react-fast-marquee";
import TransactionsSlideShow from "@/components/Home/TrasactionsSlideShow/TransactionsSlideShow";
import largeLogo from '@/public/large-logo.svg'
import {useSelector} from "react-redux";
import {useState} from "react";
import {useRouter} from "next/router";

export default function MainSlider() {

    // @ts-ignore
    const user = useSelector((state) => state.user);
    const [userWallet, setUserWallet] = useState()

    const router = useRouter();


    return <>
        <section className={styles.banner}>
            <Image src={background} className={styles.main_background} alt=''/>

            <div className={styles.banner__content}>
                <div className={styles.banner__content__title}>
                    <h1 className={styles.banner__content__title__text}>
                        <Image src={largeLogo} alt={''} width={400}/>
                    </h1>
                    <span className={styles.banner__content__title_span}>
                     Safe investment, unique profit
                </span>
                </div>
                <div className={styles.banner__content__desc}>

                </div>

                {
                    !user &&
                    <>
                        <div className={styles.banner__content__button}>
                            <Button as={Link} href='/auth/register' className="btn">
                      <span>
                        <Image src={registerIcon} alt=''/>
                      </span>
                                REGISTRATION
                            </Button>
                            <Button as={Link} href='/auth/login' className={`btn-dark`}>
                      <span>
                        <Image src={loginIcon} alt=''/>

                      </span>
                                SIGN IN
                            </Button>
                        </div>
                    </>


                }


            </div>
            <TransactionsSlideShow/>
        </section>

    </>

}