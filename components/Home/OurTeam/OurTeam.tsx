import image from '@/public/aboutUsBanner1.png'

import Image from "next/image";
import linkedInIcon from '@/public/_Linkedin.svg'
import styles from "@/styles/home.module.scss";

import React, { useRef, useState } from 'react';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import {Autoplay, Mousewheel, Navigation, Pagination} from 'swiper/modules';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import background from "@/public/mainBanner.png";


export default function OurTeam() {
    return <section className={styles.team}>
        <Image src={background} className={styles.main_background} alt=''/>
        <div className={`${styles.plans__title} ${styles.its_right}`}>
            <span className={`${styles.plans__title_sub} ${styles.its_right}`}> 04</span>
            <h1 className={`${styles.plans__title__text} ${styles.its_right}`}>Professors and faculty</h1>
        </div>
        <div className={styles.team__body}>

            <div className={styles.our_team_slider}>


                <>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        pagination={{
                            type: 'progressbar',
                        }}
                        autoplay={{
                            delay: 1000,
                            disableOnInteraction: false,
                        }}
                        direction={'vertical'}
                        navigation={true}
                        modules={[Autoplay, Mousewheel, Pagination, Navigation]}
                        mousewheel={true}
                        className="mySwiper"
                    >
                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>
                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                        <SwiperSlide>
                            <div className={styles.team__card}>
                                <div className={styles.team__card__img}>
                                    <Image src={image} alt=''/>
                                </div>
                                <div className={styles.team__card__body}>
                                    <h1 className={styles.team__card__body__title}>MARCO REIMANIN</h1>
                                    <p className={styles.team__crad__body__desc}>SUPPORT TEAM</p>
                                    <span className={styles.team__card__body__icon}>
                                            <Image className={styles.team__card__body__icon__img} src={linkedInIcon}
                                                   alt=''/>
                                        </span>
                                </div>

                            </div>

                        </SwiperSlide>

                    </Swiper>
                </>


            </div>
            <div className={styles.our_team_description}>



                <p className={styles.our_team_description_text}>
                    Our team consists of 24 experienced and highly knowledgeable
                    specialists in various areas of construction. This multidisciplinary
                    team includes civil engineers, architects, project managers, real
                    estate experts, and financial analysts, each playing a crucial role in
                    the success of our projects.
                    The core of the team is a group of experienced civil engineers and
                    architects with over 15 years of experience in designing and
                    executing various residential and commercial projects. Utilizing their
                    specialized knowledge and skills, they guide projects from the
                    ideation stage through to final execution.
                    The project management division comprises professional and
                    seasoned managers responsible for planning, coordinating, and
                    overseeing the project execution process. By managing timelines,
                    resources, and costs, they ensure projects are completed within the
                    designated time and budget.
                    Our real estate experts and financial analysts play a vital role in
                    identifying profitable investment opportunities and analyzing the
                    economic aspects of projects. Through thorough market and financial
                    data analysis, they recommend the best investment options.
                    Our diverse and specialized team, through collaborative efforts and
                    seamless coordination, has successfully delivered numerous projects
                    over the years. We continue to focus on developing our teams skills
                    and knowledge to provide superior services to our investors.

                </p>

            </div>


        </div>
    </section>
}