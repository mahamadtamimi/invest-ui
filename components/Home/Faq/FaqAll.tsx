import styles from '@/styles/home.module.scss'
import {FaqIcon} from "@/components/Home/Faq/FaqIcon";
import {Button} from '@nextui-org/react';
import {useState} from "react";

export default function FaqAll() {
    const [Faq, setFaq] = useState(
        [
            {
                title: 'How can I participate in one of the investment plans?',
                description:
                    'To participate in the investment plans, you first need to register on\n' +
                    'our website. Then, you can go to the "Investment Plans" section,\n' +
                    'select the plan you want, and pay the investment amount.'

            }
            ,

            {
                title: 'Is there any risk in investing in your plans?',
                description:
                    'We try to minimize the risks of our investment plans as much as\n' +
                    'possible. However, like any other investment, there is a small amount\n' +
                    'of risk involved. We have clearly stated the risks for each plan on its\n' +
                    'description page.'
            }
            ,

            {
                title: 'How can I receive the profit from my investment?',
                description:
                    'The profit from your investment will be credited to your account\n' +
                    'based on the time period of each plan (weekly, monthly, three\n' +
                    'months, six months or yearly). You can request a withdrawal of your\n' +
                    'profits through your user panel.'
            }
            ,

            {
                title: 'What is the minimum investment amount for the plans?',
                description:
                    'The minimum investment amount varies for each plan. This\n' +
                    'amount is stated on the description page of the respective plan. But\n' +
                    'in general, it starts from $50.'
            }
            ,

            {
                title: 'Can I choose multiple plans simultaneously?',
                description:
                    'Yes, it is possible to invest in multiple different plans at the same\n' +
                    'time. This allows you to have a more diversified investment portfolio.'
            },


            {
                title: 'How long does it take for a new plan to be approved and started\n' +
                    'after payment?',
                description: 'Yes, it is possible to invest in multiple different plans at the same\n' +
                    'time. This allows you to have a more diversified investment portfolio.'
            },

            {
                title: 'Can I withdraw my investment at any time?',
                description: 'No, unfortunately it is not possible to withdraw your investment\n' +
                    'before the completion of the plan\'s time period. Your capital is\n' +
                    'invested in projects for the specified duration (weekly, monthly, etc.).\n' +
                    'After that, you can receive your capital along with the profits.',
            },
            {
                title: 'Is there any limit on the number of times I can invest?',
                description: 'No, there is no limit on the number of times you can invest. You\n' +
                    'can invest in one of the plans whenever you wish.',
            },
            {
                title: 'Who cannot invest in your plans?',
                description: 'Individuals under 18 years old, nationals of some sanctioned\n' +
                    'countries, and those on sanctions lists are not allowed to invest in\n' +
                    'our plans.'
            },
            {
                title: 'Is there an option to get advice on selecting a plan?',
                description: 'Yes, our experienced consultants can recommend a suitable plan\n' +
                    'for you based on your circumstances, goals, and risk tolerance. You\n' +
                    'can request a consultation through the "Contact Us" section.'
            },
            {
                title: 'What happens in case of the investor\'s demise?',
                description: 'In the event of the investor\'s demise, the invested capital and\n' +
                    'accrued profits can be transferred to their legal heirs based on their\n' +
                    'last will and testament or inheritance laws.'
            },
            {
                title: 'Are the investment plans renewable?',
                description: 'Yes, after the completion of a plan\'s duration, there is an option to\n' +
                    'renew it for a new time period. In this case, the received capital and\n' +
                    'profits can be reinvested.'
            },
            {
                title: 'Why should we invest on this website?',
                description: 'There are many reasons such as high profitability, secure\n' +
                    'investments, diverse and reliable projects, complete transparency,\n' +
                    'and 24/7 support that have made our website the choice of many\n' +
                    'investors.'
            },
            {
                title: 'After investing, will I not have any responsibilities?',
                description: 'Yes, after investing in one of the plans, all executive, management,\n' +
                    'and supervisory matters will be handled by our professional team,\n' +
                    'and you will not need to be actively involved.'
            },
            {
                title: 'After investing, will I not have any responsibilities?',
                description: 'Yes, after investing in one of the plans, all executive, management,\n' +
                    'and supervisory matters will be handled by our professional team,\n' +
                    'and you will not need to be actively involved.'
            },
            {
                title: 'Is it possible to cancel an investment before the start of its period?',
                description: 'No. After activating a plan, your profit will be calculated from that\n' +
                    'moment onwards and cancellation will not be possible.'
            },
            {
                title: 'How do you ensure the security of the investment plans?',
                description: 'We follow very stringent security standards to protect investors\'\n' +
                    'capital. This includes the use of strong encryption, advanced anti-\n' +
                    'hacking systems, secure data storage, as well as reputable insurance\n' +
                    'coverage for potential risks.'
            },
            {
                title: 'Is it possible to receive the investment profit in installments?',
                description: 'Yes, for some plans there is an option to receive the profits in\n' +
                    'installments over shorter periods than the main plan duration. This\n' +
                    'option is suitable for investors who need a regular cash flow.'
            },
            {
                title: 'Is there a limit on the amount that can be invested in a plan?',
                description: 'Yes, there is an investment cap set for each plan. This cap is\n' +
                    'defined based on the capacity of the projects associated with that\n' +
                    'particular plan. The cap amount is stated on the description page of\n' +
                    'each plan.'
            },
            {
                title: 'Who is allowed to use the website and invest?',
                description: 'All natural and legal persons who have legal eligibility can register\n' +
                    'on our website and invest. However, underage individuals, nationals\n' +
                    'of certain countries, and sanctioned persons are not permitted to use\n' +
                    'the website.'
            },
            {
                title: 'Are investors charged any taxes or fees?',
                description: 'No, we do not charge investors any additional taxes or fees. All the\n' +
                    'advertised profits for each plan are gross profits after deducting\n' +
                    'operational costs and legal taxes. In other words, taxes and fees have\n' +
                    'already been deducted from the profits, and the stated profit is the net\n' +
                    'amount you will receive. We emphasize complete transparency in this\n' +
                    'regard so that investors can invest with full confidence.'
            },
            {
                title: 'Can investments be made remotely and online in the plans?',
                description: 'Yes, the entire investment process including registration, plan\n' +
                    'selection, online payment, and receiving profits can be done\n' +
                    'completely online and remotely. You can access our website from\n' +
                    'anywhere in the world.'
            },
            {
                title: 'What are the payment methods for investments?',
                description: 'We will accept investment amounts from investors via various\n' +
                    'stablecoins.'
            },
            {
                title: 'How can I be assured of the security of financial transactions?',
                description: 'To ensure the security and confidentiality of financial transactions,\n' +
                    'we use strong encryption protocols such as SSL. We also collaborate\n' +
                    'with reputable banks and online payment companies.'
            },
            {
                title: 'Is there an option to refer new investors and receive a reward?',
                description: 'Yes, we have a new investor referral program. If you refer a new\n' +
                    'investor to the site, you will receive a special reward after that person\n' +
                    'makes their first investment.'
            },
            {
                title: 'How can I stay updated on the latest news and updates from the\n' +
                    'website?',
                description: 'You can subscribe to our email newsletter to receive the latest\n' +
                    'news, updates, and important announcements. You can also follow\n' +
                    'our official channels on social media platforms.'
            },
            {
                title: 'Do you have any programs to support charitable and public\n' +
                    'welfare projects?',
                description: 'Yes, we annually allocate a portion of our revenue to charitable and\n' +
                    'public welfare projects such as building schools, health centers, and\n' +
                    'nursing homes. Investors can also participate in these projects.'
            },
            {
                title: 'What is the guarantee of return on investment at the end of the\n' +
                    'course?',
                description: 'We provide valid insurances to cover the risk of investment\n' +
                    'return.Also our diverse projects are possible to replace and they\n' +
                    'support each other so that your balance is guaranteed.'
            },
            {
                title: 'Is there a fee for consulting services?',
                description: 'No, providing free consulting services in the field of choosing the\n' +
                    'right plan, risk management and other matters for investors are part\n' +
                    'of our obligations and no extra fee is charged.'
            },
            {
                title: 'Is it possible to transfer the investment to legal heirs?',
                description: 'Yes, in the event of the investor\'s death, the capital and accrued\n' +
                    'profits can be transferred to their legal heirs based on the latest will\n' +
                    'or inheritance laws. For this, the necessary documents must be\n' +
                    'provided by the heirs.'
            },
            {
                title: 'Who is authorized to view my personal information?',
                description: 'Your personal information is completely confidential. Only\n' +
                    'authorized employees in our financial and legal departments have\n' +
                    'limited access to this information for carrying out leg'
            },
            {
                title: 'How can I stay updated on the latest changes in laws and\n' +
                    'regulations?',
                description: 'We update the latest changes in laws and regulations related to the\n' +
                    'construction and investment industry in the News and Events section\n' +
                    'of our website. You can follow the updates in that section.'
            },
            {
                title: 'Is it possible to provide suggestions and feedback?',
                description: 'Yes, we welcome any suggestions, criticisms, or feedback from\n' +
                    'investors. You can submit your comments to us through the "Contact\n' +
                    'Us" section on our website.'
            },

        ])


    const [showItem, setShowItem] = useState(0)
    return <div className={styles.faq_main_sec}>
        <div className={styles.faq_main_title_sec}>
            <span className={styles.faq_main_title_number}>FAQ</span>
            <span className={styles.faq_main_title_title}>
                investHub asked questions
            </span>


        </div>

        <div className={styles.faq_item_sec}>
            {Faq.map((item, index) => {


                return <div key={item.title} className={styles.faq_item}>
                    <div className={styles.faq_item_head} onClick={() => setShowItem(index)}>
                        <FaqIcon/>
                        <h2>
                            {item.title}
                        </h2>

                    </div>
                    {
                        showItem === index && <div className={styles.faq_item_body}>
                            {item.description}
                        </div>

                    }


                </div>

            })}

        </div>


    </div>
}