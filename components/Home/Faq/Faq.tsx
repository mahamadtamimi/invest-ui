import styles from '@/styles/home.module.scss'
import {FaqIcon} from "@/components/Home/Faq/FaqIcon";
import {Button, Link} from '@nextui-org/react';
import {useState} from "react";

export default function Faq() {
    const [Faq, setFaq] = useState(
        [
            {
                title: 'How can I participate in one of the investment plans?',
                description:
                    'To participate in the investment plans, you first need to register on\n' +
                    'our website. Then, you can go to the "Investment Plans" section,\n' +
                    'select the plan you want, and pay the investment amount.'

            }
            ,

            {
                title: 'Is there any risk in investing in your plans?',
                description:
                    'We try to minimize the risks of our investment plans as much as\n' +
                    'possible. However, like any other investment, there is a small amount\n' +
                    'of risk involved. We have clearly stated the risks for each plan on its\n' +
                    'description page.'
            }
            ,

            {
                title: 'How can I receive the profit from my investment?',
                description:
                    'The profit from your investment will be credited to your account\n' +
                    'based on the time period of each plan (weekly, monthly, three\n' +
                    'months, six months or yearly). You can request a withdrawal of your\n' +
                    'profits through your user panel.'
            }
            ,

            {
                title: 'What is the minimum investment amount for the plans?',
                description:
                    'The minimum investment amount varies for each plan. This\n' +
                    'amount is stated on the description page of the respective plan. But\n' +
                    'in general, it starts from $50.'
            }
            ,

            {
                title: 'Can I choose multiple plans simultaneously?',
                description:
                    'Yes, it is possible to invest in multiple different plans at the same\n' +
                    'time. This allows you to have a more diversified investment portfolio.'
            }
            ,


        ])


    const [showItem, setShowItem] = useState(0)
    return <div className={styles.faq_main_sec}>
        <div className={styles.faq_main_title_sec}>
            <span className={styles.faq_main_title_number}>05</span>
            <span className={styles.faq_main_title_title}>
                investHub asked questions
            </span>

            <Button as={Link} href={'/faq'} radius={'none'} color={'primary'} className={styles.faq_main_title_btn}>
                Read More
            </Button>
        </div>

        <div className={styles.faq_item_sec}>
            {Faq.map((item, index) => {


                return <div key={item.title} className={styles.faq_item}>
                    <div className={styles.faq_item_head} onClick={()=> setShowItem(index)}>
                        <FaqIcon/>
                        <h2>
                            {item.title}
                        </h2>

                    </div>
                    {
                        showItem === index && <div className={styles.faq_item_body}>
                            {item.description}
                        </div>

                    }


                </div>

            })}

        </div>


    </div>
}