import styles from '@/styles/home.module.scss'
import testMony from '@/public/testMony.svg'
import Image from "next/image";

export default function CryptoPrice() {
    return <>
        <section className={styles.card__prices}>
            <div className={styles.crad__price}>
                <div className={styles.card__price__icon}>
                    <Image
                        className={styles.card__price__img}
                        width="50"
                        src={testMony}
                        alt=''
                    />
                </div>
                <div className={styles.card__price__content}>
                    <h1 className={styles.card__price__title }>$36.700.82</h1>
                    <div className={styles.card__price__desc}>
                        <span className={styles.crad__price__desc__namad}> MLN/USD </span>
                        <span className={styles.crad__price__desc__namad }>

              37AC6C
            </span>
                    </div>
                </div>
            </div>
        </section>
    </>

}