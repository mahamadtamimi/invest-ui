import styles from '@/styles/home.module.scss'
import Image from "next/image";
import background from "@/public/mainBanner.png";
export default function OurAdvantages() {
    return <section className={styles.advantages_sec}>
        <Image src={background} className={styles.main_background} alt=''/>
        <div className={`${styles.plans__title} ${styles.its_right}`}>
            <span className={`${styles.plans__title_sub} ${styles.its_right}`}> 02</span>
            <h1 className={`${styles.plans__title__text} ${styles.its_right}`}>OUR ADVANTAGES</h1>
        </div>
        <div className={styles.advantages}>
            <div className={styles.advantages__cards}>
                <div className={styles.advantages__card}>
                    <h1 className={styles.advantages__card__title}>Experienced and Expert Team </h1>
                    <div className={styles.advantages__card__body}>
                        <p className={styles.advantages__card__text}>
                            One of the biggest advantages of investing with our site is having an
                            experienced and expert team. Our analysts and experts, with years of
                            experience in the investment field, make the best decisions and
                            strategies for you to help you achieve high returns.
                        </p>
                    </div>
                    </div>


                <div className={styles.advantages__card}>
                    <h1 className={styles.advantages__card__title}>Diverse Investment Options</h1>
                    <div className={styles.advantages__card__body}>
                        <p className={styles.advantages__card__text}>
                            On our site, you can take advantage of various diverse investment
                            options such as short-term, long-term, high-risk, low-risk, and more.
                            This diversity allows you to optimize your investment portfolio
                            according to your goals, risk tolerance, and desired time horizon.
                        </p>
                    </div>
                </div>

                <div className={styles.advantages__card}>
                    <h1 className={styles.advantages__card__title}>High Investment Returns</h1>
                    <div className={styles.advantages__card__body}>
                        <p className={styles.advantages__card__text}>
                            One of the biggest advantages of investing with our site is the high
                            investment returns we offer our users. Through smart investment
                            strategies and an expert team, we have been able to deliver
                            remarkable investment returns for our investors.
                        </p>
                    </div>
                </div>

                <div className={styles.advantages__card}>
                    <h1 className={styles.advantages__card__title}>Innovative Investment Strategies</h1>
                    <div className={styles.advantages__card__body}>
                        <p className={styles.advantages__card__text}>
                            One of the major advantages of investing with our site is the use of
                            innovative and advanced investment strategies. Our research and
                            development team is always exploring and implementing the latest
                            investment methods and algorithms to bring you the best possible
                            returns.
                        </p>
                    </div>
                </div>


            </div>

        </div>
    </section>

}