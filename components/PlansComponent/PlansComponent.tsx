'use client'
import styles from "@/styles/aboutUs.module.scss";
import {BreadcrumbItem, Breadcrumbs, Button} from "@nextui-org/react";
import Image from "next/image";
import slide1 from "@/public/slide-1.png";
import Footer from "@/components/Footer/Footer";
import {useEffect, useState} from "react";

export default function PlansComponent() {

    const [plans, setPlans] = useState()

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/plans/list`)
            .then((res) => res.json())
            .then((data) => {
                setPlans(data)

            })
    }, [])
    const descriptions = [
        [
            '- Profit is added to your initial capital after 14 days',
            '- Investment in profitable, low-risk door and window repair projects',
            '- Quick return of capital and profit after 14 days',

            'Here are the features of the Weekly/7 Day investment plan',

            'For short-term investors, the weekly investment plan is an ideal option. In this plan, your capital is used in projects to repair and renovate dilapidated doors and windows of buildings and companies.'

        ],
        [
            '- Profit is added to your initial capital after one month',
            '- Investment in profitable interior design projects',
            '- Return of capital and profit after 30 days',

            'Here are the features of the Monthly/30 Day investment plan',
            'For investors seeking medium-term returns, the monthly plan is a suitable option. In this plan, your capital is used in interior design projects for buildings and companies.'
        ],
        [

            '- Profit is added to your initial capital after 3 months',
            '- Investment in sustainable, high-quality wall and ceiling repair projects',
            '- Return of capital and profit after 90 days',

            'Here are the features of the Three Months/90 Day investment plan' ,

            'For investors seeking medium-term returns with low risk, the 3-months plan is an ideal option. In this plan, your capital is used in wall and ceiling repair and renovation projects for buildings and companies.'
        ],
        [
            '- Investment Profit: Added to your initial capital after 6 months',
            '- Investment in high-demand small residential housing projects',
            '- Return of capital and profit at the end of the 6-months period',

            'Here are the features of the Six Months/180 Day investment plan:' ,
            'For medium-term investors seeking significant returns, the 6-months plan is an attractive option. In this plan, your capital is used in small residential housing construction projects.'
        ],


    ]

    // @ts-ignore
    return <>
        <div className={styles.bread_crumbs_sec}>
            <Breadcrumbs>
                <BreadcrumbItem href={'/'}>Home</BreadcrumbItem>
                <BreadcrumbItem>Plans</BreadcrumbItem>
            </Breadcrumbs>

        </div>
        <section className={styles.plans_tables_sec}>

            <table className={styles.plans_tables}>
                <thead>
                <tr>
                    <th></th>
                    {
                        // @ts-ignore
                        plans && plans.map((item) => {
                            return <th className={styles.plans_page_head} key={`title-${item.id}`}>{item.name}</th>
                        })
                    }


                </tr>

                </thead>
                <tbody>
                <tr>
                    <th>
                        description
                    </th>
                    {    // @ts-ignore
                        plans && descriptions.map((item, index) => {
                            return <td key={`description-${index}`}>{item[4]}</td>
                        })
                    }


                </tr>
                <tr>
                    <th>

                    </th>
                    {
                        plans && descriptions.map((item, index) => {
                            return <td key={`descriptio-3-${index}`}>{item[3]}</td>
                        })
                    }


                </tr>
                <tr>
                    <th>
                        Investment Period:
                    </th>
                    {    // @ts-ignore
                        plans && plans.map((item) => {
                            return <td key={`daily-${item.id}`}>{item.dailyCredit} days</td>
                        })
                    }


                </tr>
                <tr>
                    <th>

                    </th>
                    {    // @ts-ignore
                        plans && descriptions.map((item, index) => {
                            return <td key={`descriptio-0-${index}`}>{item[0]}</td>
                        })
                    }


                </tr>
                <tr>
                    <th>

                    </th>
                    {    // @ts-ignore
                        plans && descriptions.map((item, index) => {
                            return <td key={`descriptio-1-${index}`}>{item[1]}</td>
                        })
                    }


                </tr>
                <tr>
                    <th>

                    </th>
                    {
                        plans && descriptions.map((item, index) => {
                            return <td key={`descriptio-2-${index}`}>{item[2]}</td>
                        })
                    }


                </tr>

                <tr>
                    <th>
                        Minimum Investment
                    </th>
                    {    // @ts-ignore
                        plans && plans.map((item) => {
                            return <td key={`minimum-deposit-${item.id}`}>{item.minimumDeposit} $</td>
                        })
                    }


                </tr>
                <tr>
                    <th>

                    </th>



                </tr>
                </tbody>


            </table>


        </section>


        <Footer/>

    </>


}