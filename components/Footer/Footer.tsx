import styles from '@/styles/footer.module.scss'
import Image from "next/image";
import footerBack from '@/public/footerBack.png'
import logo from "@/public/log.svg";
import React from "react";
import {Link} from "@nextui-org/react";
import nowPaymentLogo from '@/public/nowPaymentGateway.png'
import tronLogo from '@/public/tronLogo.png'
import instagramIcon from '@/public/_Instagram.svg'
import tether from '@/public/usdt_bep20.svg'
import tether2 from '@/public/usdt_trc20.svg'
import telegramIcon from '@/public/_Telegram.svg'
import linkedIn from '@/public/_Linkedin.svg'
import X from '@/public/x-social-media-round-icon.svg'


export default function Footer() {
    return <section className={styles.footer}>
        <Image className={styles.footer_back} src={footerBack} alt=''/>
        <div className={styles.footer__card}>
            <div className={styles.main__of_footer}>
                <div className={styles.footer__card__left}>
                    <div className={styles.footer__card__left__logo}>
                  <span className={styles.footer__card__left__logo__it}>
                        <Image src={logo} alt=''/>
                  </span>
                    </div>
                    <div className={styles.footer__card__left__desc}>
                        <p className={styles.footer__crad__left__dcsc__text}>
                            Making th world more beautiful , happier , easier
                        </p>
                    </div>
                </div>
                <div className={styles.footer__card__right}>


                    <div className={styles.footer__card__link}>
                        <Link href="/" className={styles.footer__link}>Home</Link>
                        <Link href="/about-us" className={styles.footer__link}>About Us</Link>
                        <Link href="/faq" className={styles.footer__link}>FAQ</Link>
                        <Link href="/plans" className={styles.footer__link}>Plans</Link>


                    </div>
                    <div className={styles.footer__card__link}>
                        <Link href="/auth/login" className={styles.footer__link}>Login</Link>
                        <Link href="/auth/register" className={styles.footer__link}>Signup</Link>
                        <Link href="/auth/login" className={styles.footer__link}>Ticket And Support</Link>


                    </div>
                </div>
                <div className={styles.footer__card__aline__end}>
                    <div className={styles.footer__card__aline__height_test}>
                        <div>
                            <div className={styles.footer__card__link__social_link}>
                                <Image src={tether} alt={''} width={40} height={40}/>
                                <Image src={tether2} alt={''} width={40} height={40}/>
                            </div>
                            <div className={styles.footer__card__link}>
                                <div className={styles.mobile_center}>
                                    <Image src={nowPaymentLogo} width={200} alt={''}/>
                                </div>

                            </div>


                        </div>
                        <div>
                            <p className={styles.footer__card__pragh_pragh}>
                                Follow us :
                            </p>

                            <div className={styles.footer__card_social__media_links}>
                                <Link href={'https://www.instagram.com/investitionhub/'}>

                                    <Image src={instagramIcon} alt={''} width={34} height={34}/>
                                </Link>
                                <Link href={'https://t.me/InvestitionHub'}>
                                    <Image src={telegramIcon} alt={''} width={34} height={34}/>
                                </Link>
                                <Link href={'https://t.me/InvestitionHubGroup'}>
                                    <Image src={telegramIcon} alt={''} width={34} height={34}/>
                                </Link>


                            </div>

                        </div>

                    </div>

                </div>

            </div>


            <div className={styles.footer__card__aline__end}>

            </div>
            <div className={styles.copywrite_text}>
                All rights reserved

            </div>

        </div>

    </section>


}