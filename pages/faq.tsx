import DefaultLayout from "@/layouts/default";

import FAQComponent from "@/components/FAQComponent/FAQComponent";


export default function faq() {
    return <DefaultLayout>
        <FAQComponent/>
    </DefaultLayout>
}