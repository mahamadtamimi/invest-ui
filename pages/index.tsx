import DefaultLayout from "@/layouts/default";
import MainSlider from "@/components/Home/MainSlider/MainSlider";
import CryptoPrice from "@/components/Home/CryptoPrice/CryptoPrice";
import Plans from "@/components/Home/Plans/Plans";
import OurAdvantages from "@/components/Home/OurAdvantages/OurAdvantages";
import AboutUs from "@/components/Home/AboutUs/AboutUs";
import OurTeam from "@/components/Home/OurTeam/OurTeam";
import HomeParchasePlan from "@/components/Home/HomeParchasePlan/HomeParchasePlan";
import Footer from "@/components/Footer/Footer";
import TransactionsSlideShow from "@/components/Home/TrasactionsSlideShow/TransactionsSlideShow";
import Faq from "@/components/Home/Faq/Faq";
import TicketToUs from "@/components/Home/TicketToUs/TicketToUs";




export default function IndexPage() {
    return (
        <DefaultLayout>

            <MainSlider/>

            {/*<CryptoPrice/>*/}
            <HomeParchasePlan/>

            {/*<Plans/>*/}
            <OurAdvantages/>
            <AboutUs/>
            <OurTeam/>
            <Faq/>
            {/*<TicketToUs />*/}
            <Footer/>

        </DefaultLayout>
    );
}
