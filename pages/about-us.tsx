import DefaultLayout from "@/layouts/default";
import AboutUsComponent from "@/components/AboutUsComponent/AboutUsComponent";


export default function aboutUs() {
    return <DefaultLayout>
        <AboutUsComponent/>
    </DefaultLayout>
}