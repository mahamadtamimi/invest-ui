import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import UserArchive from "@/components/admin/UsersArchive/UserArchive";
import AdminHead from "@/components/admin/AdminHead";

export default function list() {
    // @ts-ignore
    return <AdminLayout>
        <AdminHead title="Users List" />
        <
            // @ts-ignore
            UserArchive/>

    </AdminLayout>
}