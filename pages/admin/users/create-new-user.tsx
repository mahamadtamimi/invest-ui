import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import UserArchive from "@/components/admin/UsersArchive/UserArchive";
import CreateNewUser from "@/components/admin/CreateNewUser/CreateNewUser";
import AdminHead from "@/components/admin/AdminHead";

export default function createNewUser() {
    // @ts-ignore
    return <AdminLayout>


        <AdminHead title="Create new user" />

        <
            // @ts-ignore
            CreateNewUser/>

    </AdminLayout>
}