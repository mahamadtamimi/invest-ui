import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import UserArchive from "@/components/admin/UsersArchive/UserArchive";
import AdminHead from "@/components/admin/AdminHead";
import UserInfo from "@/components/admin/UserInfo/UserInfo";

export default function list() {

    return <AdminLayout>
        <AdminHead title="User Info"/>

        <UserInfo/>

    </AdminLayout>
}