import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import Summery from "@/components/admin/Summery/Summery";
import {useSelector} from "react-redux";


export default function index() {



    // @ts-ignore
    // const user = useSelector((state) => state.user);



    return <AdminLayout>

        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>DASHBOARD</h1>
                {/*<span> WELCOME BACK, {user.firstName} {user.lastName} </span>*/}
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>
        </div>

        <p>
            <Summery/>
        </p>

    </AdminLayout>

}