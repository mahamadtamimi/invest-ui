import DashboardLayout from "@/layouts/dashboard";
import {useRouter} from "next/router";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {Button, Link, user} from "@nextui-org/react";
import {useSelector} from "react-redux";
import style from '@/styles/chat.module.scss'
import {PlusIcon} from "@/components/Dashboard/TicketList/PlusIcon";
import {MassgeIcon} from "@/components/Dashboard/TicketList/MassgeIcon";
import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import AdminChatBody from "@/components/Chat/AdminChatBody";


export default function Tickets({id}: { id: string }) {
    const router = useRouter();
    const [data, setData] = useState()
    const [pending, setPending] = useState(true)
    const [itemSelected, setItemSelected] = useState(0)
    const [chatController, setChatController] = useState({
        value: '',
        validate: false
    })

    // @ts-ignore
    const user = useSelector((state) => state.user);
    const scrollToBottom = () => {
        window.document.getElementById('massageEmail')?.scrollTo({
            top: window.document.getElementById('massageEmail')?.scrollHeight,
            behavior: 'smooth',
        });
    };
    useEffect(() => {


        fetch(`${process.env.API_PATH}/api/v1/admin/tickets`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setData(data)
                setPending(false)

                console.log(data)


            })


    }, [])
    useEffect(() => {
        const timeoutId = setTimeout(() => {
            fetch(`${process.env.API_PATH}/api/v1/admin/tickets`, {
                headers: {
                    Authorization: `bearer ${user.token}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    setData(data)
                    setPending(false)

                    // console.log(data)

                })

        }, 60000);

        // Cleanup function to clear the timeout if the component unmounts
        return () => clearTimeout(timeoutId);


    }, [data])


    useEffect(() => {
        scrollToBottom()
    }, [data]);

    function NotFound() {
        return <div className={style.not__found}>
            <p>
                No ticket has been created yet !
            </p>


            <Button color={'primary'}
                    endContent={<PlusIcon/>}
                    className={'text-white mt-4 mb-2'} as={Link} href={'/auth/dashboard/new-ticket'}>
                New Ticket
            </Button>
        </div>;
    }



    function closeTicket(e:any){

        const userData = {
            // @ts-ignore
            ticketId: data[itemSelected].id,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");

        fetch(`${process.env.API_PATH}/api/v1/close-tickets` , {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded',
            },
            // @ts-ignore
            body: formBody
        }).then(res => res.json()).then((data) => {setData(data)})
    }

    function SideItem() {
        return <ul className={style.subject__list_ul}>

            {

                // @ts-ignore
                data.map((item, index) => {

                    const newMassage = item.TicketBodies.filter((item :any) => {
                        return (item.side == 0 && item.seen == false)
                    }).length

                    return <li key={item.id}
                               onClick={() => setItemSelected(index)}
                               className={`${style.subject__list_li} ${itemSelected === index && style.item_chat_selected}`}>
                        <MassgeIcon/>

                        <p className={style.admin_item_prag}>
                            <span>
                                     {item.subject}
                                {newMassage ? <span className={style.have_new_massage}>{newMassage}</span> : ''}
                            </span>
                            <span>
                                     {item.User.email}
                            </span>

                        </p>

                    </li>
                })}
        </ul>
    }


    function MassageSideBar() {
        return !pending ? <>
                {
                    // @ts-ignore
                    data.length ?
                        <SideItem/>
                        :
                        <NotFound/>}
            </> :
            <p>loading</p>


    }

    function ChatHeader() {

        return <div className={style.chat_main_header}>
            {!pending && <>
                {// @ts-ignore
                    data.length ?
                        <>
                            <h3 className={style.subject_name}>
                                {
                                    // @ts-ignore
                                    data[itemSelected].subject}
                            </h3>


                            <span className={style.subject_description}>
                        <Button>
                                 pending

                        </Button>
                        <p>
                              Did you get your answer?<Button onClick={(e)=>closeTicket(e)} className={style.link_new} as={Link}>
                                close ticket </Button>
                        </p>


                    </span>

                        </> : ''
                } </>}


        </div>
    }

    function checkMassage(e: any) {
        const massage = e.target.value


        setChatController({
            ...chatController,
            value: massage,
            validate: massage.length > 0
        })

    }


    function sendMassage(e: any) {
        e.preventDefault()

        const userData = {
            // @ts-ignore
            ticketId: data[itemSelected].id,
            text: chatController.value,
            side: 1
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/admin/tickets/answer`, {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded',
            },
            // @ts-ignore
            body: formBody
        }).then(res => res.json()).then((data) => {
            setData(data)
            setChatController({
                ...chatController,
                value: '',
                validate: false
            })
        })
    }


    return <AdminLayout>

        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>Tickets</h1>
                <span> WELCOME BACK, </span>
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>
        </div>

        <div className={style.chat_main_section}>
            <div className={style.subject__list__sec}>
                <MassageSideBar/>

            </div>
            <div className={style.chat_main__sec}>
                <ChatHeader/>


                {// @ts-ignore
                    !pending && data.length ?
                        <AdminChatBody chats={
                            // @ts-ignore
                            data[itemSelected]}
                                       user={
                                           // @ts-ignore
                                           data[itemSelected].User.email}/> : ''}


                <div className={style.chat_main__all_footer__sec}>
                    {
                        !pending &&
                        <>
                            <form className={style.chat__form} action="" onSubmit={(e) => sendMassage(e)}>
                                <input type={'text'} disabled={
                                    // @ts-ignore
                                    data.length == 0}
                                       className={`${style.chat_main__input__main} ${
                                           // @ts-ignore
                                           data.length == 0 && style.btn__disable}`}
                                       placeholder={'enter massage'}
                                       value={chatController.value}
                                       onChange={(e) => checkMassage(e)}
                                />
                                <Button color={'primary'}
                                        type={'submit'}
                                        isDisabled={
                                            // @ts-ignore
                                            data.length == 0 && !chatController.validate}
                                        className={`text-white ${
                                            // @ts-ignore
                                            data.length == 0 && style.btn__disable} 
                                    ${!chatController.validate && style.btn__disable}`}>

                                    send
                                </Button>
                            </form>
                        </>

                    }


                </div>

            </div>


        </div>
    </AdminLayout>
}