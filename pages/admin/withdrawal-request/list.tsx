import {useEffect} from "react";
import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";
import UserArchive from "@/components/admin/UsersArchive/UserArchive";
import WithdrawalArchive from "@/components/admin/WithdrawalArchive/WithdrawalArchive";

export default function list() {
    return <AdminLayout>
        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>Users</h1>
                <span> WELCOME BACK, MARYAM JAFARPOUR </span>
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>
        </div>

        <WithdrawalArchive/>

    </AdminLayout>


}