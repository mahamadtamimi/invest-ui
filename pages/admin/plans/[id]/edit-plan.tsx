import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";

import AddPlan from "@/components/admin/AddPlan/AddPlan";
import EditPlan from "@/components/admin/AddPlan/EditPlan";

export default function editPlan(){
    return <AdminLayout>

        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>ADD PlAN</h1>
                <span> WELCOME BACK, MARYAM JAFARPOUR </span>
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>
        </div>

        <EditPlan/>

    </AdminLayout>

}