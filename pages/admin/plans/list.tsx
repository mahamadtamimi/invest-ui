import PlanArchive from "@/components/admin/PlansArchive/PlanArchive";
import AdminLayout from "@/layouts/admin";
import styles from "@/styles/dashboard.module.scss";


export default function index(){
    return <AdminLayout>

        <div className={styles.content__header}>
            <div className={styles.content__header__title}>
                <h1>Plans</h1>
                <span> WELCOME BACK, MARYAM JAFARPOUR </span>
            </div>

            <div className={styles.content__header__icons}>
                <div className={styles.content__header__icons__date}>
                    <span> SUNDAY, NOVEMBER 26 </span>
                </div>
                <div className={styles.content__header__icon}>
                            <span className={styles.icon}>

                            </span>

                    <span className={styles.icon}>

                </span>
                </div>
            </div>
        </div>

        <PlanArchive/>

    </AdminLayout>

}