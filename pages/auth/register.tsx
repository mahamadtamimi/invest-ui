import Register from "@/components/auth/Register";
import AuthLayout from "@/layouts/auth";
import styles from "@/styles/auth.module.scss";
import Image from "next/image";
import logo from "@/public/log.svg";
import banner from '@/public/login-banner.png'

export default function register() {
    return <div className={styles.signin}>
        <div className={styles.signin__content}>

            <div className={styles.signin__content__left}>
                <div className={styles.signin__content__left__title}>
                              <span>

                                  <Image src={logo} alt=''/>
                              </span>

                </div>
                <div className={styles.signin__content__left__content}>
                    <div className={styles.signin__content__left__content__title}>
                        <h1>REGISTRATION </h1>

                    </div>
                    <Register/>

                </div>
            </div>

            <div className={styles.signin__content__rigth}>
                <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
            </div>
        </div>
    </div>

}