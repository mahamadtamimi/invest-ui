import {toast, ToastContainer} from "react-toastify";
import styles from "@/styles/auth.module.scss";
import {Button, code, Input, Link} from "@nextui-org/react";
import {BackIcon} from "@/components/BackIcon";
import Image from "next/image";
import logo from "@/public/log.svg";
import banner from "@/public/login-banner.png";
import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import validator from "validator";
import {EyeFilledIcon, EyeSlashFilledIcon} from "@nextui-org/shared-icons";
import PasswordStrengthBar from "react-password-strength-bar";
import {useDispatch} from "react-redux";

export default function ChangePassword() {

    const router = useRouter();
    const dispatch = useDispatch()

    const [isVisible, setIsVisible] = useState(false);
    const [isVisibleRePassword, setIsVisibleRePassword] = useState(false);

    const toggleVisibility = () => setIsVisible(!isVisible);
    const toggleVisibilityREenter = () => setIsVisibleRePassword(!isVisibleRePassword);

    const [timer, setTimer] = useState({
        use: true,
        timer: 60,
    })

    const [fromData, setFromData] = useState({
        code: {
            value: '',
            validate: false,
            error: ''
        },
        password: {
            value: '',
            validate: false,
            error: ''
        },
        rePassword: {
            value: '',
            validate: false,
            error: ''
        }
    })


    function checkForm(e: any) {
        switch (e.target.getAttribute("data-name")) {
            case 'code' :
                const code = e.target.value

                const codeValidate = code.length == 5
                if (code.length <= 5) {
                    setFromData({
                        ...fromData,
                        code: {
                            ...fromData.code,
                            value: code,
                            validate: codeValidate
                        }

                    })

                }
                break;
            case 'password' :
                const password = e.target.value
                const passwordValid = password.length > 2;

                setFromData({
                    ...fromData,
                    password: {
                        ...fromData.password,
                        value: e.target.value,
                    }
                })
                break;
            case 'rePassword' :
                const rePassword = e.target.value
                const rePasswordValid = rePassword === fromData.password.value;

                setFromData({
                    ...fromData,
                    rePassword: {
                        ...fromData.rePassword,
                        value: e.target.value,
                        validate: rePasswordValid,

                    }
                })
        }


    }


    function sendCode() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const userData = {
            'mail': router.query.email,

        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/auth/forgot-password/new-code`, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            // @ts-ignore
            body: formBody

        }).then(res => res.json()).then((data) => {
            if (data.success) {
                toast.success('Email sent successfully.');
                setTimer({
                    ...timer,
                    use: true,
                    timer: 60
                })
            }

        })

        console.log(router.query.email)
    }


    function changePassword() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const userData = {
            'mail': router.query.email,
            'code': fromData.code.value,
            'password': fromData.password.value,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/auth/change-password`, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            // @ts-ignore
            body: formBody

        }).then(res => res.json()).then((data) => {

            if (data.success) {



                dispatch({type: 'LOGIN', payload: {user: data.user}});
                router.push('/auth/dashboard')



            }
            if (!data.success) {
                toast.dismiss('registerToastId')
                toast.error('Invalid Code!');
            }

        })

    }


    useEffect(() => {
        if (timer.timer === 0) {
            setTimer({
                ...timer,
                use: false,
                timer: 60
            })

        }

        if (timer.use) {
            const interval = setInterval(() => {
                setTimer({
                    ...timer,
                    timer: timer.timer - 1
                })
            }, 1000);
            return () => clearInterval(interval);

        }


    }, [timer, setTimer]);

    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>


        <div className={styles.signin}>
            <div className={styles.signin__content}>
                <span className={styles.back_link}><Link href={'/'}> <BackIcon/>  <span>back</span> </Link></span>

                <div className={styles.signin__content__left}>
                    <div className={styles.signin__content__left__title}>
                    <span>

                        <Image src={logo} alt={''}/>
                    </span>

                    </div>
                    <div className={styles.signin__content__left__content}>
                        <div className={styles.signin__content__left__content__title}>
                            <h1>Forgot you password ?</h1>

                        </div>
                        <div className={styles.signin__content__left__form}>
                            <form className={styles.form_group}>
                                <div className={styles.input_group}>
                                    <Input
                                        name={'code'}
                                        value={fromData.code.value}
                                        variant="bordered"
                                        onChange={(e) => checkForm(e)}
                                        isInvalid={!fromData.code.validate}
                                        color={!fromData.code.validate ? "danger" : "success"}
                                        errorMessage={!fromData.code.validate && fromData.code.error}
                                        type="text"
                                        data-name={'code'}
                                        label="Enter code"

                                        labelPlacement='inside'
                                    />
                                </div>
                                <div className={styles.form_input_section_password}>
                                    <Input
                                        name={'password'}
                                        value={fromData.password.value}
                                        variant="bordered"
                                        // isInvalid={fromData.password.validate}
                                        color={!fromData.password.validate ? "danger" : "success"}

                                        onChange={(e) => checkForm(e)}

                                        data-name={'password'}
                                        label="Password"
                                        errorMessage={fromData.password.error}
                                        labelPlacement='inside'


                                        endContent={
                                            <button className="focus:outline-none" type="button"
                                                    onClick={toggleVisibility}>
                                                {isVisible ? (
                                                    <EyeSlashFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                ) : (
                                                    <EyeFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                )}
                                            </button>
                                        }
                                        type={isVisible ? "text" : "password"}

                                    />
                                    <div className={styles.form_input_section_validator}>
                                        <PasswordStrengthBar
                                            barColors={['#ddd', '#ef4836', '#f6b44d', '#2bef79', '#25c281']}
                                            password={fromData.password.value} onChangeScore={(score, feedback) => {
                                            if (score < 3) {
                                                const data = {
                                                    ...fromData,
                                                    password: {
                                                        ...fromData.password,
                                                        value: fromData.password.value,
                                                        validate: false,
                                                        error: feedback.warning
                                                    }

                                                }

                                                // @ts-ignore
                                                setFromData(data)
                                            }
                                            if (score >= 3) {
                                                const data = {
                                                    ...fromData,
                                                    password: {
                                                        ...fromData.password,
                                                        value: fromData.password.value,
                                                        validate: true,
                                                        error: ''
                                                    }

                                                }

                                                setFromData(data)
                                            }


                                        }}/>
                                    </div>

                                </div>
                                <div className={styles.form_input_section}>
                                    <Input
                                        name={'rePassword'}
                                        value={fromData.rePassword.value}
                                        variant="bordered"
                                        // isInvalid={formData.firstName.validate}
                                        color={!fromData.rePassword.validate ? "danger" : "success"}

                                        onChange={(e) => checkForm(e)}

                                        data-name={'rePassword'}
                                        label="Re Enter Password"
                                        errorMessage={!fromData.rePassword.validate && fromData.rePassword.error}
                                        labelPlacement='inside'


                                        endContent={
                                            <button className="focus:outline-none" type="button"
                                                    onClick={toggleVisibilityREenter}>
                                                {isVisibleRePassword ? (
                                                    <EyeSlashFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                ) : (
                                                    <EyeFilledIcon
                                                        className="text-2xl text-default-400 pointer-events-none"/>
                                                )}
                                            </button>
                                        }
                                        type={isVisibleRePassword ? "text" : "password"}

                                    />


                                </div>

                                <div className={`${styles.input_grou} mt-2`}>

                                    <Button
                                        isDisabled={!(fromData.rePassword.validate && fromData.password.validate && fromData.code.validate)}
                                        className={'btn-one'}
                                            onClick={() => changePassword()}
                                    >
                                        Change Password
                                    </Button>


                                </div>
                                <div className={`${styles.input_grou} mt-2`}>
                                    {timer.use ?
                                        <Button disabled={true} variant={'bordered'} className={'btn-two'}
                                                onClick={() => sendCode()}>

                                            Resubmit the code : {timer.timer} sec
                                        </Button>
                                        :
                                        <Button className={'btn-one'} onClick={() => sendCode()}>
                                            Get New Code
                                        </Button>
                                    }


                                </div>

                            </form>


                        </div>

                    </div>
                </div>

                <div className={styles.signin__content__rigth}>
                    <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
                </div>
            </div>
        </div>
    </>

}