import {toast, ToastContainer} from "react-toastify";
import styles from "@/styles/auth.module.scss";
import {Button, Input, Link} from "@nextui-org/react";
import {BackIcon} from "@/components/BackIcon";
import Image from "next/image";
import logo from "@/public/log.svg";

import banner from "@/public/login-banner.png";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";


export default function GoogleAuthToken() {
    const router = useRouter();
    const dispatch = useDispatch()




    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)
    // @ts-ignore
    const lock = useSelector((state) => state.lock)
    console.log(lock)
    console.log(user)

    const [formData, setFormData] = useState({
        code: {
            value: '',
            validate: false
        }

    })


    function updateFormData(e: any) {
        const code = e.target.value
        const codeValidate = code.length == 6
        if (code.length <= 6) {
            setFormData({
                ...formData,
                code: {
                    ...formData.code,
                    value: code,
                    validate: codeValidate
                }

            })

        }

    }


    function verifyCode() {

        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const requestUrl = `${process.env.API_PATH}/api/v1/auth/verify-google-code`


        const userData = {
            'code': formData.code.value,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(requestUrl, {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded'
            },
            // @ts-ignore
            body: formBody

        }).then(res => res.json()).then(data => {
            toast.dismiss('registerToastId')

            console.log(data)
            if (data.success) {
                dispatch({type: 'UN_LOCK', payload: false});
                return router.push('/auth/dashboard');
                // console.log(data)
            } else {

                toast.error('invalid code!')
            }

        })

    }


    // const searchParams = useSearchParams()


    return <>
        <ToastContainer

            position="top-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="dark"

        />
        <div className={styles.signin}>
            <div className={styles.signin__content}>
                <span className={styles.back_link}><Link href={'/'}> <BackIcon/>  <span>back</span> </Link></span>

                <div className={styles.signin__content__left}>
                    <div className={styles.signin__content__left__title}>
                    <span>

                        <Image src={logo} alt={''}/>
                    </span>

                    </div>
                    <div className={styles.signin__content__left__content}>
                        <div className={styles.signin__content__left__content__title}>
                            <h1>Google Two Step Verification</h1>
                            <p>You Are Not {user?.email} ? <Link href={'/auth/dashboard/sign-out'}
                                                                 className={'tx-red'}>Click Here</Link></p>
                        </div>


                        <div className={styles.signin__content__left__form}>
                            <form className={styles.form_group}>
                                <div className={styles.input_group}>
                                    <Input
                                        name={'code'}
                                        value={formData.code.value}
                                        variant="bordered"
                                        onChange={(e) => updateFormData(e)}
                                        isInvalid={!formData.code.validate}
                                        color={!formData.code.validate ? "danger" : "success"}
                                        errorMessage={!formData.code.validate && 'Enter the six-digit code'}
                                        type="text"
                                        data-role={'code'}
                                        label="Code"

                                        labelPlacement='inside'
                                    />
                                </div>


                                <div className={styles.input_grou}>
                                    <Button isDisabled={!formData.code.validate} disabled={!formData.code.validate}
                                            className={`btn-one ${!formData.code.validate && `disabled-btn`}`}
                                            onClick={() => verifyCode()}>
                                        Submit
                                    </Button>
                                </div>


                            </form>


                        </div>

                    </div>
                </div>

                <div className={styles.signin__content__rigth}>
                    <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
                </div>
            </div>
        </div>
    </>
}