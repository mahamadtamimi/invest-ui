import DashboardLayout from "@/layouts/dashboard";
import NewTicketPage from "@/components/Dashboard/TicketList/NewTicketPage";

export default function newTicket(){

    return <DashboardLayout>

        <NewTicketPage />
    </DashboardLayout>

}