import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import styles from "@/styles/dashboard.module.scss";
import moment from "moment/moment";
import Image from "next/image";
import testMony from "@/public/testMony.svg";
import {Button, Input, Link, Snippet} from "@nextui-org/react";
import UserTransaction from "@/components/Dashboard/Dashboard/UserTransaction";
import React, {useEffect, useState} from "react";
import DashboardLayout from "@/layouts/dashboard";
import {useDispatch, useSelector} from "react-redux";
import dashboardStyles from "@/styles/dashboard.module.scss";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {useRouter} from "next/router";


export default function generalInfo() {
    // @ts-ignore
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector((state) => state.user);

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const dispatch = useDispatch()

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [fixData, setFixData] = useState(false)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/wallet/get_wallet_address`,
            {
                method: "GET",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
            })
            .then((res) => res.json())
            .then((data) => {


                if (data.walletAddress !== null) {
                    setFormData({
                        ...formData,
                        walletAddress: {
                            ...formData.walletAddress,
                            value: data.walletAddress
                        }


                    })
                    setFixData(true)


                }

                toast.dismiss('registerToastId')


            })


    }, []);


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState({

        firstName: {
            value: user.firstName,
            validate: true
        },
        lastName: {
            value: user.lastName,
            validate: true,
        },
        email: {
            value: user.email,
            validate: true
        },
        walletAddress: {
            value: ''
        }

    })


    function updateWalletAddress() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const form = {
            // @ts-ignore
            'walletAddress': formData.walletAddress.value,

        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/wallet/update-wallet-address`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => res.json())
            .then((data) => {

                router.reload()

                dispatch({type: 'ACTIVE_ACCOUNT', payload: data});
                setFixData(true)
                toast.dismiss('registerToastId')

            })


    }

    function updateFormData(e: any) {
        setFormData({...formData, walletAddress: {...formData.walletAddress, value: e.target.value}})

    }


    return <DashboardLayout>

        <DashboardHead title={'General Info'}/>

        <div className={dashboardStyles.widrawal_main_sec}>


            <div className={dashboardStyles.widrawal_main_sec_grid}>
                <div className={dashboardStyles.widrawal_form}>


                    <form action="">
                        <div className={styles.general_info_sec}>
                            <div className={styles.general_info_input_sec}>
                                <Input
                                    readOnly={true}
                                    type="text"
                                    labelPlacement={'outside'}
                                    label={'First Name'}
                                    value={user.firstName}/>
                            </div>
                            <div className={styles.general_info_input_sec}>
                                <Input readOnly={true}
                                       type="text"
                                       labelPlacement={'outside'}
                                       label={'Last Name'} value={user.lastName}/>
                            </div>
                            <div className={styles.general_info_input_sec}>
                                <Input readOnly={true}
                                       type="text"
                                       labelPlacement={'outside'}
                                       label={'Email'} value={user.email}/>
                            </div>
                            <div className={styles.general_info_input_sec}>
                                <Input
                                    onChange={(e) => updateFormData(e)}
                                    color={formData.walletAddress.value.length < 1 ? 'danger' : 'success'}
                                    variant={'bordered'}
                                    readOnly={fixData}
                                    type="text"
                                    labelPlacement={'outside'}
                                    label={'Wallet'}
                                    errorMessage={'Warning: Be careful when entering the wallet address! Your wallet address cannot be re-edited , Just enter the address of TRC20 or Bep20 wallet'}
                                    value={formData.walletAddress.value}
                                />
                            </div>
                            <div className={styles.general_info_input_btn}>
                                {!fixData &&

                                    <Button
                                        onClick={updateWalletAddress}
                                        className={styles.updateGeneralInfo}
                                        isDisabled={formData.walletAddress.value.length < 1}
                                        color={'primary'}>
                                        save
                                    </Button>


                                }
                            </div>
                        </div>

                    </form>


                </div>

                <div className={dashboardStyles.widrawal_description}>
                    <h2 className={dashboardStyles.widrawal_description_head}> Terms & Conditions </h2>
                    <div className={dashboardStyles.widrawal_description_text}>
                        <p>
                            - Each user is only allowed to register their digital wallet address once
                            in the user profile section.
                        </p>
                        <p>
                            - The wallet address registered by the user is non-editable, and the
                            user cannot modify or replace it later.
                        </p>
                        <p>
                            - Users must register a valid and active wallet address. The site is not
                            responsible for registering an invalid or inactive address.
                        </p>
                        <p>
                            - After registering the wallet address, all profit and capital withdrawals
                            will solely be made to that address.
                        </p>
                        <p>
                            - In case of loss or theft of the wallet, the site is not responsible for
                            returning or compensating the funds sent to the registered address.
                        </p>
                        <p>
                            - Users must ensure the correctness and security of the wallet
                            address before registering it.
                        </p>
                        <p>
                            - The site reserves the right to withhold fund transfers to the
                            registered address if any suspicious or illegal activity is observed.
                        </p>
                        <p>
                            - Dear user, please carefully read and accept these terms and
                            conditions, then register your valid and active digital wallet address in
                            the respective section.
                        </p>

                    </div>
                </div>

            </div>


        </div>


    </DashboardLayout>


}