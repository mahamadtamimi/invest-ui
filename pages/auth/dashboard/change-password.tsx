import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import styles from "@/styles/dashboard.module.scss";
import moment from "moment/moment";
import Image from "next/image";
import testMony from "@/public/testMony.svg";
import {Button, Input, Link, Snippet} from "@nextui-org/react";
import UserTransaction from "@/components/Dashboard/Dashboard/UserTransaction";
import React, {useEffect, useState} from "react";
import DashboardLayout from "@/layouts/dashboard";
import {useDispatch, useSelector} from "react-redux";
import dashboardStyles from "@/styles/dashboard.module.scss";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {EyeFilledIcon, EyeSlashFilledIcon} from "@nextui-org/shared-icons";
import PasswordStrengthBar from "react-password-strength-bar";
import {initialData} from "@/components/auth/initialData";
import checkRegisterFormData from "@/components/auth/checkFormData";
import {useRouter} from "next/router";


export default function generalInfo() {
    // @ts-ignore
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector((state) => state.user);

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const dispatch = useDispatch()

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [inLoad, setInLoad] = useState(false)


    // eslint-disable-next-line react-hooks/rules-of-hooks


    const init: any = initialData

    const toggleVisibility = () => setIsVisible(!isVisible);
    const toggleVisibilityREenter = () => setIsVisibleRePassword(!isVisibleRePassword);

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [isVisible, setIsVisible] = useState(false);


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [isVisibleRePassword, setIsVisibleRePassword] = useState(false);


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(init)

    function checkForm(e: any) {
        const data = checkRegisterFormData(e, formData)


        setFormData(data)
    }


    function handleSubmit() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const form = {
            // @ts-ignore
            'password': formData.password.value,

        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/auth/change-user-password`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => res.json())
            .then((data) => {
                toast.dismiss('registerToastId')
                if (data.success){
                    router.push("/auth/dashboard")
                }else {
                    toast.error('An error has occurred !')
                }

                // dispatch({type: 'ACTIVE_ACCOUNT', payload: data});
                // setFixData(true)


            })

    }


    return <DashboardLayout>
        <ToastContainer

            position="top-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="dark"

        />

        <DashboardHead title={'Change Password'}/>

        <div className={dashboardStyles.chenge_password_section}>


            <div className={dashboardStyles.widrawal_main_sec_grid}>
                <div className={dashboardStyles.chanege_password_form}>


                    <div className={styles.form_input_section_password}>
                        <Input
                            name={'password'}
                            value={formData.password.value}
                            variant="bordered"
                            // isInvalid={formData.firstName.validate}
                            color={!formData.password.validate ? "danger" : "success"}
                            labelPlacement={'outside'}
                            onChange={(e) => checkForm(e)}
                            data-name={'password'}
                            label="Password"
                            errorMessage={formData.password.error}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisible ? "text" : "password"}

                        />
                        <div className={styles.form_input_section_validator}>
                            <PasswordStrengthBar
                                barColors={['#ddd', '#ef4836', '#f6b44d', '#2bef79', '#25c281']}
                                password={formData.password.value} onChangeScore={(score, feedback) => {
                                if (score < 3) {
                                    const data = {
                                        ...formData,
                                        password: {
                                            ...formData.password,
                                            value: formData.password.value,
                                            validate: false,
                                            error: feedback.warning
                                        }

                                    }

                                    setFormData(data)
                                }
                                if (score >= 3) {
                                    const data = {
                                        ...formData,
                                        password: {
                                            ...formData.password,
                                            value: formData.password.value,
                                            validate: true,
                                            error: ''
                                        }

                                    }

                                    setFormData(data)
                                }


                            }}/>
                        </div>

                    </div>
                    <div className={styles.form_input_section}>
                        <Input
                            name={'rePassword'}
                            value={formData.rePassword.value}
                            variant="bordered"
                            // isInvalid={formData.firstName.validate}
                            color={!formData.rePassword.validate ? "danger" : "success"}
                            labelPlacement={'outside'}
                            onChange={(e) => checkForm(e)}

                            data-name={'rePassword'}
                            label="Re Enter Password"
                            errorMessage={!formData.rePassword.validate && formData.rePassword.error}


                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibilityREenter}>
                                    {isVisibleRePassword ? (
                                        <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisibleRePassword ? "text" : "password"}

                        />


                    </div>
                    <div className={styles.general_info_input_btn}>

                        <Button

                            className={`${styles.updateGeneralInfo}
                                 mt-4
                                  ${!(formData.password.validate &&
                                formData.rePassword.validate) ?
                                'disabled-btn' : ''}
                                 `}

                            color={'primary'}

                            isLoading={inLoad}
                            onClick={() => handleSubmit()}
                            isDisabled={!(
                                formData.password.validate &&
                                formData.rePassword.validate)}


                        >
                            save
                        </Button>


                    </div>


                </div>


            </div>


        </div>


    </DashboardLayout>


}