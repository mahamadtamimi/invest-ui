import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";

export default function signOut() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const dispatch = useDispatch()
    // @ts-ignore
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector((state) => state.user);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();


    dispatch({type: 'LOGIN', payload: {user: null}});
    dispatch({type: 'UN_LOCK', payload: true});
    router.push('/auth/login')
    // return null


}