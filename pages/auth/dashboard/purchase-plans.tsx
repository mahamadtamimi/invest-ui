import dashboardLayout from "@/layouts/dashboard";
import DashboardLayout from "@/layouts/dashboard";
import Plans from "@/components/Home/Plans/Plans";
import PurchasePlan from "@/components/Dashboard/ParchasePlan/PurchasePlan";

export default function purchasePlans() {
   return <DashboardLayout>

       <PurchasePlan/>
   </DashboardLayout>
}