import DashboardLayout from "@/layouts/dashboard";
import TankYou from "@/components/Dashboard/ParchasePlan/TankYou";
import Withdrawal from "@/components/Dashboard/Withdrawal/Withdrawal";


export default function withdrawal() {
    return <DashboardLayout>

        <Withdrawal/>
    </DashboardLayout>
}