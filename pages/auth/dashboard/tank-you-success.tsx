import DashboardLayout from "@/layouts/dashboard";

import TankYouSuccess from "@/components/Dashboard/ParchasePlan/TankYouSuccess";


export default function purchasePlansSuccess() {
    return <DashboardLayout>

        <TankYouSuccess/>
    </DashboardLayout>
}