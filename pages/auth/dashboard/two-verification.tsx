import DashboardLayout from "@/layouts/dashboard";
import {toast, ToastContainer} from "react-toastify";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import dashboardStyles from "@/styles/dashboard.module.scss";

import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Image} from "@nextui-org/image";
import {Button, Checkbox, CheckboxGroup, Input, Snippet} from "@nextui-org/react";
import {EyeFilledIcon, EyeSlashFilledIcon} from "@nextui-org/shared-icons";
import {useRouter} from "next/router";


export default function TwoVerification() {
    // @ts-ignore
    const user = useSelector((state) => state.user);


    const dispatch = useDispatch();

    const route = useRouter();
    const [data, setData] = useState()
    const [code, setCode] = useState()
    const [tab, setTab] = useState(1)
    const [formData, setFormData] = useState({
        password: {
            value: '',
            validate: false,
            error: ''
        },
        code: {
            value: '',
            validate: false,
            error: ''
        }
    })
    const [isVisibleRePassword, setIsVisibleRePassword] = useState(false);
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1//auth/two-verification`,
            {
                method: "GET",
                headers: {

                    Authorization: `bearer ${user?.token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                if (data.success) {
                    setData(data.data)
                    setCode(data.base32)
                }

                console.log(data)
            })
    }, []);

    const toggleVisibilityREenter = () => setIsVisibleRePassword(!isVisibleRePassword);

    function checkForm(e: any) {
        switch (e.target.getAttribute("data-role")) {
            case 'password' :
                const passwordValidate = e.target.value.length > 0;
                setFormData({
                    ...formData,
                    password: {
                        ...formData.password,
                        value: e.target.value,
                        validate: passwordValidate
                    }
                })
                break;
            case 'code' : {
                if (e.target.value.length > 6) return
                const codeValidate = e.target.value.length === 6;
                setFormData({
                    ...formData,
                    code: {
                        ...formData.code,
                        value: e.target.value,
                        validate: codeValidate
                    }
                })
                break;
            }
        }
    }


    function handleSubmit(e: any) {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const form = {
            // @ts-ignore
            'password': formData.password.value,
            'code': formData.code.value,
            'secret': code
        }


        let formBody = [];
        for (var property in form) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(form[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/auth/two-verification-verify`,
            {
                method: "POST",
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    Authorization: `bearer ${user.token}`
                },
                // @ts-ignore
                body: formBody,
            })
            .then((res) => res.json())
            .then((data) => {
                toast.dismiss('registerToastId')
                if (data.success) {

                    /////////////
                    //
                    //
                    dispatch({type: 'UN_LOCK', payload: false});
                    route.push("/auth/dashboard")
                } else {
                    toast.error('Invalid Code Or Password !')
                    setFormData({
                        ...formData,
                        password: {
                            ...formData.password,
                            value: '',
                            validate: false,
                        },
                        code: {
                            ...formData.code,
                            value: '',
                            validate: false
                        }
                    })
                }
                console.log(data)
                // dispatch({type: 'ACTIVE_ACCOUNT', payload: data});
                // setFixData(true)


            })
    }

    return <DashboardLayout>

        <DashboardHead title={'Two Verification'}/>

        <div className={dashboardStyles.chenge_password_section}>


            <div className={dashboardStyles.widrawal_main_sec_grid}>
                <div className={dashboardStyles.authticator}>

                    {tab === 1 &&
                        <div className={dashboardStyles.qr}>
                            <div className={dashboardStyles.seprator}>
                                <p className={dashboardStyles.seprator_pragh}>
                                    scan QR
                                </p>
                                <span className={dashboardStyles.seprator_pragh_seprator}>

                            </span>
                            </div>
                            <div className={dashboardStyles.qr_section}>
                                <Image className={dashboardStyles.qr_image} src={data} alt={''}/>

                            </div>
                            <div className={dashboardStyles.seprator}>
                                <p className={dashboardStyles.seprator_pragh}>
                                    Or Copy Secret
                                </p>
                                <span className={dashboardStyles.seprator_pragh_seprator}>

                            </span>
                            </div>
                            <div className={dashboardStyles.secrect}>
                                <Snippet variant="bordered" symbol="">{code}</Snippet>
                            </div>


                        </div>
                    }
                    {tab === 2 &&
                        <div className={dashboardStyles.qr}>

                            <div className={'mt-4'}>
                                <Input type="number"
                                       labelPlacement={'outside'}
                                       label={'enter code'}
                                       placeholder={'Enter six digit code'}
                                       data-role={'code'}
                                       onChange={(e) => checkForm(e)}
                                       color={!formData.code.validate ? "danger" : "success"}
                                       value={formData.code.value}
                                />

                            </div>
                            <div className={'mt-4'}>
                                <Input
                                    name={'password'}
                                    data-role={'password'}
                                    value={formData.password.value}
                                    variant="bordered"
                                    // isInvalid={formData.firstName.validate}
                                    color={!formData.password.validate ? "danger" : "success"}
                                    labelPlacement={'outside'}
                                    onChange={(e) => checkForm(e)}
                                    placeholder={'enter current password'}
                                    label="Re Enter Password"
                                    errorMessage={!formData.password.validate && formData.password.error}


                                    endContent={
                                        <button className="focus:outline-none"

                                                type="button" onClick={toggleVisibilityREenter}>
                                            {isVisibleRePassword ? (
                                                <EyeSlashFilledIcon
                                                    className="text-2xl text-default-400 pointer-events-none"/>
                                            ) : (
                                                <EyeFilledIcon
                                                    className="text-2xl text-default-400 pointer-events-none"/>
                                            )}
                                        </button>
                                    }
                                    type={isVisibleRePassword ? "text" : "password"}

                                />
                            </div>


                        </div>
                    }


                    {
                        tab === 1 &&
                        <div className={dashboardStyles.btns_sec_tab1}>
                            <Button color={'primary'} className={'text-amber-50'} onClick={() => setTab(2)}>
                                Next
                            </Button>
                        </div>
                    }


                    {
                        tab === 2 &&
                        <div className={dashboardStyles.btns_sec}>
                            <Button color={'primary'} className={'text-amber-50'} onClick={() => setTab(1)}>
                                Pre
                            </Button>

                            <Button color={'primary'}
                                    className={`  ${!(formData.password.validate &&
                                        formData.code.validate) ?
                                        'disabled-btn' : ''} 
                                        text-amber-50`}
                                    isDisabled={!(
                                        formData.password.validate &&
                                        formData.code.validate)}

                                    onClick={(e) => handleSubmit(e)}>
                                Submit And Active Two Factor
                            </Button>
                        </div>
                    }
                </div>


            </div>


        </div>


    </DashboardLayout>
}