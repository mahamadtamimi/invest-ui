import DashboardLayout from "@/layouts/dashboard";
import {useRouter} from "next/router";
import DashboardHead from "@/components/Dashboard/Sidebar/DashboardHead";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {Button, Link, user} from "@nextui-org/react";
import {useSelector} from "react-redux";
import style from '@/styles/chat.module.scss'
import {PlusIcon} from "@/components/Dashboard/TicketList/PlusIcon";
import {MassgeIcon} from "@/components/Dashboard/TicketList/MassgeIcon";
import ChatBody from "@/components/Chat/ChatBody";
import {da} from "@faker-js/faker";
import item from "react-password-strength-bar/lib/Item";


export default function Ticket({id}: { id: string }) {
    const router = useRouter();
    const [data, setData] = useState()
    const [pending, setPending] = useState(true)
    const [itemSelected, setItemSelected] = useState(0)

    const [chatController, setChatController] = useState({
        value: '',
        validate: false
    })
    // @ts-ignore
    const user = useSelector((state) => state.user);

    useEffect(() => {


        fetch(`${process.env.API_PATH}/api/v1/user/tickets`, {
            headers: {
                Authorization: `bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setData(data)
                setPending(false)
            })


    }, [])


    function ShowMassage() {
        return <div className={style.chat_main__all_section}>
            {!pending && <>
                {
                    // @ts-ignore
                    data.length &&
                    // @ts-ignore
                    data[itemSelected].TicketBodies.map((ticket) => (

                        <>
                            {ticket.side == 0 ? <div
                                    key={ticket.id}
                                    className={style.chat_main_user_sec}>
                                    <span className={style.chat_main_user}>
                                        {ticket.text}


                                        <div className={style.chat_main_user_data}>
                                            <span> 27/10/1373</span>
                                            <span>16:30</span>
                                        </div>

                                    </span>


                                </div> :
                                <div className={style.chat_main_admin_sec}>
                                      <span className={style.chat_main_admin}>hello

                                        <div className={style.chat_main_user_data}>
                                                <span> 27/10/1373</span>
                                                <span>16:30</span>
                                            </div>

                                        </span>

                                </div>}
                        </>
                    ))
                } </>}

        </div>
    }

    function checkMassage(e: any) {
        const massage = e.target.value


        setChatController({
            ...chatController,
            value: massage,
            validate: massage.length > 0
        })

    }

    function NotFound() {
        return <div className={style.not__found}>
            <p>
                No ticket has been created yet !
            </p>


            <Button color={'primary'}
                    endContent={<PlusIcon/>}
                    className={'text-white mt-4 mb-2'} as={Link} href={'/auth/dashboard/new-ticket'}>
                New Ticket
            </Button>
        </div>;
    }


    function SideItem() {
        return <ul className={style.subject__list_ul}>

            {
                // @ts-ignore
                data.map((item, index) => {
                    console.log(item)
                    const newMassage = item.TicketBodies.filter((item: any) => {
                        return (item.side == 1 && item.seen == false)
                    }).length

                    return <li key={`sid-${item.id}`}
                               onClick={() => setItemSelected(index)}
                               className={`${style.subject__list_li} ${itemSelected === index && style.item_chat_selected} ${item.status && style.item_chat_archived}`}>

                        {!item.status && <MassgeIcon/>}
                        <span>
                            {item.subject}

                            {newMassage ? <span className={style.have_new_massage}>{newMassage}</span> : ''}
                            {item.status && <span className={style.archived}>archive</span>}
                        </span>

                    </li>
                })}
        </ul>
    }


    function MassageSideBar() {
        return !pending ? <>

            <div>
                <Button color={'primary'}
                        radius={'none'}
                        endContent={<PlusIcon/>}
                        className={'text-white w-full h-14'} as={Link} href={'/auth/dashboard/new-ticket'}>
                    New Ticket
                </Button>
            </div>

            {
                // @ts-ignore
                data.length ?
                    <SideItem/>
                    :
                    <NotFound/>}
        </> : <p>loading</p>


    }

    function ChatHeader() {

        return <div className={style.chat_main_header}>
            {!pending && <>
                {// @ts-ignore
                    data.length ?
                        <>
                            <h3 className={style.subject_name}>
                                {// @ts-ignore
                                    data[itemSelected]?.subject}
                            </h3>


                            <span className={style.subject_description}>
                        <Button disabled={true} isDisabled={true}>
                                {// @ts-ignore
                                    data[itemSelected].status ? 'close' : 'pending'}

                        </Button>
                         <p>
                                  {// @ts-ignore
                                      data[itemSelected].status ? '' :
                                      <>
                                          Did you get your answer?<Button onClick={(e) => closeTicket(e)}
                                                                          className={style.link_new}
                                                                          as={Link}>
                                          close ticket </Button>

                                      </>

                                  }

                        </p>


                    </span>

                        </> :
                        ''
                } </>}


        </div>
    }

    function sendMassage(e: any) {
        e.preventDefault()
        const userData = {
            // @ts-ignore
            ticketId: data[itemSelected].id,
            text: chatController.value,
            side: 0
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(`${process.env.API_PATH}/api/v1/admin/tickets/answer`, {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded',
            },
            // @ts-ignore
            body: formBody
        }).then(res => res.json()).then((data) => {
            setData(data)
            setChatController({
                ...chatController,
                value: '',
                validate: false
            })
        })
    }


    const scrollToBottom = () => {
        window.document.getElementById('massageEmail')?.scrollTo({
            top: window.document.getElementById('massageEmail')?.scrollHeight,
            behavior: 'smooth',
        });
    };

    useEffect(() => {
        scrollToBottom()
    }, [data]);


    useEffect(() => {


        const timeoutId = setTimeout(() => {
            fetch(`${process.env.API_PATH}/api/v1/user/tickets`, {
                headers: {
                    Authorization: `bearer ${user.token}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    setData(data)
                    setPending(false)

                })

        }, 60000);

        return () => clearTimeout(timeoutId);


    }, [data])

    function closeTicket(e: any) {

        const userData = {
            // @ts-ignore
            ticketId: data[itemSelected].id,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");

        fetch(`${process.env.API_PATH}/api/v1/close-tickets`, {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded',
            },
            // @ts-ignore
            body: formBody
        }).then(res => res.json()).then((data) => {
            setData(data)
        })
    }

    // @ts-ignore
    return <DashboardLayout>
        <DashboardHead title={'Tickets'}/>


        <div className={style.chat_main_section}>
            <div className={style.subject__list__sec}>
                <MassageSideBar/>

            </div>
            <div className={style.chat_main__sec}>
                <ChatHeader/>


                {!pending && <ChatBody chats={
                    // @ts-ignore
                    data[itemSelected]}/>}


                <div className={style.chat_main__all_footer__sec}>
                    {
                        !pending &&
                        <form className={style.chat__form}
                              action=""
                              onSubmit={(e) => sendMassage(e)}>

                            <input type={'text'} disabled={
                                // @ts-ignore
                                data.length == 0 || data[itemSelected].status}
                                   className={`${style.chat_main__input__main} ${
                                       // @ts-ignore
                                       data.length == 0 && style.btn__disable}
                                               
                                       ${
                                       // @ts-ignore
                                       pending && data[itemSelected].status  && style.btn__disable}
                                       `}
                                   placeholder={'enter massage'}
                                   value={chatController.value}
                                   onChange={(e) => checkMassage(e)}
                            />
                            <Button color={'primary'}
                                    type={'submit'}
                                    isDisabled={
                                        // @ts-ignore
                                        data.length == 0 && !chatController.validate}
                                    className={`text-white ${
                                        // @ts-ignore
                                        data.length == 0 && style.btn__disable} ${!chatController.validate && style.btn__disable}`

                            }>

                                send
                            </Button></form>

                    }


                </div>

            </div>

            <br/><br/><br/><br/><br/>
        </div>

    </DashboardLayout>
}