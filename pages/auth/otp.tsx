import {toast, ToastContainer} from "react-toastify";
import styles from "@/styles/auth.module.scss";
import {Button, Input, Link} from "@nextui-org/react";
import {BackIcon} from "@/components/BackIcon";
import Image from "next/image";
import logo from "@/public/log.svg";

import banner from "@/public/login-banner.png";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";


export default function Otp() {
    const router = useRouter();
    const dispatch = useDispatch()

    const [timer, setTimer] = useState({
        use: true,
        timer: 60
    })

    // const [pending, setPending] = useState(false)
    // const [useTimer, setUseTimer] = useState(false)
    //
    // const [timer, setTimer] = useState(60)
    // const [success, setSuccess] = useState(false)


    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)


    const [formData, setFormData] = useState({
        code: {
            value: '',
            validate: false
        }

    })

    useEffect(() => {
        if (timer.timer === 0) {
            setTimer({
                ...timer,
                use: false,
                timer: 60
            })

        }

        if (timer.use) {
            const interval = setInterval(() => {
                setTimer({
                    ...timer,
                    timer: timer.timer - 1
                })
            }, 1000);
            return () => clearInterval(interval);

        }


    }, [timer, setTimer]);


    function updateFormData(e: any) {
        const code = e.target.value
        const codeValidate = code.length == 5
        if (code.length <= 5) {
            setFormData({
                ...formData,
                code: {
                    ...formData.code,
                    value: code,
                    validate: codeValidate
                }

            })

        }

    }


    function verifyCode() {

        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const requestUrl = `${process.env.API_PATH}/api/v1/auth/verify-code`


        const userData = {
            'code': formData.code.value,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        fetch(requestUrl, {
            method: "POST",
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded'
            },
            // @ts-ignore
            body: formBody

        }).then(res => res.json()).then(data => {
            toast.dismiss('registerToastId')
            if (data.success) {
                dispatch({type: 'LOGIN', payload: {user: data.user}});
                router.push('/auth/dashboard');
                // console.log(data)
            } else {

                toast.error('invalid code!')
            }

        })

    }


    function sendCode() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})

        const requestUrl = `${process.env.API_PATH}/api/v1/auth/get-code`


        fetch(requestUrl, {
            headers: {
                Authorization: `bearer ${user.token}`,
                "Content-Type": 'application/x-www-form-urlencoded'
            },


        }).then(res => res.json()).then(data => {
            toast.dismiss('registerToastId')
            toast.success('The code has been sent !')
            setTimer({
                ...timer,
                use: true,
            })


        })

    }

    // const searchParams = useSearchParams()


    return <>
        <div>

            <ToastContainer

                position="top-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>


        <div className={styles.signin}>
            <div className={styles.signin__content}>
                <span className={styles.back_link}><Link href={'/'}> <BackIcon/>  <span>back</span> </Link></span>

                <div className={styles.signin__content__left}>
                    <div className={styles.signin__content__left__title}>
                    <span>

                        <Image src={logo} alt={''}/>
                    </span>

                    </div>
                    <div className={styles.signin__content__left__content}>
                        <div className={styles.signin__content__left__content__title}>
                            <h1>OTP</h1>
                            <p>You Are Not {user?.email} ? <Link href={'/auth/dashboard/sign-out'}
                                                                className={'tx-red'}>Click Here</Link></p>
                        </div>


                        <div className={styles.signin__content__left__form}>
                            <form className={styles.form_group}>
                                <div className={styles.input_group}>
                                    <Input
                                        name={'code'}
                                        value={formData.code.value}
                                        variant="bordered"
                                        onChange={(e) => updateFormData(e)}
                                        isInvalid={!formData.code.validate}
                                        color={!formData.code.validate ? "danger" : "success"}
                                        errorMessage={!formData.code.validate && 'Enter the five-digit code'}
                                        type="text"
                                        data-role={'code'}
                                        label="Code"

                                        labelPlacement='inside'
                                    />
                                </div>


                                <div className={styles.input_grou}>
                                    <Button isDisabled={!formData.code.validate} disabled={!formData.code.validate}
                                            className={`btn-one ${!formData.code.validate && `disabled-btn`}`}
                                            onClick={() => verifyCode()}>
                                        Submit
                                    </Button>
                                </div>
                                <div className={`${styles.input_grou} mt-2`}>
                                    {timer.use ?
                                        <Button isDisabled={true} disabled={true} variant={'bordered'}
                                                className={'btn-two'} onClick={() => sendCode()}>

                                            Resubmit the code : {timer.timer} sec
                                        </Button>
                                        :
                                        <Button className={'btn-one'} onClick={() => sendCode()}>
                                            Get Code
                                        </Button>
                                    }

                                </div>


                            </form>


                        </div>

                    </div>
                </div>

                <div className={styles.signin__content__rigth}>
                    <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
                </div>
            </div>
        </div>
    </>
}