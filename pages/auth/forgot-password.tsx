import {toast, ToastContainer} from "react-toastify";
import styles from "@/styles/auth.module.scss";
import {Button, Input, Link} from "@nextui-org/react";
import {BackIcon} from "@/components/BackIcon";
import Image from "next/image";
import logo from "@/public/log.svg";
import banner from "@/public/login-banner.png";
import React, {useState} from "react";
import validator from "validator";
import {useRouter} from "next/router";


export default function ForgotPasswordPage() {

    const router = useRouter();
    const [fromData, setFromData] = useState({
        email: {
            value: '',
            validate: false,
            error: 'Please enter valid Email !'
        }
    })

    function updateFormData(e: any) {
        switch (e.target.getAttribute("data-role")) {
            case 'email' :
                const email = e.target.value

                const emailValid = validator.isEmail(email)

                setFromData({
                    ...fromData,
                    email: {
                        ...fromData.email,
                        value: e.target.value,
                        validate: emailValid,
                        error: 'Please enter valid Email !'
                    }
                })


        }


    }


    function sendCode() {
        toast.loading('Please Wait !', {toastId: "registerToastId"})
        const userData = {
            'email': fromData.email.value,
        }


        let formBody = [];
        for (var property in userData) {
            var encodedKey = encodeURIComponent(property);
            // @ts-ignore
            var encodedValue = encodeURIComponent(userData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        // @ts-ignore
        formBody = formBody.join("&");


        // @ts-ignore
        fetch(`${process.env.API_PATH}/api/v1/auth/forgot-password`, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded'
            },
            // @ts-ignore
            body: formBody

        }).then(res => res.json()).then((data) => {

            toast.dismiss('registerToastId')

            if (!data.success) {
                toast.error('User not found!');
            } else {
                router.push(`/auth/change-password?email=${fromData.email.value}`);
            }

        })
    }


    return <>


        <ToastContainer

            position="top-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="dark"

        />


        <div className={styles.signin}>
            <div className={styles.signin__content}>
                <span className={styles.back_link}><Link href={'/'}> <BackIcon/>  <span>back</span> </Link></span>

                <div className={styles.signin__content__left}>
                    <div className={styles.signin__content__left__title}>
                    <span>

                        <Image src={logo} alt={''}/>
                    </span>

                    </div>
                    <div className={styles.signin__content__left__content}>
                        <div className={styles.signin__content__left__content__title}>
                            <h1>Forgot you password ?</h1>

                        </div>
                        <div className={styles.signin__content__left__form}>
                            <form className={styles.form_group}>
                                <div className={styles.input_group}>
                                    <Input
                                        name={'email'}
                                        value={fromData.email.value}
                                        variant="bordered"
                                        onChange={(e) => updateFormData(e)}
                                        isInvalid={!fromData.email.validate}
                                        color={!fromData.email.validate ? "danger" : "success"}
                                        errorMessage={!fromData.email.validate && fromData.email.error}
                                        type="text"
                                        data-role={'email'}
                                        label="Enter your Email"

                                        labelPlacement='inside'
                                    />
                                </div>


                                <div className={`${styles.input_grou} mt-2`}>

                                    <Button isDisabled={!fromData.email.validate}
                                            className={`btn-one ${!fromData.email.validate && `disabled-btn`}`}
                                            onClick={() => sendCode()}
                                    >
                                        Get Code
                                    </Button>


                                </div>


                            </form>


                        </div>

                    </div>
                </div>

                <div className={styles.signin__content__rigth}>
                    <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
                </div>
            </div>
        </div>
    </>


}