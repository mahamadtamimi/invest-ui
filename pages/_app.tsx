'use client'
import type {AppProps} from "next/app";
import 'react-toastify/dist/ReactToastify.css';
import {NextUIProvider} from "@nextui-org/react";
import {ThemeProvider as NextThemesProvider} from "next-themes";
import '@/styles/slider.css';
import {Router, useRouter} from 'next/router';
import "@/styles/globals.css";
import "@/styles/globals.scss";

import Loader from "@/components/Loader/Loader";
import React, {useEffect, useState} from "react";
import {Provider} from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import {persistor, store} from "@/reduxStore/store";
import {ToastContainer} from "react-toastify";


export default function App({Component, pageProps}: AppProps) {


    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        Router.events.on("routeChangeStart", (url) => {
            setIsLoading(true)
        });

        Router.events.on("routeChangeComplete", (url) => {
            setIsLoading(false)
        });

        Router.events.on("routeChangeError", (url) => {
            setIsLoading(false)
        });

    }, [Router])

    return (
        <NextUIProvider navigate={router.push}>
            <NextThemesProvider>
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                    {isLoading && <Loader/>}
                        <ToastContainer

                            position="top-left"
                            autoClose={5000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover
                            theme="dark"

                        />
                        <Component {...pageProps} />

                    </PersistGate>
                </Provider>
            </NextThemesProvider>
        </NextUIProvider>
    );
}

