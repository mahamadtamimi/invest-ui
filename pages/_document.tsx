import {Html, Head, Main, NextScript} from 'next/document'
import favIcon from '@/public/favicon.ico'
export default function Document() {
    return (
        <Html lang="en">

            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
                <meta name="cryptomus" content="e1e8b96a"/>
                <title>investitionHub</title>
                {/*<link rel="shortcut icon" href={'public/favicon.ico'}/>*/}
            </Head>


            <body className="light text-foreground bg-background">
            <Main/>
            <NextScript/>
            </body>
        </Html>
    )
}
