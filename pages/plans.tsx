import DefaultLayout from "@/layouts/default";
import AboutUsComponent from "@/components/AboutUsComponent/AboutUsComponent";
import PlansComponent from "@/components/PlansComponent/PlansComponent";


export default function aboutUs() {
    return <DefaultLayout>
        <PlansComponent/>
    </DefaultLayout>
}