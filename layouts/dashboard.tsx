import styles from "@/styles/dashboard.module.scss";
import Sidebar from "@/components/Dashboard/Sidebar/Sidebar";
import Dashboard from "@/components/Dashboard/Dashboard/Dashboard";
import {useSelector} from "react-redux";

import {useRouter} from "next/router";
import AdminSidebar from "@/components/admin/Sidebar/AdminSidebar";
import DashboradMenu from "@/components/DashboradMenu/DashboradMenu";
import {ToastContainer} from "react-toastify";
import React, {useEffect} from "react";


export default function DashboardLayout({
                                            children,
                                        }: {
    children: React.ReactNode;
}) {

    const router = useRouter();

    // @ts-ignore
    const user = useSelector((state) => state.user);

    if (user === null) router.push("/auth/login");


    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/auth/me`, {
            headers: {
                Authorization: `bearer ${user?.token}`

            }
        }).then(res => res.json()).then(data => {
            !data && router.push('/auth/login')
            !data.verifyAt && router.push('/auth/otp')
        })
    }, []);



    // !user.verifyAt && router.push('/auth/otp')
    //
    // console.log(user)

    return (<>
            <DashboradMenu/>

            <div>

                <ToastContainer

                    position="top-left"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="dark"

                />
            </div>

            {
                user && <div className={styles.root}>

                    <Sidebar/>
                    <div className={styles.content}>
                        {children}
                    </div>

                </div>
            }


        </>
    )
}