import {Navbar} from "@/components/navbar";
import {Link} from "@nextui-org/link";
import {Head} from "./head";
import styles from "@/styles/auth.module.scss";
import Image from "next/image";
import logo from "@/public/log.svg";
import {Button, Input} from "@nextui-org/react";
import bannerImage from "@/public/[fpdl 10.png";
import banner from "@/public/[fpdl 10.png";

export default function AuthLayout({
                                          children,
                                      }: {
    children: React.ReactNode;
}) {

    return <div className={styles.signin}>
        <div className={styles.signin__content}>

            <div className={styles.signin__content__left}>
                <div className={styles.signin__content__left__title}>
                              <span>

                                  <Image src={logo} alt=''/>
                              </span>
                    <h1>LOREM IPSUM</h1>
                </div>
                <div className={styles.signin__content__left__content}>
                    <div className={styles.signin__content__left__content__title}>
                        <h1>REGISTRATION </h1>

                    </div>
                    {children}

                </div>
            </div>

            <div className={styles.signin__content__rigth}>
                <Image src={banner} style={{objectFit: "cover", borderRadius: "10px"}} alt=""/>
            </div>
        </div>
    </div>
}
