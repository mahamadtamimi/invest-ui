import styles from "@/styles/dashboard.module.scss";
import Sidebar from "@/components/Dashboard/Sidebar/Sidebar";
import Dashboard from "@/components/Dashboard/Dashboard/Dashboard";
import AdminSidebar from "@/components/admin/Sidebar/AdminSidebar";
import {ToastContainer} from "react-toastify";
import React from "react";


export default function AdminLayout
({
                                            children,
                                        }: {
    children: React.ReactNode;
}) {

    return (<div className={styles.root}>
            <div>

                <ToastContainer

                    position="top-left"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="dark"

                />
            </div>
            <AdminSidebar/>
            <div className={styles.content}>

                {children}
            </div>

        </div>
    )
}