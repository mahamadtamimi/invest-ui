import MainMenu from "@/components/MainMenu/MainMenu";

import {myFont, myFontBold, roboto} from "@/config/fonts";


export default function DefaultLayout({
                                          children,
                                      }: {
    children: React.ReactNode;
}) {
    return (
        <div className={`${myFont.variable} ${myFontBold.variable}  ${roboto.variable} `}>
            <MainMenu/>
            <main className="">
                {children}
            </main>

        </div>
    )
        ;
}
